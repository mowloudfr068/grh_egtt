<?php

namespace Database\Seeders;

use App\Models\Month;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MonthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $months =[
            ['id' => 1 , 'libelle' => 'Janvier','days' => 31],
            ['id' => 2 , 'libelle' => 'Février','days' => 28],
            ['id' => 3 , 'libelle' => 'Mars','days' => 31],
            ['id' => 4 , 'libelle' => 'Avril','days' => 30],
            ['id' => 5 , 'libelle' => 'Mai','days' => 31],
            ['id' => 6 , 'libelle' => 'Juin','days' => 30],
            ['id' => 7 , 'libelle' => 'Juillet','days' => 31],
            ['id' => 8 , 'libelle' => 'Août','days' => 31],
            ['id' => 9 , 'libelle' => 'Septembre','days' => 30],
            ['id' => 10 , 'libelle' => 'Octobre','days' => 31],
            ['id' => 11 , 'libelle' => 'Novembre','days' => 30],
            ['id' => 12, 'libelle' => 'Décembre','days' => 31],
        ];

        foreach($months as $month){
            Month::create($month);
        }
    }
}
