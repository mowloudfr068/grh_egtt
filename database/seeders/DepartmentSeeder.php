<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            [
                'name' => 'Exploration (EXPL)',
                'description' => 'Département chargé des activités d\'exploration, notamment des terrains et ressources.',
                'status' => 1,

            ],
            [
                'name' => 'Drilling (DRLG)',
                'description' => 'Département spécialisé dans les travaux de forage et les activités liées à l\'extraction.',
                'status' => 1,

            ],
            [
                'name' => 'Fossil Fuel Operations (FOSS)',
                'description' => 'Département dédié aux opérations liées aux carburants fossiles, comme la maintenance et la logistique.',
                'status' => 1,

            ],
            [
                'name' => 'Topographie (TOPO)',
                'description' => 'Département de cartographie et de mesures pour l\'étude des terrains.',
                'status' => 1,

            ],
            [
                'name' => 'Geophysics (GPHS)',
                'description' => 'Département de géophysique, travaillant sur l\'étude des propriétés physiques des ressources.',
                'status' => 1,

            ],
            [
                'name' => 'Reclamation (RECN)',
                'description' => 'Département axé sur la réhabilitation environnementale et la remise en état des terrains exploités.',
                'status' => 1,

            ],
            [
                'name' => 'Headquarters (HQ)',
                'description' => 'Département central ou administratif, gérant les fonctions organisationnelles et le support logistique.',
                'status' => 1,

            ],
            [
                'name' => 'CAMP',
                'description' => '',
                'status' => 1,

            ],
        ];

        // Insérer 
        DB::table('departments')->insert($departments);
    }
}
