<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    public function run()
    {
        $positions = [
            [
                'title' => 'Ouvrier', 
                'description' => 'Effectue des tâches manuelles et contribue aux activités de base.', 
                'base_salary' => 21800,
                'department_id' =>5
            ],
            [
                'title' => 'Géologue', 
                'description' => '', 
                'base_salary' => 21800,
                'department_id' =>1
            ],
            [
                'title' => 'Traducteur', 
                'description' => 'Responsable de la traduction des documents et communications.', 
                'base_salary' => 45000,
                'department_id' =>1
                
            ],
            [
                'title' => 'Administrateur de Camp', 
                'description' => 'Supervise les opérations et la gestion des camps.', 
                'base_salary' => 55000,
                'department_id' =>8
            ],
            [
                'title' => 'Electricien', 
                'description' => 'Effectue des installations et des réparations électriques.', 
                'base_salary' => 41000,
                'department_id' =>8
            ],
            [
                'title' => 'Géophisicien', 
                'description' => 'Analyse les données géophysiques pour des projets scientifiques et industriels.', 
                'base_salary' => 67000,
                'department_id' =>5
            ],
            [
                'title' => 'Opérateur Géophysique', 
                'description' => 'Utilise des équipements pour collecter des données géophysiques sur le terrain.', 
                'base_salary' => 50000,
                'department_id' =>5
            ],
            [
                'title' => 'Technicien Géophysique', 
                'description' => 'Assiste les géophysiciens dans les analyses et les relevés techniques.', 
                'base_salary' => 41000,
                'department_id' =>5
            ],
            [
                'title' => 'Mécanicien supérieur', 
                'description' => 'Effectue la maintenance et la réparation d’équipements complexes.', 
                'base_salary' => 41000,
                'department_id' =>8
            ],
            [
                'title' => 'Soudeur', 
                'description' => 'Assemble des pièces métalliques en utilisant diverses techniques de soudage.', 
                'base_salary' => 30600,
                'department_id' =>8
            ],
            [
                'title' => 'Administrateur', 
                'description' => 'Gère les opérations administratives et les tâches organisationnelles.', 
                'base_salary' => 55000,
                'department_id' =>1
            ],
            [
                'title' => 'Femme de Ménage', 
                'description' => 'Assure la propreté et l’entretien des espaces.', 
                'base_salary' => 10000,
                'department_id' =>7
            ],
            [
                'title' => 'Gardien', 
                'description' => 'Surveille et sécurise les lieux et propriétés.', 
                'base_salary' => 10000,
                'department_id' =>7
            ],
        ];

        DB::table('positions')->insert($positions);
    }
}
