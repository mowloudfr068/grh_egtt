<?php

namespace Database\Seeders;

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class EmployeeSeeder extends Seeder
{
    
    public function run()
    {
        $employees = [
            ['matricule' => 104, 'first_name' => 'BOILIL', 'last_name' => 'Mohamed Mostapha', 'nni' => '7638393138', 'position' =>1],
            ['matricule' => 105, 'first_name' => 'TAYA', 'last_name' => 'Sidi Mohamed', 'nni' => '3910572364', 'position' => 1],
            ['matricule' => 107, 'first_name' => 'BOBA', 'last_name' => 'Moussa', 'nni' => '0684678866', 'position' => 1],
            ['matricule' => 106, 'first_name' => 'ABEIDNA', 'last_name' => 'Bedr Dine', 'nni' => '2912277173', 'position' => 1],
            ['matricule' => 108, 'first_name' => 'BRAHIM', 'last_name' => 'Mohameden', 'nni' => '4684670582', 'position' =>1],
            ['matricule' => 109, 'first_name' => 'CHEIKH MALAININE', 'last_name' => 'Sidi Ethmane', 'nni' => '0280626821', 'position' => 1],
            ['matricule' => 110, 'first_name' => 'SAF MAATALLA', 'last_name' => 'Mohamed Salem', 'nni' => '5211096857', 'position' => 1],
            ['matricule' => 111, 'first_name' => 'ABDALLAHI LEMAIDEL', 'last_name' => 'Nema', 'nni' => '1816634819', 'position' => 1],
            ['matricule' => 102, 'first_name' => 'CHEIKH SAAD BOUH', 'last_name' => 'Aghdhafna', 'nni' => '4578128983', 'position' => 1],
            ['matricule' => 113, 'first_name' => 'ABDALLAHI EL VAIGH', 'last_name' => 'Sidi Ahmed', 'nni' => '2680917041', 'position' => 1],
            ['matricule' => 112, 'first_name' => 'AHMED EL VAIGH', 'last_name' => 'Ivekou', 'nni' => '8893605245', 'position' => 1],
            ['matricule' => 115, 'first_name' => 'SIDI MOHAMED', 'last_name' => 'Sidi', 'nni' => '4068991794', 'position' => 1],
            ['matricule' => 706, 'first_name' => 'ABBEKE', 'last_name' => 'Mohamed Mahmoud', 'nni' => '2189284763', 'position' => 2],
            ['matricule' => 709, 'first_name' => 'AHMED WAHENNE', 'last_name' => 'Brahim', 'nni' => '3713398621', 'position' => 2],
            ['matricule' => 701, 'first_name' => 'HABIB', 'last_name' => 'Amar Brahim', 'nni' => '1660310492', 'position' =>3],
            ['matricule' => 707, 'first_name' => 'AHMED AIDDA', 'last_name' => 'Ethmane', 'nni' => '7797362170', 'position' => 2],
            ['matricule' => 704, 'first_name' => 'GUEYA', 'last_name' => 'Mohamed Mahmoud', 'nni' => '9732869615', 'position' => 2],
            ['matricule' => 705, 'first_name' => 'EL HADJ WEISS', 'last_name' => 'Mohamed Lemine', 'nni' => '9375005284', 'position' => 2],
            ['matricule' => 702, 'first_name' => 'Bezeid', 'last_name' => 'Nasser Dine', 'nni' => '2147483647', 'position' => 2],
            ['matricule' => 801, 'first_name' => 'YEHDHIH', 'last_name' => 'Mohamed', 'nni' => '9212052462', 'position' => 4],
            ['matricule' => 805, 'first_name' => 'Sidi Ahmed', 'last_name' => 'Mohamedou', 'nni' => '9501585919', 'position' => 4],
            ['matricule' => 803, 'first_name' => 'MAMADI', 'last_name' => 'Mohamed Lemine', 'nni' => '0434031642', 'position' => 4],
            ['matricule' => 603, 'first_name' => 'DIENG', 'last_name' => 'Dame', 'nni' => '5197003727', 'position' => 5],
            ['matricule' => 605, 'first_name' => 'SARR', 'last_name' => 'Farba', 'nni' => '1977923218', 'position' => 6],
            ['matricule' => 606, 'first_name' => 'SY', 'last_name' => 'Moussa', 'nni' => '4234484755', 'position' =>6],
            ['matricule' => 607, 'first_name' => 'TALEB AHMED', 'last_name' => 'Ahmed Taleb', 'nni' => '0613319167', 'position' =>6],
            ['matricule' => 133, 'first_name' => 'Bodah', 'last_name' => 'Brahim', 'nni' => '0691935048', 'position' => 1],
            ['matricule' => 134, 'first_name' => 'Khabbaz', 'last_name' => 'Mohamed Sidina', 'nni' => '3012884118', 'position' => 1],
            ['matricule' => 135, 'first_name' => 'Jedou', 'last_name' => 'Mohamed El Boukhari', 'nni' => '9399825547', 'position' => 1],
            ['matricule' => 136, 'first_name' => 'Tacki', 'last_name' => 'Brahim', 'nni' => '8171639192', 'position' => 1],
            ['matricule' => 118, 'first_name' => 'El Ghotob LIMAM', 'last_name' => 'Sidi Mohamed', 'nni' => '7829436772', 'position' => 7],
            ['matricule' => 120, 'first_name' => 'Brahim TOUMANI', 'last_name' => 'Toumani', 'nni' => '4044528782', 'position' => 7],
            ['matricule' => 121, 'first_name' => 'SAMOURI', 'last_name' => 'Mohamed Salem', 'nni' => '6002274738', 'position' => 1],
            ['matricule' => 130, 'first_name' => 'HAEIMOUD', 'last_name' => 'Abdellahi', 'nni' => '8364732630', 'position' => 1],
            ['matricule' => 132, 'first_name' => 'LEBGHAYL', 'last_name' => 'Ely Cheikh', 'nni' => '7551074029', 'position' => 1],
            ['matricule' => 129, 'first_name' => 'HAJ WEISS', 'last_name' => 'Mohamed', 'nni' => '3622491482', 'position' => 1],
            ['matricule' => 137, 'first_name' => 'BRAHIM TFEIL', 'last_name' => 'Sidi Mohamed', 'nni' => '6082900168', 'position' => 1],
            ['matricule' => 138, 'first_name' => 'MOHAMED EL HADRAMY', 'last_name' => 'Mohamed Abdallahi', 'nni' => '9190575110', 'position' => 1],
            ['matricule' => 806, 'first_name' => 'Kaliuzhnyi', 'last_name' => 'Oleg', 'nni' => '9355470260', 'position' => 8],
            ['matricule' => 807, 'first_name' => 'KANE', 'last_name' => 'Assane', 'nni' => '0719600101', 'position' => 9],
            ['matricule' => 804, 'first_name' => 'Sidi Ahmed Saad', 'last_name' => 'Brahim', 'nni' => '3245048283', 'position' => 9],
            ['matricule' => 808, 'first_name' => 'AHMEDOU', 'last_name' => 'Mohamed Salem', 'nni' => '0752214243', 'position' => 9],
            ['matricule' => 125, 'first_name' => 'EL KOWRY', 'last_name' => 'Mahmoud', 'nni' => '3968385625', 'position' => 1],
            ['matricule' => 126, 'first_name' => 'MOHAMED', 'last_name' => 'Aly', 'nni' => '0993794780', 'position' => 1],
            ['matricule' => 127, 'first_name' => 'LIMAM', 'last_name' => 'Mohamed El Hadramy', 'nni' => '2685378944', 'position' => 1],
            ['matricule' => 123, 'first_name' => 'ABEIDNA', 'last_name' => 'Chems Dine', 'nni' => '3548479512', 'position' => 1],
            ['matricule' => 128, 'first_name' => 'AHMED LEALY', 'last_name' => 'El Hadrami', 'nni' => '7994465491', 'position' => 1],
            ['matricule' => 703, 'first_name' => 'MOHAMED BOILIL', 'last_name' => 'Salem', 'nni' => '9981643672', 'position' =>10],
            ['matricule' => 905, 'first_name' => 'COULIBALY', 'last_name' => 'Moima', 'nni' => '8164190174', 'position' => 11],
            ['matricule' => 906, 'first_name' => 'DIA', 'last_name' => 'Abou Ethmane', 'nni' => '3655673436', 'position' => 12],
        ];

        foreach ($employees as $employeeData) {
            $employee = Employee::create([
                'matricule' => $employeeData['matricule'],
                'nni' => $employeeData['nni'],
                'first_name' => $employeeData['first_name'],
                'last_name' => $employeeData['last_name'],
                'email' => strtolower($employeeData['first_name']) .$employeeData['matricule'].'@gmail.com',
                'phone' => '49874807',
                'address' => 'Nouakchott',
                'date_of_birth' => Carbon::createFromDate(2000, 1, 1),
                'date_of_hire' => now(),
                'position_id' => $employeeData['position'],
                'password' => Hash::make($employeeData['nni']),
                'status' => 1,
                'contract_type' => 'CDD',
                'contract_duration' => 1, 
            ]);

            $roleEmployee = Role::firstOrCreate(['name' => 'employee', 'guard_name' => 'employee']);
            $employee->assignRole([$roleEmployee->id]);
        }
    }
        
    }

