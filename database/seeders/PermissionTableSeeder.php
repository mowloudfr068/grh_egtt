<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Liste des permissions avec le guard 'web' par défaut
        $permissions = [
            'departements',
            'list_departement',
            'ajout_departement',
            'modifier_departement',
            'supprimer_departement',

            'positions',
            'list_position',
            'ajout_position',
            'modifier_position',
            'supprimer_position',

            'emploiyes',
            'list_emploiye',
            'ajout_emploiye',
            'modifier_emploiye',
            'supprimer_emploiye',

            'utilisateurs',
            'list_utilisateur',
            'ajout_utilisateur',
            'modifier_utilisateur',
            'supprimer_utilisateur',
            'voir_utilisateur',

            'roles_utilisateur',
            'list_roles_utilisateur',
            'ajout_roles_utilisateur',
            'modifier_roles_utilisateur',
            'supprimer_roles_utilisateur',
            'voir_roles_utilisateur',

            'conges',
            'list_conges',
            'ajout_conges',
            'modifier_conges',
            'supprimer_conges',

            'presences',
            'list_presences',
            'ajout_presences',
            'modifier_presences',
            'supprimer_presences',

            'paiements',
            'list_paiements',
            'ajout_paiements',
            'annuler_paiements',

            'bultins',
            'list_emploiyee_bultin',
            'imprimer_bultin_emploiyee',
            'imprimer_bultin_patron',
            
            'factures',
            'list_factures',
            'ajout_factures',
            'modifier_factures',
            'supprimer_factures',

            'lettre',
            'create_lettre',
            'notifications'
        ];

       
        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission,
                'guard_name' => 'web', 
            ]);
        }

        
        $employeePermissions = [
            'getInfoEmploiyeeById',
            'getInfoEmploiyeedetailsById',
            'getInfoCongesEmploiyeeById',
            'getInfoEmploiyeeBulletinById',
            'getInfoPointagesEmploiyeeById',
        ];

        
        foreach ($employeePermissions as $permission) {
            Permission::create([
                'name' => $permission,
                'guard_name' => 'employee', 
            ]);
        }
    }
}
