<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateAdminUserSeeder extends Seeder
{
   
    public function run()
    {
        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123123123'),
            'roles_name' => ['owner'],
            'status' => 'activer',
            ]);

            $employee = Employee::create([
                'matricule' => 1,
                'nni' => '1234567890',
                'first_name' => 'John',
                'last_name' => 'Doe',
                'email' => 'john.doe@example.com',
                'phone' => '123456789',
                'address' => '123 Main St, Example City',
                'date_of_birth' => now(),
                'date_of_hire' => now(),
                'position_id' => 1,
                'password' => Hash::make('password123'),
                'status' =>1,
                'contract_type' => 'CDI', 
                'contract_duration' => null, 
            ]);
            $role = Role::create(['name' => 'owner']);
            $roleEmployee = Role::create(['name' => 'employee', 'guard_name' => 'employee']);
            $permissions = Permission::where('guard_name','web')->pluck('id');
            $permissionsEmp = Permission::where('guard_name','employee')->pluck('id');
            $role->syncPermissions($permissions);
            $roleEmployee->syncPermissions($permissionsEmp);
            $user->assignRole([$role->id]);
            $employee->assignRole([$roleEmployee->id]);

            }
    }


