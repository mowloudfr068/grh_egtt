<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('ordre'); // Ordre unique 
            // Colonnes financieres
            $table->decimal('salaire_brut_mensuel', 10, 2);
            $table->decimal('cnam_employee', 10, 2);
            $table->decimal('cnss_employee', 10, 2);
            $table->decimal('its', 10, 2);
            $table->decimal('salaire_net', 10, 2);
            $table->decimal('cnam_patron', 10, 2);
            $table->decimal('medecin_travail', 10, 2);
            $table->decimal('cnss_patron', 10, 2);
            $table->decimal('conges', 10, 2);
            $table->decimal('total_cout', 10, 2);
            $table->unsignedBigInteger('attendance_id');
            $table->timestamps();
            // Clé étrangère
            $table->foreign('attendance_id')->references('id')->on('attendances')->onDelete('cascade');
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
