<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('nni')->unique(); 
            $table->bigInteger('matricule')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone'); 
            $table->string('address');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('photo')->nullable();
            $table->date('date_of_birth');
            $table->date('date_of_hire');
            $table->string('contract_type')->default('CDI'); 
            $table->integer('contract_duration')->nullable(); 
            $table->boolean('status');
            $table->unsignedBigInteger('position_id');
            $table->timestamps();
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
