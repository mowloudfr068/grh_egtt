-- MariaDB dump 10.19  Distrib 10.4.32-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: grh_egtt
-- ------------------------------------------------------
-- Server version	10.4.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attendances`
--

DROP TABLE IF EXISTS `attendances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendances` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) unsigned NOT NULL,
  `month_id` bigint(20) unsigned NOT NULL,
  `anne` year(4) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `days` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`days`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attendances_employee_id_foreign` (`employee_id`),
  KEY `attendances_month_id_foreign` (`month_id`),
  CONSTRAINT `attendances_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attendances_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendances`
--

LOCK TABLES `attendances` WRITE;
/*!40000 ALTER TABLE `attendances` DISABLE KEYS */;
INSERT INTO `attendances` VALUES (14,14,1,2025,1,'\"[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(15,11,1,2025,1,'\"[3,4,5,6,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(16,9,1,2025,1,'\"[1,3,4,5,6,7,8,10,11,12,13,14,15,16,17]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(17,5,1,2025,1,'\"[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(18,47,1,2025,1,'\"[1,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(19,17,1,2025,1,'\"[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(20,12,1,2025,1,'\"[1,2,3,4,5,6,8,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(21,48,1,2025,1,'\"[1,2,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(22,15,1,2025,1,'\"[3,4,5,6,7,8,9,10,11,12,13]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(23,43,1,2025,1,'\"[2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]\"','2025-01-25 12:01:14','2025-01-25 12:01:14'),(24,20,1,2025,1,'\"[\\\"1\\\",\\\"2\\\",\\\"3\\\",\\\"4\\\",\\\"5\\\",\\\"6\\\",\\\"7\\\",\\\"8\\\",\\\"9\\\",\\\"10\\\",\\\"11\\\",\\\"12\\\",\\\"13\\\",\\\"14\\\",\\\"15\\\",\\\"16\\\",\\\"17\\\"]\"','2025-01-25 12:02:44','2025-01-25 12:05:12'),(25,28,1,2025,1,'\"[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:02:44','2025-01-25 12:02:44'),(26,6,1,2025,1,'\"[1,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:02:44','2025-01-25 12:02:44'),(27,52,1,2025,1,'\"[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:23:10','2025-01-25 12:23:10'),(28,52,2,2025,1,'\"[4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]\"','2025-01-25 12:25:18','2025-01-25 12:25:18');
/*!40000 ALTER TABLE `attendances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_logs`
--

DROP TABLE IF EXISTS `audit_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `role` text NOT NULL,
  `action` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audit_logs_user_id_foreign` (`user_id`),
  CONSTRAINT `audit_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_logs`
--

LOCK TABLES `audit_logs` WRITE;
/*!40000 ALTER TABLE `audit_logs` DISABLE KEYS */;
INSERT INTO `audit_logs` VALUES (6,1,'\"owner\"','Connexion','Utilisateur connecté avec succès','127.0.0.1','2025-01-25 11:51:41','2025-01-25 11:51:41'),(7,1,'[\"owner\"]','Enregistrement','Action sur pointage id : 14','127.0.0.1','2025-01-25 12:01:14','2025-01-25 12:01:14'),(8,1,'[\"owner\"]','Enregistrement','Action sur pointage id : 24','127.0.0.1','2025-01-25 12:02:44','2025-01-25 12:02:44'),(9,1,'[\"owner\"]','modification','Action sur pointage mois : 1 anne 2025 employee_id 20','127.0.0.1','2025-01-25 12:05:12','2025-01-25 12:05:12'),(10,1,'[\"owner\"]','Enregistrement','Action sur Paiements id : 1 jusqua 13','127.0.0.1','2025-01-25 12:07:55','2025-01-25 12:07:55'),(11,1,'[\"owner\"]','Enregistrement','Action sur facture id : 1','127.0.0.1','2025-01-25 12:15:30','2025-01-25 12:15:30'),(12,1,'[\"owner\"]','Enregistrement','Action sur conge id : 1','127.0.0.1','2025-01-25 12:18:46','2025-01-25 12:18:46'),(13,1,'[\"owner\"]','modification','Action sur conge id : 1','127.0.0.1','2025-01-25 12:19:07','2025-01-25 12:19:07'),(14,1,'[\"owner\"]','Enregistrement','Action sur Employee id : 52','127.0.0.1','2025-01-25 12:21:18','2025-01-25 12:21:18'),(15,1,'[\"owner\"]','Enregistrement','Action sur pointage id : 27','127.0.0.1','2025-01-25 12:23:10','2025-01-25 12:23:10'),(16,1,'[\"owner\"]','Enregistrement','Action sur paiement id : 14 ORDRE PAY-002','127.0.0.1','2025-01-25 12:23:57','2025-01-25 12:23:57'),(17,1,'[\"owner\"]','Enregistrement','Action sur pointage id : 28','127.0.0.1','2025-01-25 12:25:18','2025-01-25 12:25:18'),(18,1,'[\"owner\"]','Enregistrement','Action sur paiement id : 15 ORDRE PAY-003','127.0.0.1','2025-01-25 12:26:26','2025-01-25 12:26:26');
/*!40000 ALTER TABLE `audit_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'Exploration (EXPL)','Département chargé des activités d\'exploration, notamment des terrains et ressources.',1,NULL,NULL),(2,'Drilling (DRLG)','Département spécialisé dans les travaux de forage et les activités liées à l\'extraction.',1,NULL,NULL),(3,'Fossil Fuel Operations (FOSS)','Département dédié aux opérations liées aux carburants fossiles, comme la maintenance et la logistique.',1,NULL,NULL),(4,'Topographie (TOPO)','Département de cartographie et de mesures pour l\'étude des terrains.',1,NULL,NULL),(5,'Geophysics (GPHS)','Département de géophysique, travaillant sur l\'étude des propriétés physiques des ressources.',1,NULL,NULL),(6,'Reclamation (RECN)','Département axé sur la réhabilitation environnementale et la remise en état des terrains exploités.',1,NULL,NULL),(7,'Headquarters (HQ)','Département central ou administratif, gérant les fonctions organisationnelles et le support logistique.',1,NULL,NULL),(8,'CAMP','',1,NULL,NULL);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nni` varchar(255) NOT NULL,
  `matricule` bigint(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `date_of_hire` date NOT NULL,
  `contract_type` varchar(255) NOT NULL DEFAULT 'CDI',
  `contract_duration` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `position_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_nni_unique` (`nni`),
  UNIQUE KEY `employees_matricule_unique` (`matricule`),
  UNIQUE KEY `employees_email_unique` (`email`),
  KEY `employees_position_id_foreign` (`position_id`),
  CONSTRAINT `employees_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'1234567890',1,'John','Doe','123456789','123 Main St, Example City','john.doe@example.com','$2y$10$/F3DK1Bf2C.u1unOSOvA.OWWotDXAwdkSo6nNwyQBNHlfFaUidZ2W',NULL,'2025-01-25','2025-01-25','CDI',NULL,1,1,'2025-01-25 11:23:48','2025-01-25 11:23:48'),(2,'7638393138',104,'BOILIL','Mohamed Mostapha','49874807','Nouakchott','boilil104@gmail.com','$2y$10$YeOuF/muhffwhW.IvQ5wj.ivGnHmlxx5zuEJng91ZcIGMjheKP1M6',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:48','2025-01-25 11:23:48'),(3,'3910572364',105,'TAYA','Sidi Mohamed','49874807','Nouakchott','taya105@gmail.com','$2y$10$F67YiUqDocfOdnhPsNv6ZO5oNtfbcZ9YfJjMyzp9o0f05U5/YOKAC',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:48','2025-01-25 11:23:48'),(4,'0684678866',107,'BOBA','Moussa','49874807','Nouakchott','boba107@gmail.com','$2y$10$8YcLeSpPi5lVhK1zADRwyesp8e04X18xB3C74.uDDiHKkwEEzdy8e',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(5,'2912277173',106,'ABEIDNA','Bedr Dine','49874807','Nouakchott','abeidna106@gmail.com','$2y$10$dVEDHkAIR5wmV5xmUtfdoezX9b2cw8yiUj4jWKlqlr/94hljz9N9.',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(6,'4684670582',108,'BRAHIM','Mohameden','49874807','Nouakchott','brahim108@gmail.com','$2y$10$0Fz4t7wwxhxcDOxzlWZxfe/XGcrUOYscqwOpSU7RlUQhsmdcu4SXm',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(7,'0280626821',109,'CHEIKH MALAININE','Sidi Ethmane','49874807','Nouakchott','cheikh malainine109@gmail.com','$2y$10$LxPlyF0TJ7bo94mTXuF01ObiSwE5vmsBdMtN9pxctUaXMXVy51Iie',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(8,'5211096857',110,'SAF MAATALLA','Mohamed Salem','49874807','Nouakchott','saf maatalla110@gmail.com','$2y$10$.dXgJ9gbtDWMkrPcAQ9V2.V4GGt9QWa9Oh0IvovF0EcW2dCiofs3e',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(9,'1816634819',111,'ABDALLAHI LEMAIDEL','Nema','49874807','Nouakchott','abdallahi lemaidel111@gmail.com','$2y$10$YfrfQtf6nMpJNy4lDnB7G.ksod3G4QJlqG29otoxYyhGfySe7qp9y',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(10,'4578128983',102,'CHEIKH SAAD BOUH','Aghdhafna','49874807','Nouakchott','cheikh saad bouh102@gmail.com','$2y$10$JSL2uKdovt4ZbBNhC0WEAO2fNGVUU9PiXfjAPxb0YEMFlPSPuqpVq',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(11,'2680917041',113,'ABDALLAHI EL VAIGH','Sidi Ahmed','49874807','Nouakchott','abdallahi el vaigh113@gmail.com','$2y$10$TOi2xpdAeaeo85UombZihOhuLFjrr3bTMuKGVm7rh.xUrVDfGXRf2',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(12,'8893605245',112,'AHMED EL VAIGH','Ivekou','49874807','Nouakchott','ahmed el vaigh112@gmail.com','$2y$10$cLbBGqvO5PfljnQonPlOtOPCBDaUiYXUuxYJst9OXIKvzCG5Pp62S',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(13,'4068991794',115,'SIDI MOHAMED','Sidi','49874807','Nouakchott','sidi mohamed115@gmail.com','$2y$10$S/egEuL.qaIC3Jaf4PpY6.0Hc0ZBPmuM/vo6Z4LZRw9k/sYclztxa',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:49','2025-01-25 11:23:49'),(14,'2189284763',706,'ABBEKE','Mohamed Mahmoud','49874807','Nouakchott','abbeke706@gmail.com','$2y$10$2uzfbBV22Eos3sLrY0VVhOHK9UWMxoQw3UBMQqwNYjgEMUwHLMm.W',NULL,'2000-01-01','2025-01-25','CDD',1,1,2,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(15,'3713398621',709,'AHMED WAHENNE','Brahim','49874807','Nouakchott','ahmed wahenne709@gmail.com','$2y$10$porW0mgOcT/QB7Q57Fb7a.Y/nv3Qt0tHAsMwzpd0C9A6bghu5bDTC',NULL,'2000-01-01','2025-01-25','CDD',1,1,2,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(16,'1660310492',701,'HABIB','Amar Brahim','49874807','Nouakchott','habib701@gmail.com','$2y$10$T4BcjLeVYQ6LwkrM3WfVHOwJqG0ENRBrvSOfyWZv9VWw3Dr07uySS',NULL,'2000-01-01','2025-01-25','CDD',1,1,3,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(17,'7797362170',707,'AHMED AIDDA','Ethmane','49874807','Nouakchott','ahmed aidda707@gmail.com','$2y$10$75k3P01aM3iUuU20Rexpa.Hs9zjDTGr4ki98p0soKN09.sTgffKzK',NULL,'2000-01-01','2025-01-25','CDD',1,1,2,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(18,'9732869615',704,'GUEYA','Mohamed Mahmoud','49874807','Nouakchott','gueya704@gmail.com','$2y$10$QVsJmRUlWWyABWKeOqmCQ.aQubhzg7S35rUNpsPanThFef0PWEqOG',NULL,'2000-01-01','2025-01-25','CDD',1,1,2,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(19,'9375005284',705,'EL HADJ WEISS','Mohamed Lemine','49874807','Nouakchott','el hadj weiss705@gmail.com','$2y$10$50HRP19WEd/xq0lD8MFpMeLREsn2H5j2uM/2RQBJDE8WLdQlPLHCC',NULL,'2000-01-01','2025-01-25','CDD',1,1,2,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(20,'2147483647',702,'Bezeid','Nasser Dine','49874807','Nouakchott','bezeid702@gmail.com','$2y$10$jJj9BiXU3ta00qa9PC7akOJsg3/VwLqkX69eVW2yZFV2zTIz2ZDWu',NULL,'2000-01-01','2025-01-25','CDD',1,1,2,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(21,'9212052462',801,'YEHDHIH','Mohamed','49874807','Nouakchott','yehdhih801@gmail.com','$2y$10$FuZGTYZL7xSaebYsx/PHP.dRR99Okb36ktVaQhJRnAFTLLOXeMe82',NULL,'2000-01-01','2025-01-25','CDD',1,1,4,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(22,'9501585919',805,'Sidi Ahmed','Mohamedou','49874807','Nouakchott','sidi ahmed805@gmail.com','$2y$10$7R/C7hsbjsy7Ovx4lpCa6u7mFyNnKPXv6NFrO0RiB.6EqDqpbNtdK',NULL,'2000-01-01','2025-01-25','CDD',1,1,4,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(23,'0434031642',803,'MAMADI','Mohamed Lemine','49874807','Nouakchott','mamadi803@gmail.com','$2y$10$XrQhl.5Q00cWFa.CYnkDvepCF.QTFAfthHU48g5.FhtCOcoiA5ia6',NULL,'2000-01-01','2025-01-25','CDD',1,1,4,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(24,'5197003727',603,'DIENG','Dame','49874807','Nouakchott','dieng603@gmail.com','$2y$10$jCqHYM7hNyzSFZaUssJWKezNQ/UNfoq40I5wjx0GSL0nkdpP0l9e.',NULL,'2000-01-01','2025-01-25','CDD',1,1,5,'2025-01-25 11:23:50','2025-01-25 11:23:50'),(25,'1977923218',605,'SARR','Farba','49874807','Nouakchott','sarr605@gmail.com','$2y$10$AhubK1YYZXGiXQBr643pMu4Yka0MD2zbmhxNtk73dj8EDTcxmIrBO',NULL,'2000-01-01','2025-01-25','CDD',1,1,6,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(26,'4234484755',606,'SY','Moussa','49874807','Nouakchott','sy606@gmail.com','$2y$10$M7qjkW.DZfZ18sTxnVVvEOPZT1auNLMKl5OAa60eFMo7DzfRCC.lS',NULL,'2000-01-01','2025-01-25','CDD',1,1,6,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(27,'0613319167',607,'TALEB AHMED','Ahmed Taleb','49874807','Nouakchott','taleb ahmed607@gmail.com','$2y$10$vrAcbxmXHEXp19RHEcis5eXZRyzsRJxfQKJ4Sue/dWprXorqlCwha',NULL,'2000-01-01','2025-01-25','CDD',1,1,6,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(28,'0691935048',133,'Bodah','Brahim','49874807','Nouakchott','bodah133@gmail.com','$2y$10$tDw.Ks2bOywwiUJi6JPh2OkJSpdfS6ldaDklMnpULs0v6DRamTsGW',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(29,'3012884118',134,'Khabbaz','Mohamed Sidina','49874807','Nouakchott','khabbaz134@gmail.com','$2y$10$b8kOe4VCZAx1h4qKCPLGNekP8Gt5Fc.PVLMov6zIs24qaTws8BFhW',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(30,'9399825547',135,'Jedou','Mohamed El Boukhari','49874807','Nouakchott','jedou135@gmail.com','$2y$10$pbBbjyDN3FvzueK9/7vHx.fXhBx7rbreAPew7VOiRLUkvr.QkwEPe',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(31,'8171639192',136,'Tacki','Brahim','49874807','Nouakchott','tacki136@gmail.com','$2y$10$9ScII2DJzj8EWCF5btYPNu8FIDyCxYJowNe/oMFAxtLXRkWtF2CO6',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(32,'7829436772',118,'El Ghotob LIMAM','Sidi Mohamed','49874807','Nouakchott','el ghotob limam118@gmail.com','$2y$10$TURWe55oaQdfhC/HqyCVyOvqWfKiyyf6joOUuJGhERpu7MYMi2yHK',NULL,'2000-01-01','2025-01-25','CDD',1,1,7,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(33,'4044528782',120,'Brahim TOUMANI','Toumani','49874807','Nouakchott','brahim toumani120@gmail.com','$2y$10$k7EQiDGSg.wZs0qiQPmBReG.EZZkgemuf/wgNWnz41g6TBJD/XV.O',NULL,'2000-01-01','2025-01-25','CDD',1,1,7,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(34,'6002274738',121,'SAMOURI','Mohamed Salem','49874807','Nouakchott','samouri121@gmail.com','$2y$10$L4l1ufBDFXYKyfCRh795ROPXeI3bbfp3PhBZ2YschCTzoxFGBYkCq',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(35,'8364732630',130,'HAEIMOUD','Abdellahi','49874807','Nouakchott','haeimoud130@gmail.com','$2y$10$6nQPfTEZav79rSrMOnumRebDhJd6kfJkAuHk4ZdQfJDbEI6bfE82a',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:51','2025-01-25 11:23:51'),(36,'7551074029',132,'LEBGHAYL','Ely Cheikh','49874807','Nouakchott','lebghayl132@gmail.com','$2y$10$YYMrPDp2ZoaWI3C6rYfQUurvL6gXEnihg11SB0d5G0/n4eIFh00jW',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(37,'3622491482',129,'HAJ WEISS','Mohamed','49874807','Nouakchott','haj weiss129@gmail.com','$2y$10$KD6x0B.66lNN8P7WupXSveMQbdrOmf1C3230Nyt4q/nYoYlxnzzBO',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(38,'6082900168',137,'BRAHIM TFEIL','Sidi Mohamed','49874807','Nouakchott','brahim tfeil137@gmail.com','$2y$10$chUkb/ik9J2yW1W1oPoTye7YhNf7qgR5CTVe6TMY93ZtVEUwR6z7m',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(39,'9190575110',138,'MOHAMED EL HADRAMY','Mohamed Abdallahi','49874807','Nouakchott','mohamed el hadramy138@gmail.com','$2y$10$s3uwWUmmU1DlvA2chEcuauYu44XRffuTzD/DQGeO8NoAKz2pAAetO',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(40,'9355470260',806,'Kaliuzhnyi','Oleg','49874807','Nouakchott','kaliuzhnyi806@gmail.com','$2y$10$Yqz7wKV2F2ZxY1bcKwL0dODiAghkP.qUmXqzsCzNyB4rKENsJKK/q',NULL,'2000-01-01','2025-01-25','CDD',1,1,8,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(41,'0719600101',807,'KANE','Assane','49874807','Nouakchott','kane807@gmail.com','$2y$10$7r6IzzBCzf5eCDs3HoMnKew0xl5ewgxF0rcGgAfzSErUni.lWXYsq',NULL,'2000-01-01','2025-01-25','CDD',1,1,9,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(42,'3245048283',804,'Sidi Ahmed Saad','Brahim','49874807','Nouakchott','sidi ahmed saad804@gmail.com','$2y$10$xuaEsQUvq9ESFOjaLyBK6u0CT32coZ02ughtgkY4kdfoKxn2ACOxu',NULL,'2000-01-01','2025-01-25','CDD',1,1,9,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(43,'0752214243',808,'AHMEDOU','Mohamed Salem','49874807','Nouakchott','ahmedou808@gmail.com','$2y$10$5eG4hxgQLb5/MGbiwvWtxuKvX3a5qEIpFEhlJzolito3dlN7/5otm',NULL,'2000-01-01','2025-01-25','CDD',1,1,9,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(44,'3968385625',125,'EL KOWRY','Mahmoud','49874807','Nouakchott','el kowry125@gmail.com','$2y$10$TvRgpr6vdcEH9jndVyrlgu/gazMiKVtlE2ymRPKI0pcD6NfGNcNCq',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(45,'0993794780',126,'MOHAMED','Aly','49874807','Nouakchott','mohamed126@gmail.com','$2y$10$dQ6XEDCiuhnsMCIcWk./luMpyCS1t8LYLM3ccB8j57pJmb6DX.60u',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(46,'2685378944',127,'LIMAM','Mohamed El Hadramy','49874807','Nouakchott','limam127@gmail.com','$2y$10$plgncc4uLOLre0BsLLlHSuAXtFW3ROyIfUenXyP9T132i2DSqdAKe',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:52','2025-01-25 11:23:52'),(47,'3548479512',123,'ABEIDNA','Chems Dine','49874807','Nouakchott','abeidna123@gmail.com','$2y$10$gKWToqzSEYSO.F4YMjFmfeB6XC/OmwBZvgWygIO30f.GBgTSOfCHW',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:53','2025-01-25 11:23:53'),(48,'7994465491',128,'AHMED LEALY','El Hadrami','49874807','Nouakchott','ahmed lealy128@gmail.com','$2y$10$1cEkaFPHaKpr7QiEu67si.Ggk22NA2CAMTWkFM0cASFpjzyNhhe3S',NULL,'2000-01-01','2025-01-25','CDD',1,1,1,'2025-01-25 11:23:53','2025-01-25 11:23:53'),(49,'9981643672',703,'MOHAMED BOILIL','Salem','49874807','Nouakchott','mohamed boilil703@gmail.com','$2y$10$zxoOo.xH0o1UNHA6uY/QMuurrcYfFdiCKqjxCsZ14GNdByy0nP8NK',NULL,'2000-01-01','2025-01-25','CDD',1,1,10,'2025-01-25 11:23:53','2025-01-25 11:23:53'),(50,'8164190174',905,'COULIBALY','Moima','49874807','Nouakchott','coulibaly905@gmail.com','$2y$10$04gtbqf7KCMLK6toaVn5W.f8jMq7sp9lUeSAuZa8lMlH0Xe4ti7vm',NULL,'2000-01-01','2025-01-25','CDD',1,1,11,'2025-01-25 11:23:53','2025-01-25 11:23:53'),(51,'3655673436',906,'DIA','Abou Ethmane','49874807','Nouakchott','dia906@gmail.com','$2y$10$kzG38ty6AmndWI9fwclmLu5KWJSZQdhawN8Ek/J7Rvcq3Nz.kMfDC',NULL,'2000-01-01','2025-01-25','CDD',1,1,12,'2025-01-25 11:23:53','2025-01-25 11:23:53'),(52,'0433744716',907,'mowloud','ahmed','49874807','32WQ+CFJ نواكشوط، موريتانيا','mowloud@gmail.com','$2y$10$H5tFuJWNogZJteF4zC5rweOjsf8OyRvmVDoa/48SJvoAWD/09.PXe',NULL,'2001-08-20','2025-01-25','CDD',2,1,2,'2025-01-25 12:21:18','2025-01-25 12:21:18');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `nom_client` varchar(255) NOT NULL,
  `tel_client` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (1,'prestation de services de l\'acompagnement des travaux d\'exploitation geologique','2025-01-25','2025-02-28',252000.00,'emiral mining sarl','49874807','2025-01-25 12:15:30','2025-01-25 12:15:30');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaves`
--

DROP TABLE IF EXISTS `leaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaves` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` enum('En Attente','Approuvé','Refusé') NOT NULL DEFAULT 'En Attente',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `leaves_employee_id_foreign` (`employee_id`),
  CONSTRAINT `leaves_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaves`
--

LOCK TABLES `leaves` WRITE;
/*!40000 ALTER TABLE `leaves` DISABLE KEYS */;
INSERT INTO `leaves` VALUES (1,2,'2025-01-25','2025-01-27','maladie','Approuvé','2025-01-25 12:18:46','2025-01-25 12:19:07'),(2,52,'2025-03-01','2025-01-03','maladie','En Attente','2025-01-25 12:29:56','2025-01-25 12:29:56');
/*!40000 ALTER TABLE `leaves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2024_06_05_122346_create_departments_table',1),(6,'2024_06_05_171135_create_positions_table',1),(7,'2024_06_19_134137_create_employees_table',1),(8,'2024_09_19_120721_create_leaves_table',1),(9,'2024_09_20_102322 _create_months_table',1),(10,'2024_09_20_102326_create_attendances_table',1),(11,'2024_11_16_110626 _create_payments_table',1),(12,'2024_12_03_185723_create_invoices_table',1),(13,'2024_12_04_225105_create_permission_tables',1),(14,'2024_12_12_103053_create_notifications_table',1),(15,'2024_12_16_183239_create_password_resets_employees_table',1),(16,'2024_12_17_113924_create_audit_logs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\Models\\User',1),(2,'App\\Models\\Employee',1),(2,'App\\Models\\Employee',2),(2,'App\\Models\\Employee',3),(2,'App\\Models\\Employee',4),(2,'App\\Models\\Employee',5),(2,'App\\Models\\Employee',6),(2,'App\\Models\\Employee',7),(2,'App\\Models\\Employee',8),(2,'App\\Models\\Employee',9),(2,'App\\Models\\Employee',10),(2,'App\\Models\\Employee',11),(2,'App\\Models\\Employee',12),(2,'App\\Models\\Employee',13),(2,'App\\Models\\Employee',14),(2,'App\\Models\\Employee',15),(2,'App\\Models\\Employee',16),(2,'App\\Models\\Employee',17),(2,'App\\Models\\Employee',18),(2,'App\\Models\\Employee',19),(2,'App\\Models\\Employee',20),(2,'App\\Models\\Employee',21),(2,'App\\Models\\Employee',22),(2,'App\\Models\\Employee',23),(2,'App\\Models\\Employee',24),(2,'App\\Models\\Employee',25),(2,'App\\Models\\Employee',26),(2,'App\\Models\\Employee',27),(2,'App\\Models\\Employee',28),(2,'App\\Models\\Employee',29),(2,'App\\Models\\Employee',30),(2,'App\\Models\\Employee',31),(2,'App\\Models\\Employee',32),(2,'App\\Models\\Employee',33),(2,'App\\Models\\Employee',34),(2,'App\\Models\\Employee',35),(2,'App\\Models\\Employee',36),(2,'App\\Models\\Employee',37),(2,'App\\Models\\Employee',38),(2,'App\\Models\\Employee',39),(2,'App\\Models\\Employee',40),(2,'App\\Models\\Employee',41),(2,'App\\Models\\Employee',42),(2,'App\\Models\\Employee',43),(2,'App\\Models\\Employee',44),(2,'App\\Models\\Employee',45),(2,'App\\Models\\Employee',46),(2,'App\\Models\\Employee',47),(2,'App\\Models\\Employee',48),(2,'App\\Models\\Employee',49),(2,'App\\Models\\Employee',50),(2,'App\\Models\\Employee',51),(2,'App\\Models\\Employee',52);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `months`
--

DROP TABLE IF EXISTS `months`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `months` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `days` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `months`
--

LOCK TABLES `months` WRITE;
/*!40000 ALTER TABLE `months` DISABLE KEYS */;
INSERT INTO `months` VALUES (1,'Janvier',31,'2025-01-25 11:23:45','2025-01-25 11:23:45'),(2,'Février',28,'2025-01-25 11:23:45','2025-01-25 11:23:45'),(3,'Mars',31,'2025-01-25 11:23:45','2025-01-25 11:23:45'),(4,'Avril',30,'2025-01-25 11:23:45','2025-01-25 11:23:45'),(5,'Mai',31,'2025-01-25 11:23:45','2025-01-25 11:23:45'),(6,'Juin',30,'2025-01-25 11:23:45','2025-01-25 11:23:45'),(7,'Juillet',31,'2025-01-25 11:23:46','2025-01-25 11:23:46'),(8,'Août',31,'2025-01-25 11:23:46','2025-01-25 11:23:46'),(9,'Septembre',30,'2025-01-25 11:23:46','2025-01-25 11:23:46'),(10,'Octobre',31,'2025-01-25 11:23:46','2025-01-25 11:23:46'),(11,'Novembre',30,'2025-01-25 11:23:46','2025-01-25 11:23:46'),(12,'Décembre',31,'2025-01-25 11:23:46','2025-01-25 11:23:46');
/*!40000 ALTER TABLE `months` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) NOT NULL,
  `type` varchar(255) NOT NULL,
  `notifiable_type` varchar(255) NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES ('846fea62-e9fb-4628-a397-14c9f8debf87','App\\Notifications\\DemandeConge','App\\Models\\User',1,'{\"id\":2,\"title\":\"Un conge a ete demande par l\'employe : \",\"user\":\"mowloud\"}',NULL,'2025-01-25 12:29:56','2025-01-25 12:29:56');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets_employees`
--

DROP TABLE IF EXISTS `password_resets_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets_employees` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `code` varchar(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_employees_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets_employees`
--

LOCK TABLES `password_resets_employees` WRITE;
/*!40000 ALTER TABLE `password_resets_employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ordre` varchar(255) NOT NULL,
  `salaire_brut_mensuel` decimal(10,2) NOT NULL,
  `cnam_employee` decimal(10,2) NOT NULL,
  `cnss_employee` decimal(10,2) NOT NULL,
  `its` decimal(10,2) NOT NULL,
  `salaire_net` decimal(10,2) NOT NULL,
  `cnam_patron` decimal(10,2) NOT NULL,
  `medecin_travail` decimal(10,2) NOT NULL,
  `cnss_patron` decimal(10,2) NOT NULL,
  `conges` decimal(10,2) NOT NULL,
  `total_cout` decimal(10,2) NOT NULL,
  `attendance_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_attendance_id_foreign` (`attendance_id`),
  CONSTRAINT `payments_attendance_id_foreign` FOREIGN KEY (`attendance_id`) REFERENCES `attendances` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'PAY-001',21800.00,872.00,70.00,2814.50,18043.50,1090.00,436.00,1050.00,1816.67,26192.67,17,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(2,'PAY-002',19723.81,788.95,70.00,2316.22,16548.64,986.19,394.48,1050.00,1816.67,23971.15,26,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(3,'PAY-003',15571.43,622.86,70.00,1331.79,13546.78,778.57,311.43,1050.00,1816.67,19528.10,16,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(4,'PAY-004',18685.71,747.43,70.00,2067.07,15801.21,934.29,373.71,1050.00,1816.67,22860.38,15,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(5,'PAY-005',19723.81,788.95,70.00,2316.22,16548.64,986.19,394.48,1050.00,1816.67,23971.15,20,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(6,'PAY-006',21800.00,872.00,70.00,2814.50,18043.50,1090.00,436.00,1050.00,1816.67,26192.67,14,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(7,'PAY-007',11419.05,456.76,70.00,733.84,10158.45,570.95,228.38,1050.00,1816.67,15085.05,22,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(8,'PAY-008',21800.00,872.00,70.00,2814.50,18043.50,1090.00,436.00,1050.00,1816.67,26192.67,19,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(9,'PAY-009',17647.62,705.90,70.00,1817.93,15053.79,882.38,352.95,1050.00,1816.67,21749.62,24,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(10,'PAY-010',21800.00,872.00,70.00,2814.50,18043.50,1090.00,436.00,1050.00,1816.67,26192.67,25,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(11,'PAY-011',29285.71,1171.43,70.00,4767.72,23276.56,1464.29,585.71,1050.00,3416.67,35802.38,23,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(12,'PAY-012',19723.81,788.95,70.00,2316.22,16548.64,986.19,394.48,1050.00,1816.67,23971.15,18,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(13,'PAY-013',19723.81,788.95,70.00,2316.22,16548.64,986.19,394.48,1050.00,1816.67,23971.15,21,'2025-01-25 12:07:55','2025-01-25 12:07:55'),(14,'PAY-002',21800.00,872.00,70.00,2814.50,18043.50,1090.00,436.00,1050.00,1816.67,26192.67,27,'2025-01-25 12:23:57','2025-01-25 12:23:57'),(15,'PAY-003',18685.71,747.43,70.00,2067.07,15801.21,934.29,373.71,1050.00,1816.67,22860.38,28,'2025-01-25 12:26:26','2025-01-25 12:26:26');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'departements','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(2,'list_departement','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(3,'ajout_departement','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(4,'modifier_departement','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(5,'supprimer_departement','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(6,'positions','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(7,'list_position','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(8,'ajout_position','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(9,'modifier_position','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(10,'supprimer_position','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(11,'emploiyes','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(12,'list_emploiye','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(13,'ajout_emploiye','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(14,'modifier_emploiye','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(15,'supprimer_emploiye','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(16,'utilisateurs','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(17,'list_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(18,'ajout_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(19,'modifier_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(20,'supprimer_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(21,'voir_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(22,'roles_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(23,'list_roles_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(24,'ajout_roles_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(25,'modifier_roles_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(26,'supprimer_roles_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(27,'voir_roles_utilisateur','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(28,'conges','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(29,'list_conges','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(30,'ajout_conges','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(31,'modifier_conges','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(32,'supprimer_conges','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(33,'presences','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(34,'list_presences','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(35,'ajout_presences','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(36,'modifier_presences','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(37,'supprimer_presences','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(38,'paiements','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(39,'list_paiements','web','2025-01-25 11:23:46','2025-01-25 11:23:46'),(40,'ajout_paiements','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(41,'annuler_paiements','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(42,'bultins','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(43,'list_emploiyee_bultin','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(44,'imprimer_bultin_emploiyee','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(45,'imprimer_bultin_patron','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(46,'factures','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(47,'list_factures','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(48,'ajout_factures','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(49,'modifier_factures','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(50,'supprimer_factures','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(51,'lettre','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(52,'create_lettre','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(53,'notifications','web','2025-01-25 11:23:47','2025-01-25 11:23:47'),(54,'getInfoEmploiyeeById','employee','2025-01-25 11:23:47','2025-01-25 11:23:47'),(55,'getInfoEmploiyeedetailsById','employee','2025-01-25 11:23:47','2025-01-25 11:23:47'),(56,'getInfoCongesEmploiyeeById','employee','2025-01-25 11:23:47','2025-01-25 11:23:47'),(57,'getInfoEmploiyeeBulletinById','employee','2025-01-25 11:23:47','2025-01-25 11:23:47'),(58,'getInfoPointagesEmploiyeeById','employee','2025-01-25 11:23:47','2025-01-25 11:23:47');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `base_salary` decimal(8,2) NOT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `positions_department_id_foreign` (`department_id`),
  CONSTRAINT `positions_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (1,'Ouvrier','Effectue des tâches manuelles et contribue aux activités de base.',21800.00,5,NULL,NULL),(2,'Géologue','',21800.00,1,NULL,NULL),(3,'Traducteur','Responsable de la traduction des documents et communications.',45000.00,1,NULL,NULL),(4,'Administrateur de Camp','Supervise les opérations et la gestion des camps.',55000.00,8,NULL,NULL),(5,'Electricien','Effectue des installations et des réparations électriques.',41000.00,8,NULL,NULL),(6,'Géophisicien','Analyse les données géophysiques pour des projets scientifiques et industriels.',67000.00,5,NULL,NULL),(7,'Opérateur Géophysique','Utilise des équipements pour collecter des données géophysiques sur le terrain.',50000.00,5,NULL,NULL),(8,'Technicien Géophysique','Assiste les géophysiciens dans les analyses et les relevés techniques.',41000.00,5,NULL,NULL),(9,'Mécanicien supérieur','Effectue la maintenance et la réparation d’équipements complexes.',41000.00,8,NULL,NULL),(10,'Soudeur','Assemble des pièces métalliques en utilisant diverses techniques de soudage.',30600.00,8,NULL,NULL),(11,'Administrateur','Gère les opérations administratives et les tâches organisationnelles.',55000.00,1,NULL,NULL),(12,'Femme de Ménage','Assure la propreté et l’entretien des espaces.',10000.00,7,NULL,NULL),(13,'Gardien','Surveille et sécurise les lieux et propriétés.',10000.00,7,NULL,NULL);
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,2),(55,2),(56,2),(57,2),(58,2);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'owner','web','2025-01-25 11:23:48','2025-01-25 11:23:48'),(2,'employee','employee','2025-01-25 11:23:48','2025-01-25 11:23:48');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `roles_name` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@gmail.com',NULL,'$2y$10$YJyvG2FfE3Zjl2zeE1XH1.Q6jXB4XGRV1rJ5FYH/gWLzdVKgi5ysi','[\"owner\"]','activer',NULL,'2025-01-25 11:23:48','2025-01-25 11:23:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-01-25 14:30:03
