@extends('layouts.master')
<style>
    .hover-effect {
    transition: transform 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
}

.hover-effect:hover {
    transform: scale(1.09);
    box-shadow: 0 6px 15px rgba(0, 0, 0, 0.2);
    background-color: rgb(252, 252, 252);
}
</style>
@section('content')
<div class="container mt-5">
    <!-- Section du tableau de bord principal -->
    <div class="text-center mb-5">
        <h1 class="text-primary font-weight-bold">Tableau de Bord</h1>
        <p class="text-muted">Suivi Financier et Ressources Humaines de l'Entreprise</p>
    </div>

    <!-- Section des Statistiques Globales -->
    <div class="row justify-content-center mb-4">
        <div class="col-md-3 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-primary">Salaires de Base</h5>
                    <p class="display-5 font-weight-bold text-dark">
                        {{ number_format($totalSalaries, 2) }} 
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-primary">Salaires Bruts</h5>
                    <p class="display-5 font-weight-bold text-dark">
                        {{ number_format($totalSalariesBrut, 2) }}
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-primary">Coût Total</h5>
                    <p class="display-5 font-weight-bold text-dark">
                        {{ number_format($totalCout, 2) }}
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Section des Cotisations et Impôts -->
    <div class="row mb-4">
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-secondary">Cotisations CNAM</h5>
                    <p class="display-6 font-weight-bold text-dark">
                        {{ number_format($totalCnamEmployee, 2) }}
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-secondary">Cotisations CNSS</h5>
                    <p class="display-6 font-weight-bold text-dark">
                        {{ number_format($totalCnssEmployee, 2) }}
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-secondary">ITS</h5>
                    <p class="display-6 font-weight-bold text-dark">
                        {{ number_format($totalIts, 2) }}
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Section des Employés -->
    <div class="row mb-4">
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-success">Employés CDI</h5>
                    <p class="display-6 font-weight-bold">{{ $employeesCdi }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-success">Employés CDD</h5>
                    <p class="display-6 font-weight-bold">{{ $employeesCdd }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-success">Total Employés</h5>
                    <p class="display-6 font-weight-bold">{{ $totalEmployees }}</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Section des Factures -->
    <div class="row mb-4">
        <div class="col-md-6 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-info">Total Factures</h5>
                    <p class="display-6 font-weight-bold text-dark">
                        {{ number_format($totalInvoicesAmount, 2) }}
                        <span class="text-dark">MRU</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-info">Nombre de Factures</h5>
                    <p class="display-6 font-weight-bold">{{ $totalInvoicesCount }}</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Section des Départements et Postes -->
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-warning">Départements</h5>
                    <p class="display-6 font-weight-bold">{{ $totalDepartments }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-warning">Postes</h5>
                    <p class="display-6 font-weight-bold">{{ $totalPositions }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="card  border-0 rounded-lg shadow-sm hover-effect">
                <div class="card-body text-center">
                    <h5 class="text-warning">Employés en Congé</h5>
                    <p class="display-6 font-weight-bold">{{ $employeesOnLeave }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
