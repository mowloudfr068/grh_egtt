@extends('layouts.master')

@section('css')
<style>
    .select2-container {
        width: 100% !important;
    }
    .select2-selection {
        height: 50px !important;
    }
    .card {
        margin: 20px 0;
    }
</style>
@endsection

@section('title')
    {{ __('Détails du Congé') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Accueil') }}</a>/<a href="#">{{ __('Congés/Demande') }}</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_conges')
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editLeaveModal">
                    <i class="fas fa-edit"></i> {{ __('Modifier la demande') }}
                </button>
                @endcan
            </div>
        </div>
    </div>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header bg-success text-white">
            <h4>{{ __('Détails du Congé') }}</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>{{ __('Employé') }} :</strong> {{ $leave->employee->first_name }} {{ $leave->employee->last_name }} {{ " de matricule : ".$leave->employee->matricule }}</p>
                    <p><strong>{{ __('Date de début') }} :</strong> {{ $leave->start_date }}</p>
                    <p><strong>{{ __('Date de fin') }} :</strong> {{ $leave->end_date }}</p>
                </div>
                <div class="col-md-6">
                    <p><strong>{{ __('Type') }} :</strong> {{ $leave->type }}</p>
                    <p><strong>{{ __('Statut') }} :</strong> 
                        <span class="badge {{ $leave->status == 'Approuvé' ? 'badge-success' : ($leave->status == 'Refusé' ? 'badge-danger' : 'badge-warning') }}">
                            {{ $leave->status }}
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editLeaveModal" tabindex="-1" role="dialog" aria-labelledby="editLeaveModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editLeaveModalLabel">{{ __('Modifier Congé') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('leaves.update',$leave->id) }}" method="post">
                        @method('patch')
                        @csrf
                        <input type="hidden" name="employee_id" value="{{ $leave->employee_id }}">
                        <input type="hidden" name="id" value="{{ $leave->id }}">
                        <div class="form-group">
                            <label for="start_date">{{ __('Date de début') }}</label>
                            <input type="date" name="start_date" class="form-control" value="{{ $leave->start_date }}" required>
                        </div>
                        <div class="form-group">
                            <label for="end_date">{{ __('Date de fin') }}</label>
                            <input type="date" name="end_date" class="form-control" value="{{ $leave->end_date }}" required>
                        </div>
                        <div class="form-group">
                            <label for="type">{{ __('Type') }}</label>
                            <input type="text" name="type" class="form-control" value="{{ $leave->type }}" required>
                        </div>
                        <div class="form-group">
                            <label for="status">{{ __('Statut') }}</label>
                            <select name="status" class="form-control select2">
                                <option value="En Attente" {{ $leave->status == 'En Attente' ? 'selected' : '' }}>{{ __('En Attente') }}</option>
                                <option value="Approuvé" {{ $leave->status == 'Approuvé' ? 'selected' : '' }}>{{ __('Approuvé') }}</option>
                                <option value="Refusé" {{ $leave->status == 'Refusé' ? 'selected' : '' }}>{{ __('Refusé') }}</option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Modifier') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
