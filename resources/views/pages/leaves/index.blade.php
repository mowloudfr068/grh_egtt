@extends('layouts.master')
@section('css')
<style>
    .select2-container {
    width: 100% !important;
}
.select2-selection {
    height: 50px !important;
}
</style>
@endsection
@section('title')
    {{ __('Congés') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Home') }}</a>/<a href="#">{{ __('Congés') }}</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_conges')
                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addLeaveModal">
                    <i class="fas fa-add"></i>  {{ __('Ajouter Congé') }}
                </button>   
                @endcan
                
            </div>
        </div>
    </div>
@endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Employé') }}</th>
                <th>{{ __('Date de début') }}</th>
                <th>{{ __('Date de fin') }}</th>
                <th>{{ __('Type') }}</th>
                <th>{{ __('Statut') }}</th>
                <th class="text-center">{{ __('Opérations') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($leaves as $leave)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $leave->employee->first_name }} {{ $leave->employee->last_name }}</td>
                    <td>{{ $leave->start_date }}</td>
                    <td>{{ $leave->end_date }}</td>
                    <td>{{ $leave->type }}</td>
                    <td>{{ $leave->status }}</td>
                    <td class="text-center">
                        @can('modifier_conges')
                            
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editLeaveModal{{ $leave->id }}">
                            <i class="fas fa-edit"></i>
                        </button>
                        @endcan
                        @can('supprimer_conges')  
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteLeaveModal{{ $leave->id }}">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                        @endcan
                    </td>
                </tr>

                <!-- Edit Modal -->
                <div class="modal fade" id="editLeaveModal{{ $leave->id }}" tabindex="-1" role="dialog" aria-labelledby="editLeaveModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editLeaveModalLabel">{{ __('Modifier Congé') }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('leaves.update',$leave->id) }}" method="post">
                                    @method('patch')
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $leave->id }}">
                                    <div class="form-group">
                                        <label for="employee_id">{{ __('Employé') }}</label>
                                        <select name="employee_id" class="form-control select2" required>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}" {{ $leave->employee_id == $employee->id ? 'selected' : '' }}>
                                                    {{ $employee->first_name }} {{ $employee->last_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="start_date">{{ __('Date de début') }}</label>
                                        <input type="date" name="start_date" class="form-control" value="{{ $leave->start_date }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="end_date">{{ __('Date de fin') }}</label>
                                        <input type="date" name="end_date" class="form-control" value="{{ $leave->end_date }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">{{ __('Type') }}</label>
                                        <input type="text" name="type" class="form-control" value="{{ $leave->type }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">{{ __('Statut') }}</label>
                                        <select name="status" class="form-control select2">
                                            <option value="En Attente" {{ $leave->status == 'En Attente' ? 'selected' : '' }}>{{ __('En Attente') }}</option>
                                            <option value="Approuvé" {{ $leave->status == 'Approuvé' ? 'selected' : '' }}>{{ __('Approuvé') }}</option>
                                            <option value="Refusé" {{ $leave->status == 'Refusé' ? 'selected' : '' }}>{{ __('Refusé') }}</option>
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-success">{{ __('Modifier') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Delete Modal -->
                <div class="modal fade" id="deleteLeaveModal{{ $leave->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteLeaveModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteLeaveModalLabel">{{ __('Supprimer Congé') }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('leaves.destroy',$leave->id) }}" method="post">
                                    @method('delete')
                                    @csrf
                                    {{ __('Êtes-vous sûr de vouloir supprimer ce congé?') }}
                                    <input type="hidden" name="id" value="{{ $leave->id }}">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-danger">{{ __('Supprimer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    <!-- Add Modal -->
    <div class="modal fade" id="addLeaveModal" tabindex="-1" role="dialog" aria-labelledby="addLeaveModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addLeaveModalLabel">{{ __('Ajouter Congé') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('leaves.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="employee_id">{{ __('Employé') }}</label>
                            <select name="employee_id" class="form-control select2" required>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start_date">{{ __('Date de début') }}</label>
                            <input type="date" name="start_date" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="end_date">{{ __('Date de fin') }}</label>
                            <input type="date" name="end_date" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="type">{{ __('Type') }}</label>
                            <input type="text" name="type" class="form-control" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Enregistrer') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
        // Initialiser Select2
        $('.select2').select2({
            placeholder: "Sélectionner ...",  // Placeholder pour le champ
            allowClear: true,  // Permet de supprimer la sélection
            width: '100%', 
        });
    });

        $(document).ready(function() {
            $('table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
