@extends('layouts.master')
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<style>

    
    .select2-container {
    width: 100% !important;
}
.select2-selection {
    height: 50px !important;
}
</style>
@endsection
@section('title')
    {{ __('Employés') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Accueil') }}</a>/<a href="#">{{ __('Employés') }}</a>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_emploiye')
                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addEmployeeModal">
                    <i class="fas fa-add"></i>  {{ __('Ajouter Employé') }}
                </button>   
                @endcan
                
            </div>
        </div>

        @if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{ $message }}</p>
</div>
@endif
        
    </div>
@endsection
@section('content')

    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Nni') }}</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Prénom') }}</th>
                <th>{{ __('Nom') }}</th>
                <th>{{ __('Email') }}</th>
                <th>{{ __('Téléphone') }}</th>
                <th>{{ __('Position') }}</th>
                <th>{{ __('Département') }}</th>
                <th>{{ __('Contrat') }}</th>
                <th>{{ __('Salaire') }}</th>
                <th class="text-center dt-no-sorting">{{ __('Opérations') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $employee->nni }}</td>
                    <td>{{ $employee->matricule }}</td>
                    <td>{{ $employee->first_name }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{ $employee->email }}</td>
                    <td>{{ $employee->phone }}</td>
                    <td>{{ $employee->position->title }}</td>
                    <td>{{ $employee->position->department->name }}</td>
                    <td>
                        <p><strong>Type de contrat :</strong> {{ $employee->contract_type }}</p>
@if ($employee->contract_type === 'CDD')
    <p><strong>Durée du contrat :</strong> {{ $employee->contract_duration }} ans</p>

@endif

    
    @if($employee->contract_type === 'CDD')
        <a href="{{ route('contracts.cdd', $employee->id) }}" class="btn btn-warning btn-sm" title="Télécharger Contrat CDD">
            <i class="fas fa-download"></i> CDD
        </a>
    @else

    <a href="{{ route('contracts.cdi', $employee->id) }}" class="btn btn-primary btn-sm" title="Télécharger Contrat CDI">
        <i class="fas fa-download"></i> CDI
    </a>
    @endif


                    </td>
                    <td>{{ $employee->position->base_salary." MRU" }} </td>
                    <td class="text-center">
                        @can('modifier_emploiye')
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                        data-target="#edit{{ $employee->id }}" title="{{ __('Modifier') }}">
                    
                        <i class="fas fa-edit"></i>
                    </button>   
                        @endcan
                        
                        @can('supprimer_emploiye')
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                            data-target="#delete{{ $employee->id }}" title="{{ __('Supprimer') }}">
                        
                            <i class="fas fa-trash-alt"></i>
                        </button>    
                        @endcan
                        
                    </td>
                </tr>
               <!-- edit_modal_employee -->
<div class="modal fade" id="edit{{ $employee->id }}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                    {{ __('Modifier Employé') }}
                </h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('employees.update', $employee->id) }}" method="post">
                    @method('patch')
                    @csrf
                    <input type="hidden" name="id" class="form-control" value="{{ $employee->id }}" required>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nni">{{ __('NNI') }}:</label>
                                <input type="text" name="nni" class="form-control" value="{{ $employee->nni }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">{{ __('Prénom') }}:</label>
                                <input type="text" name="first_name" class="form-control" value="{{ $employee->first_name }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">{{ __('Nom') }}:</label>
                                <input type="text" name="last_name" class="form-control" value="{{ $employee->last_name }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">{{ __('Email') }}:</label>
                                <input type="email" name="email" class="form-control" value="{{ $employee->email }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">{{ __('Téléphone') }}:</label>
                                <input type="text" name="phone" class="form-control" value="{{ $employee->phone }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">{{ __('Adresse') }}:</label>
                                <input type="text" name="address" class="form-control" value="{{ $employee->address }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">{{ __('Date de naissance') }}:</label>
                                <input type="date" name="date_of_birth" class="form-control" value="{{ $employee->date_of_birth }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_hire">{{ __('Date d\'embauche') }}:</label>
                                <input type="date" name="date_of_hire" class="form-control" value="{{ $employee->date_of_hire }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="contract_type">{{ __('Type de contrat') }}:</label>
                        <select name="contract_type" id="edit_contract_type" class="form-control" onchange="edittoggleDurationField(this.value)">
                            <option value="CDI" {{ $employee->contract_type === 'CDI' ? 'selected' : '' }}>CDI</option>
                            <option value="CDD" {{ $employee->contract_type === 'CDD' ? 'selected' : '' }}>CDD</option>
                        </select>
                    </div>
                    
                    <div class="form-group" id="edit_duration_field" style="display: none;">
                        <label for="contract_duration">{{ __('Durée du contrat (en années)') }}</label>
                        <input type="number" name="contract_duration" id="edit_contract_duration" class="form-control" 
                            value="{{ $employee->contract_duration }}" min="1" step="1">
                    </div>

                    <div class="form-group">
                        <label for="department_id">{{ __('Département') }}:</label>
                        <select name="department_id" class="form-control select2">
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}" {{ $employee->position->department->id == $department->id ? 'selected' : '' }}>
                                    {{ $department->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="position_id">{{ __('Position') }}:</label>
                        <select name="position_id" class="form-control select2">
                            @foreach($positions as $position)
                                <option value="{{ $position->id }}" {{ $employee->position_id == $position->id ? 'selected' : '' }}>
                                    {{ $position->title }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Modifier') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



                <!-- delete_modal_employee -->
                <div class="modal fade" id="delete{{ $employee->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('Supprimer Employé') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('employees.destroy', $employee->id) }}" method="post">
                                    @method('delete')
                                    @csrf
                                    {{ __('Êtes-vous sûr de vouloir supprimer cet employé ?') }}
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-danger">{{ __('Supprimer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            
        </tbody>
    </table>

    <!-- add_modal_employee -->
<div class="modal fade" id="addEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                    {{ __('Ajouter Employé') }}
                </h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('employees.store') }}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nni">{{ __('NNI') }}:</label>
                            <input type="number" name="nni" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="first_name">{{ __('Prénom') }}:</label>
                            <input type="text" name="first_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="last_name">{{ __('Nom') }}:</label>
                            <input type="text" name="last_name" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">{{ __('Email') }}:</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="phone">{{ __('Téléphone') }}:</label>
                            <input type="number" name="phone" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address">{{ __('Adresse') }}:</label>
                            <input type="text" name="address" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="date_of_birth">{{ __('Date de naissance') }}:</label>
                            <input type="date" name="date_of_birth" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="date_of_hire">{{ __('Date d\'embauche') }}:</label>
                            <input type="date" name="date_of_hire" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="contract_type">{{ __('Type de contrat') }}:</label>
                        <select name="contract_type" id="contract_type" class="form-control" onchange="toggleDurationField(this.value)">
                            <option value="CDI" >CDI</option>
                            <option value="CDD" >CDD</option>
                        </select>
                    </div>
                    
                    <div class="form-group" id="duration_field" style="display: none;">
                        <label for="contract_duration">{{ __('Durée du contrat (en années)') }}:</label>
                        <input type="number" name="contract_duration" id="contract_duration" class="form-control"  min="1" step="1">
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="department_id">{{ __('Département') }}:</label>
                            <select name="department_id" id="department_id" class="form-control select2" required>
                                <option value="">{{ __('Sélectionner un département') }}</option>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="position_id">{{ __('Position') }}:</label>
                            <select name="position_id" id="position_id" class="form-control select2" required>
                                <option value="">{{ __('Sélectionner une position') }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                        <button type="submit" class="btn btn-success">{{ __('Enregistrer') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
    <script>

function toggleDurationField(contractType) {
        const durationField = document.getElementById('duration_field');
        if (contractType === 'CDD') {
            durationField.style.display = 'block';
        } else {
            durationField.style.display = 'none';
        }
    }

    // Initialiser l'affichage du champ lors du chargement
    document.addEventListener('DOMContentLoaded', () => {
        toggleDurationField(document.getElementById('contract_type').value);
    });

    
    function edittoggleDurationField(contractType) {
        const durationField = document.getElementById('edit_duration_field');
        if (contractType === 'CDD') {
            durationField.style.display = 'block';
        } else {
            durationField.style.display = 'none';
        }
    }

    document.addEventListener('DOMContentLoaded', () => {
        edittoggleDurationField(document.getElementById('edit_contract_type').value);
    });

$(document).ready(function() {
        // Initialiser Select2
        $('.select2').select2({
            placeholder: "Sélectionner ...",  // Placeholder pour le champ
            allowClear: true,  // Permet de supprimer la sélection
            width: '100%', 
        });
    });

$(document).ready(function() {
        $('#department_id').on('change', function() {
            var departmentId = $(this).val();
            var positionSelect = $('#position_id');

            // Vider le champ des positions
            positionSelect.empty().append('<option value="">{{ __("Sélectionner une position") }}</option>');

            if (departmentId) {
                $.ajax({
                    url: "{{ route('positions.by.department', '') }}/" + departmentId,
                    type: "GET",
                    success: function(data) {
                        if (data.length > 0) {
                            data.forEach(function(position) {
                                positionSelect.append(
                                    `<option value="${position.id}">${position.title}</option>`
                                );
                            });
                        } else {
                            positionSelect.append('<option value="">{{ __("Aucune position disponible") }}</option>');
                        }
                    },
                    error: function() {
                        alert("{{ __('Erreur lors de la récupération des données') }}");
                    }
                });
            }
        });
    });


    $(document).ready(function() {
            $('select[name="department_id"]').on('change', function() {
                var department_id = $(this).val();
                if (department_id) {
                    $.ajax({
                        url:"{{ route('positionsBydepartment','') }}/" + department_id,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            $('select[name="position_id"]').empty();
                            $.each(data, function(key,value) {
                                $('select[name="position_id"]').append('<option value="' +
                                    key + '">' + value + '</option>');
                            });
                        },
                    });
                } else {
                    console.log('AJAX erreur');
                }
            });
        });

    // -----------end edit get position by departemnt----------
        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
