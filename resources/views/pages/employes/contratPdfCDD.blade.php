<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contrat CDD</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            margin: 0;
            padding: 0;
        }
        .container {
            margin: 20px;
        }
        .header {
            text-align: center;
            margin-bottom: 30px;
        }
        .header h1 {
            margin: 0;
            font-size: 24px;
        }
        .content {
            text-align: justify;
        }
        .section {
            margin-bottom: 15px;
        }
        .signature {
            margin-top: 50px;
            display: flex;
            justify-content: space-between;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Contrat de Travail – CDD</h1>
        </div>
        <div class="content">
            <p>
                Entre les soussignés :<br>
                <strong>L’Entreprise :</strong> {{ $company_name }}, représentée par {{ $employer_name }}.<br>
                Et<br>
                <strong>L’Employé :</strong> {{ $employee_name }}.
            </p>
            <div class="section">
                <p><strong>1. Nature du contrat :</strong></p>
                <p>Ce contrat est conclu pour une durée déterminée de <strong>{{ $duration }} ans</strong>, renouvelable par accord mutuel écrit entre les parties.</p>
            </div>
            <div class="section">
                <p><strong>2. Poste et responsabilités :</strong></p>
                <p>L’employé sera affecté au poste de <strong>{{ $position }}</strong> dans le département <strong>{{ $department }}</strong>. Il s’engage à exécuter ses fonctions avec professionnalisme et diligence, conformément aux instructions de l’employeur.</p>
            </div>
            <div class="section">
                <p><strong>3. Salaire :</strong></p>
                <p>L’employé percevra un salaire mensuel brut de <strong>{{ $salary }} MRU</strong>, payable à la fin de chaque mois. Ce salaire inclut toutes les cotisations sociales et fiscales conformément à la législation en vigueur.</p>
            </div>
            <div class="section">
                <p><strong>4. Durée hebdomadaire de travail :</strong></p>
                <p>La durée de travail hebdomadaire est fixée à 40 heures, réparties selon l’emploi du temps défini par l’entreprise.</p>
            </div>
            <div class="section">
                <p><strong>5. Date d’entrée en fonction :</strong></p>
                <p>L’employé prendra ses fonctions à compter du <strong>{{ $start_date }}</strong>. Tout retard ou absence devra être justifié conformément aux règlements internes.</p>
            </div>
            <div class="section">
                <p><strong>6. Confidentialité et non-concurrence :</strong></p>
                <p>L’employé s’engage à respecter la confidentialité des informations relatives à l’entreprise et à ne pas exercer d’activité concurrente pendant la durée de ce contrat.</p>
            </div>
            <div class="section">
                <p><strong>7. Résiliation du contrat :</strong></p>
                <p>Ce contrat peut être résilieé par l’une ou l’autre des parties moyennant un préavis écrit de 15 jours, sauf en cas de faute grave.</p>
            </div>
        </div>
        <div class="signature">
            <div>
                <p>Signature de l’Employeur :</p>
                <p>________________________</p>
            </div>
            <div>
                <p>Signature de l’Employé :</p>
                <p>________________________</p>
            </div>
        </div>
    </div>
</body>
</html>
