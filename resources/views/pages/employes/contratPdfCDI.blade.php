<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contrat CDI</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            margin: 0;
            padding: 0;
        }
        .container {
            margin: 20px;
        }
        .header {
            text-align: center;
            margin-bottom: 30px;
        }
        .header h1 {
            margin: 0;
            font-size: 24px;
        }
        .content {
            text-align: justify;
        }
        .signature {
            margin-top: 50px;
            display: flex;
            justify-content: space-between;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1>Contrat de Travail - CDI</h1>
        </div>
        <div class="content">
            <p>
                Entre les soussignés : <br>
                <strong>L'Entreprise :</strong> {{ $company_name }}, représentée par {{ $employer_name }}.<br>
                Et <br>
                <strong>L'Employé :</strong> {{ $employee_name }}.
            </p>
            <p>
                Il est convenu ce qui suit :
            </p>
            <p>
                <strong>1. Nature du contrat :</strong><br>
                Ce contrat est conclu pour une durée indéterminée.
            </p>
            <p>
                <strong>2. Poste et responsabilités :</strong><br>
                L'employé sera affecté au poste de <strong>{{ $position }}</strong> 
                dans le département <strong>{{ $department }}</strong>.
            </p>
            <p>
                <strong>3. Salaire :</strong><br>
                L'employé percevra un salaire mensuel de <strong>{{ $salary }} MRU</strong>.
            </p>
            <p>
                <strong>4. Durée hebdomadaire de travail :</strong><br>
                La durée de travail hebdomadaire est fixée à 40 heures.
            </p>
            <p>
                <strong>5. Date d'entrée en fonction :</strong><br>
                L'employé prendra ses fonctions le <strong>{{ $start_date }}</strong>.
            </p>
        </div>
        <div class="signature">
            <div>
                <p>Signature de l'Employeur :</p>
                <p>________________________</p>
            </div>
            <div>
                <p>Signature de l'Employé :</p>
                <p>________________________</p>
            </div>
        </div>
    </div>
</body>
</html>
