@extends('layouts.master')
@section('css')
<style>
     .select2-container {
        width: 100% !important;
    }

    .select2-selection {
        height: 45px !important;
    }

</style>
@endsection
@section('title')
    {{ __('Liste des Pointages') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center">{{ __('Imprimer Bultin de Paie') }}</h3>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <form action="{{ route('bulletin.generate') }}" method="get">
        <div class="form-row mb-3">
            <div class="col-md-4">
                <label for="month_id">{{ __('Mois') }}</label>
                <select class="form-control select2" onchange="this.form.submit()" id="month_id" name="month_id">
                    @foreach ($months as $month)
                        <option value="{{ $month->id }}" {{ $month->id == $selectedMonthId ? 'selected' : '' }}>
                            {{ $month->libelle }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label for="year">{{ __('Année') }}</label>
                <input type="number" class="form-control" id="year" name="year" value="{{ $selectedYear }}" min="2000">
            </div>
        </div>
    </form>

    <table class="table table-striped table-bordered datatable">
        <thead>
            <tr>
                <th>{{ __('Nom complet') }}</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Jours travaillés') }}</th>
                <th>{{ __('Détails') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                    <td>{{ $employee->matricule }}</td>
                
                    @php
                        $workDays = 0;
                        $dailyDetails = [];
                        $employeeDays = $attendanceData[$employee->id] ?? [];
                    @endphp
                    @for ($day = 1; $day <= $selectedMonthDays; $day++)
                        @php
                            $isPresent = in_array($day, $employeeDays);
                            if ($isPresent) {
                                $workDays++;
                            } 
                        @endphp
                    @endfor
                    <td>{{ $workDays }}</td>
                    <td>
                        <div class="d-inline-flex align-items-center">
                            
                            @can('imprimer_bultin_emploiyee')
                                
                            <form action="{{ route('bulltin.show') }}" method="POST" class="">
                                @csrf
                                <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                                <input type="hidden" name="month_id" value="{{ $selectedMonthId }}">
                                <input type="hidden" name="anne" value="{{ $selectedYear }}">
                                
                                <button type="submit" class="btn btn-success btn-sm">
                                 <i class="fas fa-print"></i>   {{ __('Bultin Emploiye') }}
                                </button>
                            </form>
                            @endcan
                            @can('imprimer_bultin_patron')  
                            <form action="{{ route('bulltin.showPatron') }}" method="POST" class="m-1">
                                @csrf
                                <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                                <input type="hidden" name="month_id" value="{{ $selectedMonthId }}">
                                <input type="hidden" name="anne" value="{{ $selectedYear }}">
                                
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fas fa-print"></i>   {{ __('Bultin Patron') }}
                                </button>
                            </form>
                            @endcan
                        </div>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection


@section('js')
    <script>
        $(document).ready(function() {

            $('.select2').select2({
    placeholder: "{{ __('Sélectionner ...') }}",  
    allowClear: true,  
    width: '100%', 
});

            $('.datatable').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "{{ __('Affichage de la page _PAGE_ sur _PAGES_') }}",
                    "sSearch": "{{ __('Recherche') }}",
                    "sLengthMenu": "{{ __('Afficher _MENU_ éléments') }}",
                    "sInfoEmpty": "{{ __('Aucun résultat disponible') }}",
                    "sInfoFiltered": "{{ __('(filtré à partir de _MAX_ éléments)') }}",
                },
                "stripeClasses": []
            });
        });
    </script>
@endsection
