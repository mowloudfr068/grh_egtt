<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bulletin de Paie - Patron</title>
    <link rel="stylesheet" href="w3.css">
    <link rel="stylesheet" href="./assets/w3.css">
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            font-size: 12px;
        }
        th, td {
            padding: 10px;
            text-align: left;
            border: 1px solid #ddd;
        }
        th {
            background-color: #f2f2f2;
            font-weight: bold;
        }
        .table-header {
            background-color: #4CAF50;
            color: white;
        }
        .signature-section {
            font-size: 12px;
            margin-top: 20px;
            text-align: center;
        }
        .signature-section p {
            margin: 5px 0;
        }
        .footer-note {
            font-size: 12px;
            text-align: center;
            margin-top: 20px;
        }
    </style>
</head>
<body onload="printing()">
    <div class="w3-container">
        <!-- Titre du Bulletin de Paie -->
        <table>
            <tr>
                <th class="table-header" colspan="4">Bulletin de Paie</th>
                <th>Periode</th>
                <th>Du : {{ $startDate }} &nbsp; Au : {{ $endDate }}</th>
                <th colspan="5">Paiement du: {{ $payment->updated_at }} &nbsp;</th>
            </tr>
            <tr>
                <th rowspan="3">EGTT</th>
                <th>Matricule : {{ $payment->attendance->employee->matricule }}</th>
                <th>Nombre jours travaillés : @php
                    $daysArray = json_decode($payment->attendance->days, true);
                    $joursTravailles = is_array($daysArray) ? count($daysArray) : 0;
                @endphp
                {{ $joursTravailles }}</th>
                <th>Jours Normaux :</th>
                <th>Indice : {{ $payment->attendance->employee->position->id ?? 'N/A' }}</th>
                <th>Date d'embauche : {{ \Carbon\Carbon::parse($payment->attendance->employee->date_of_hire)->format('d/m/Y') }}</th>
                <th colspan="2">Numéro de sécurité sociale : {{ $payment->attendance->employee->nni }}</th>
            </tr>
            <tr>
                <th colspan="2">Société : {{ $companyName }}</th>
                <th colspan="2">Emploi occupé : {{ $payment->attendance->employee->position->title ?? 'N/A' }}</th>
                <th colspan="3">Département : {{ $payment->attendance->employee->position->department->name ?? 'N/A' }}</th>
            </tr>
            <tr>
                <th>NNI : {{ $payment->attendance->employee->nni }}</th>
                <th>Horaire : </th>
                <th colspan="5">Pointage : Du {{ $startDate }} au {{ $endDate }}</th>
            </tr>
            <tr>
                <th>Repos Comp / Congés : </th>
                <th>Acquis</th>
                <th>Restant à prendre : </th>
                <th>Pris : </th>
            </tr>
        </table>

        <!-- Partie des Détails des Rémunérations -->
        <table>
            <tr>
                <th class="table-header">Désignation</th>
                <th>Nombre</th>
                <th>Base</th>
                <th colspan="6">Part Patronale</th>
            </tr>
            <tr>
                <td>Salaire base brut</td>
                <td></td>
                <td>{{ number_format($payment->salaire_brut_mensuel, 2) }} UM</td>
                <td></td>
                <td>{{ number_format($payment->cnam_patron, 2) }} UM</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <!-- Autres lignes avec les primes, charges patronales etc. -->
        </table>

        <!-- Cumul des montants -->
        <table>
            <tr>
                <th>Cumul</th>
                <th>Salaire brut mensuel</th>
                <th>Net imposable</th>
                <th>Avantages en nature</th>
                <th>Heures travaillées</th>
                <th>Charges salariales</th>
                <th>Charges retenues</th>
                <th class="table-header">NET A PAYER</th>
            </tr>
            <tr>
                <td>Période<br>Annee 2024</td>
                <td>{{ number_format($payment->salaire_brut_mensuel, 2) }} UM</td>
                <td>{{ number_format($payment->salaire_net, 2) }} UM</td>
                <td></td>
                <td></td>
                <td>{{ number_format($payment->total_cout, 2) }} UM</td>
                <td>{{ number_format($payment->cnam_employee + $payment->cnss_employee, 2) }} UM</td>
                <td>{{ number_format($payment->salaire_net, 2) }} UM</td>
            </tr>
        </table>

        <!-- Signature et Information -->
        <div class="signature-section">
            <p>Fait à {{ $companyName }} le {{ $payment->updated_at }}</p>

            {{-- <img src="{{ asset('images/EGTT_CACHE.png') }}" alt=""> --}}

        </div>

        <div class="footer-note">
            <p>* Les montants sont donnés à titre indicatif et peuvent être ajustés selon les évolutions fiscales ou contractuelles.</p>

        </div>
    </div>

    <script>
        function printing() {
        
    window.print();
    
    window.setTimeout(function() {
        
        window.location.href = "{{ route('bulletin.generate') }}";
    }, 1000);
}
    </script>
</body>
</html>
