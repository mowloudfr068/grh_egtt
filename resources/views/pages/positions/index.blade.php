@extends('layouts.master')
@section('css')
<style>
/* S'assurer que le champ de recherche est visible */
.select2-container {
    width: 100% !important;
}
.select2-selection {
    height: 50px !important;
}

</style>
@endsection
@section('title')
    {{ __('Positions') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Home') }}</a>/<a href="#">{{ __('Positions') }}</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_position')
                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addPositionModal">
                    <i class="fas fa-add"></i>{{ __('Ajouter Position') }}
                </button>   
                @endcan
                
            </div>
        </div>
    </div>
@endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Titre') }}</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('Salaire de base') }}</th>
                <th>{{ __('Departement') }}</th>
                <th class="text-center dt-no-sorting">{{ __('Opérations') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($positions as $position)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $position->title }}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#descriptionModal{{ $position->id }}">
                            <i class="fas fa-eye"></i> <!-- Icône "eye" de Font Awesome -->
                        </button> 
                    </td>
                    <td>{{ $position->base_salary." MRU" }}</td>
                    <td>{{ $position->department->name }}</td>
                    <td class="text-center">
                        @can('modifier_position')
                         <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                            data-target="#edit{{ $position->id }}" title="{{ __('Modifier') }}">
                            <i class="fas fa-edit"></i>
                        </button>   
                        @endcan
                        @can('supprimer_position')
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                            data-target="#delete{{ $position->id }}" title="{{ __('Supprimer') }}">
                            <i class="fas fa-trash-alt"></i>
                            
                        </button>    
                        @endcan
                        
                    </td>
                </tr>
                <!-- Modal de description -->
<div class="modal fade" id="descriptionModal{{ $position->id }}" tabindex="-1" role="dialog" aria-labelledby="descriptionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="descriptionModalLabel">{{ __('Description de la Position') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Description affichée dans le modal -->
                <p>{{ $position->description }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
            </div>
        </div>
    </div>
</div>
                <!-- edit_modal_position -->
                <div class="modal fade" id="edit{{ $position->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('Modifier Position') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <!-- edit_form -->
                                <form action="{{ route('positions.update','test') }}" method="post">
                                    @method('patch')
                                    @csrf
                                    <input id="id" type="hidden" name="id" class="form-control"
                                                value="{{ $position->id }}">
                                    <div class="form-group">
                                        <label for="title">{{ __('Titre') }}:</label>
                                        <input type="text" name="title" class="form-control" value="{{ $position->title }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">{{ __('Description') }}:</label>
                                        <textarea name="description" class="form-control" rows="3">{{ $position->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="base_salary">{{ __('Salaire de base') }}:</label>
                                        <input type="number" name="base_salary" class="form-control" value="{{ $position->base_salary }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="department_id">{{ __('Département') }}:</label>
                                        <select name="department_id" class="form-control select2" required>
                                            @foreach($departments as $department)
                                                <option value="{{ $department->id }}" {{ $position->department_id == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-success">{{ __('Modifier') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- delete_modal_position -->
                <div class="modal fade" id="delete{{ $position->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('Supprimer Position') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('positions.destroy', $position->id) }}" method="post">
                                    @method('delete')
                                    @csrf
                                    {{ __('Êtes-vous sûr de vouloir supprimer cette position?') }}
                                    <input type="hidden" name="id" value="{{ $position->id }}">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-danger">{{ __('Supprimer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    <!-- add_modal_position -->
    <div class="modal fade" id="addPositionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                        {{ __('Ajouter Position') }}
                    </h5>
                </div>
                <div class="modal-body">
                    <!-- add_form -->
                    <form action="{{ route('positions.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="title">{{ __('Titre') }}:</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="description">{{ __('Description') }}:</label>
                            <textarea name="description" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="base_salary">{{ __('Salaire de base') }}:</label>
                            <input type="number" name="base_salary" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="department_id">{{ __('Departement') }}:</label>
                            <select name="department_id" class="form-control select2" required>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}">{{ $department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Enregistrer') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>

$(document).ready(function() {
        // Initialiser Select2
        $('.select2').select2({
            placeholder: "Sélectionner un Departement",  // Placeholder pour le champ
            allowClear: true,  // Permet de supprimer la sélection
            width: '100%', 
        });
    });

        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
