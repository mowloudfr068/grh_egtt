@extends('layouts.master')
@section('css')


@endsection
@section('title')
    {{ __('Factures') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9" >
                <div class="mt-2">
                    <a href="#">{{ __('Home') }}</a>/<a href="#">{{ __('Factures') }}</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_factures')
                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addInvoiceModal">
                    <i class="fas fa-add"></i>  {{ __('Ajouter Facture') }}
                </button>   
                @endcan
                
            </div>
        </div>
    </div>
@endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('Periode') }}</th>
                <th>{{ __('Total') }}</th>
                <th>{{ __('Nom client') }}</th>
                <th>{{ __('Tel client') }}</th>
                <th class="text-center dt-no-sorting">{{ __('Opérations') }}</th>
            </tr>
        </thead>
        
        <tbody>
            @foreach ($invoices as $invoice)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        <!-- Bouton avec icône Font Awesome -->
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#descriptionModal{{ $invoice->id }}">
                    <i class="fas fa-eye"></i> <!-- Icône "eye" de Font Awesome -->
                </button>
                    </td>
                    
                    
                    
                    <td>Du {{ $invoice->date_debut }} <div>Au {{ $invoice->date_fin }}</div></td>
                    <td>{{ $invoice->total }}</td>
                    <td>{{ $invoice->nom_client }}</td>
                    <td>{{ $invoice->tel_client }}</td>
                    <td class="text-center">
                        @can('modifier_factures')  
                        <!-- Bouton Modifier avec icône Font Awesome -->
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editInvoiceModal{{ $invoice->id }}" title="{{ __('Modifier') }}">
                            <i class="fas fa-edit"></i> <!-- Icône "edit" de Font Awesome -->
                        </button>                
                        @endcan
                        @can('supprimer_factures')  
                        <!-- Bouton Supprimer avec icône Font Awesome -->
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteInvoiceModal{{ $invoice->id }}" title="{{ __('Supprimer') }}">
                            <i class="fas fa-trash-alt"></i> <!-- Icône "trash-alt" de Font Awesome -->
                        </button>                      
                          @endcan
                        @can('list_factures')  
                        <!-- Bouton Imprimer avec icône Font Awesome -->
                    <a href="{{ route('invoices.print', $invoice->id) }}" class="btn btn-info btn-sm" title="{{ __('Imprimer la facture') }}">
                        <i class="fas fa-print"></i> <!-- Icône "print" de Font Awesome -->
                    </a>
                        @endcan
                        @can('list_factures')
                        <a href="{{ route('invoices.download', $invoice->id) }}" class="btn btn-info btn-sm" title="{{ __('Télécharger PDF') }}">
                            <i class="fas fa-download"></i>
 <!-- Icône "file-pdf" de Font Awesome -->
                        </a>
                        @endcan
                    </td>
                </tr>

                <!-- Modal de modification -->
                <div class="modal fade" id="editInvoiceModal{{ $invoice->id }}" tabindex="-1" role="dialog" aria-labelledby="editInvoiceModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="editInvoiceModalLabel">{{ __('Modifier Facture') }}</h5>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('invoices.update', $invoice->id) }}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label for="description">{{ __('Description') }}</label>
                                        <textarea id="description" name="description" class="form-control" rows="3">{{ old('description', $invoice->description ?? '') }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="date_debut">{{ __('Date de début') }}</label>
                                        <input type="date" class="form-control" name="date_debut" value="{{ $invoice->date_debut }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="date_fin">{{ __('Date de fin') }}</label>
                                        <input type="date" class="form-control" name="date_fin" value="{{ $invoice->date_fin }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="total">{{ __('Total') }}</label>
                                        <input type="number" class="form-control" name="total" value="{{ $invoice->total }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nom_client">{{ __('Nom client') }}</label>
                                        <input type="text" class="form-control" name="nom_client" value="{{ $invoice->nom_client }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="tel_client">{{ __('Tel client') }}</label>
                                        <input type="text" class="form-control" name="tel_client" value="{{ $invoice->tel_client }}" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-success">{{ __('Modifier') }}</button>
                                        
                                    
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de description -->
<div class="modal fade" id="descriptionModal{{ $invoice->id }}" tabindex="-1" role="dialog" aria-labelledby="descriptionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="descriptionModalLabel">{{ __('Description de la Facture') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Description affichée dans le modal -->
                <p>{{ $invoice->description }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
            </div>
        </div>
    </div>
</div>


                <!-- Modal de suppression -->
                <div class="modal fade" id="deleteInvoiceModal{{ $invoice->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteInvoiceModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteInvoiceModalLabel">{{ __('Supprimer Facture') }}</h5>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('invoices.destroy', $invoice->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <p>{{ __('Etes-vous sûr de vouloir supprimer cette facture ?') }}</p>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit" class="btn btn-danger">{{ __('Supprimer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    <!-- Modal d'ajout -->
    <div class="modal fade" id="addInvoiceModal" tabindex="-1" role="dialog" aria-labelledby="addInvoiceModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addInvoiceModalLabel">{{ __('Ajouter Facture') }}</h5>
                </div>
                <div class="modal-body">
                    <form action="{{ route('invoices.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="description">{{ __('Description') }}</label>
                            <textarea id="description" name="description" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="date_debut">{{ __('Date de début') }}</label>
                            <input type="date" class="form-control" name="date_debut" required>
                        </div>
                        <div class="form-group">
                            <label for="date_fin">{{ __('Date de fin') }}</label>
                            <input type="date" class="form-control" name="date_fin" required>
                        </div>
                        <div class="form-group">
                            <label for="total">{{ __('Total') }}</label>
                            <input type="number" class="form-control" name="total" required>
                        </div>
                        <div class="form-group">
                            <label for="nom_client">{{ __('Nom client') }}</label>
                            <input type="text" class="form-control" name="nom_client" required>
                        </div>
                        <div class="form-group">
                            <label for="tel_client">{{ __('Tel client') }}</label>
                            <input type="text" class="form-control" name="tel_client" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                            <button type="submit" class="btn btn-success">{{ __('Enregistrer') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $('table.multi-table').DataTable({
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7,
            drawCallback: function() {
                $('.t-dot').tooltip({
                    template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                })
                $('.dataTables_wrapper table').removeClass('table-striped');
            }
        });
    });
</script>
@endsection
