<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Facture - INV-{{ $invoice->id }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        .content {
            flex: 1;
            padding: 20px;
        }
        .header {
            display: flex;
            justify-content: space-between;
            margin-bottom: 40px;
        }
        .header .title {
            font-size: 60px; 
            color: blue;
        }
        .header .invoice-title {
            font-size: 48px;
            color: black;
        }
        .client-info {
            margin-bottom: 30px;
        }
        .client-info p {
            font-size: 20px;
            color: black;
            margin: 5px 0;
        }
        .table-invoice {
            width: 100%;
            margin-bottom: 30px;
            border-collapse: collapse;
            font-size: 18px;
        }
        .table-invoice th, .table-invoice td {
            padding: 15px;
            border: 1px solid black;
            text-align: left;
        }
        .table-invoice th {
            background-color: #f2f2f2;
        }
        .total-section {
            display: flex;
            justify-content: flex-end; 
            margin-top: 20px;
            font-size: 20px;
        }
        .signature-space {
            margin-top: auto;
            text-align: right; 
            font-size: 16px;
            margin-right: 20px;
        }
        .footer {
            margin-top: auto;
            font-size: 16px;
            color: black;
            border-top: 2px solid #000;
            padding: 20px 0;
        }
        .footer .footer-title {
            color: blue;
        }
        .footer .footer-left,
        .footer .footer-center,
        .footer .footer-right {
            display: inline-block;
            width: 32%; 
            vertical-align: top;
        }
        .footer .footer-left {
            text-align: left;
        }
        .footer .footer-center {
            text-align: center;
        }
        .footer .footer-right {
            text-align: right;
        }
    </style>
</head>
<body onload="printing()">
    <div class="content">
        <div class="header">
            <div class="title">EGTT</div>
            <div class="invoice-title">INVOICE</div>
        </div>

        <div class="client-info">
            <p>Client: {{ $invoice->nom_client }}</p>
            <p>Date: {{ \Carbon\Carbon::parse($invoice->date_debut)->format('d F Y') }}</p>
            <p>Invoice: INV-{{ $invoice->id }}</p>
        </div>

        <table class="table-invoice">
            <thead>
                <tr>
                    <th>Description</th>
                    <th>Période</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $invoice->description }}</td>
                    <td>Du {{ \Carbon\Carbon::parse($invoice->date_debut)->format('d/m/Y') }} au {{ \Carbon\Carbon::parse($invoice->date_fin)->format('d/m/Y') }}</td>
                    <td>{{ number_format($invoice->total, 0, ',', ' ') }} MRU Hors taxes</td>
                </tr>
            </tbody>
        </table>

        <div class="total-section">
            <strong>GRAND TOTAL : </strong> {{ number_format($invoice->total, 0, ',', ' ') }} MRU
        </div>

        <div class="signature-space">
            <p style="margin-top: 50px; text-align: right; font-size: 16px;">Signature :</p>
        </div>
    </div>

    <div class="footer">
        <div class="footer-left">
            <p class="footer-title">Reference:</p>
            <p>RC:4726</p>
            <p>NIF: 20600976</p>
            <p>CNSS: 12148</p>
        </div>
        <div class="footer-center">
            <p class="footer-title">Bank Info:</p>
            <p>Account: </p>
            <p>BNM 03201219801-56</p>
        </div>
        <div class="footer-right">
            <p class="footer-title">Contact Information:</p>
            <p>BP: 121 ZOUERATE</p>
            <p><i class="fa fa-phone"></i> +22236653685 / +22246929292</p>
            <p><i class="fa fa-envelope"></i> info@egttonline.com</p>
        </div>
    </div>

    <script>
        function printing() {
            window.print();
            setTimeout(function() {
                window.location.href = "{{ route('invoices.index') }}";
            }, 1000);
        }
    </script>
</body>
</html>
