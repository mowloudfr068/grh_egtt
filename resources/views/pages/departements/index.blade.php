@extends('layouts.master')
@section('css')
@endsection
@section('title')
    {{ __('Departements') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9" >
                <div class="mt-2">
                    <a href="#">{{ __('Home') }}</a>/<a
                        href="#">{{ __('Departements') }}</a>
                        
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_departement')
                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal">
                    <i class="fas fa-add"></i>
                    {{ __('Ajouter Departement') }}
                </button>  
                @endcan
                
            </div>
            

        </div>

    </div>
@endsection
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Nom') }}</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('statut') }}</th>
                <th class="text-center dt-no-sorting">{{ __('Opperation') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($departements as $departement)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $departement->name }}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#descriptionModal{{ $departement->id }}">
                            <i class="fas fa-eye"></i> <!-- Icône "eye" de Font Awesome -->
                        </button>
                    </td>
                    <td>
                        @if ($departement->status == 1)
                            <label
                                class="badge badge-success">{{ __('activer') }}</label>
                        @else
                            <label
                                class="badge badge-danger">{{ __('desactiver') }}</label>
                        @endif

                    </td>
                    <td class="text-center">
                        @can('modifier_departement')
                         <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                            data-target="#edit{{ $departement->id }}"
                            title="{{ __('Modifier') }}">
                            <i class="fas fa-edit"></i>
                        
                        </button>   
                        @endcan
                        
                        @can('supprimer_departement')
                         
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete{{ $departement->id }}" title="{{ __('Supprimer') }}">
                                <i class="fas fa-trash-alt"></i> <!-- Icône "trash-alt" de Font Awesome -->
                            </button>  
                        @endcan
                        
                    </td>
                </tr>
                <!-- edit_modal_departement -->
                <div class="modal fade" id="edit{{ $departement->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('Modifier  departement') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <!-- add_form -->
                                <form action="{{ route('departments.update', 'test') }}" method="post">
                                    {{ method_field('patch') }}
                                    @csrf
                                    <div class="row">
                                        <div class="col">
                                            <label for="Name_ar" class="mr-sm-2">{{ __('Nom') }}
                                                :</label>
                                            <input id="Name" type="text" name="name" class="form-control"
                                                value="{{ $departement->name }}" required>
                                            <input id="id" type="hidden" name="id" class="form-control"
                                                value="{{ $departement->id }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                      <div class="form-group">
                                        <label for="exampleFormControlTextarea1">{{ __('description') }}
                                            :</label>
                                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3">{{ $departement->description }}</textarea>
                                    </div>  
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-check">
                                                @if ($departement->status === 1)
                                                    <input type="checkbox" checked class="form-check-input"
                                                        name="status" id="exampleCheck1">
                                                @else
                                                    <input type="checkbox" class="form-check-input"
                                                        name="status" id="exampleCheck1">
                                                @endif
                                                <label class="form-check-label"
                                                    for="exampleCheck1">{{ __('statut') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    </div>
                                    <br><br>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit"
                                            class="btn btn-success">{{ __('Modifier') }}</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal de description -->
<div class="modal fade" id="descriptionModal{{ $departement->id }}" tabindex="-1" role="dialog" aria-labelledby="descriptionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="descriptionModalLabel">{{ __('Description de la Departement') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Description affichée dans le modal -->
                <p>{{ $departement->description }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
            </div>
        </div>
    </div>
</div>

                <!-- delete_modal_departement -->
                <div class="modal fade" id="delete{{ $departement->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                                    {{ __('supprimer departement') }}
                                </h5>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('departments.destroy', 'test') }}" method="post">
                                    {{ method_field('Delete') }}
                                    @csrf
                                    {{ __('ete vous sur pour la suppression ?') }}
                                    <input id="id" type="hidden" name="id" class="form-control"
                                        value="{{ $departement->id }}">
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('Annuler') }}</button>
                                        <button type="submit"
                                            class="btn btn-danger">{{ __('Supprimer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>

    </table>

    <!-- add_modal_department -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="font-family: 'Cairo', sans-serif;" class="modal-title" id="exampleModalLabel">
                        {{ __('Ajouter Departement') }}
                    </h5>

                </div>
                <div class="modal-body">
                    <!-- add_form -->
                    <form action="{{ route('departments.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label for="name" class="mr-sm-2">{{ __('name') }}
                                    :</label>
                                <input id="name" type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                               <div class="form-group">
                            <label for="exampleFormControlTextarea1">{{ __('Description') }}
                                :</label>
                            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div> 
                            </div>
                        </div>
                            
                        
                        <br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ __('Annuler') }}</button>
                    <button type="submit" class="btn btn-success">{{ __('enregistre') }}</button>
                </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
