@extends('layouts.master')

@section('content')
<h1>Page Test .. </h1>
<p>La page de test de Livewire validation</p>

<div>
    <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#add">Afficher Modal</button>
</div>

@livewire('add-employee')
@endsection

@section('js')
<script>
    document.addEventListener('livewire:load', function () {
        Livewire.on('closeModal', () => {
            $('#add').modal('hide');
        });
    });
</script>


@endsection