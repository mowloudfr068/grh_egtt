@extends('layouts.master')

@section('css')
@endsection

@section('title')
    {{ __('Créer une Lettre') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Home') }}</a> / <a href="#">{{ __('Créer une Lettre') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="card mt-3">
        <div class="card-header">
            <h4>{{ __('Créer une Lettre') }}</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('lettre.imprimer') }}" method="GET" target="_blank">
                <div class="form-group">
                    <label for="destinataire">{{ __('À Monsieur') }}</label>
                    <input type="text" id="destinataire" name="destinataire" class="form-control" placeholder=" Le Directeur ..." required>
                </div>
                <div class="form-group">
                    <label for="corps">{{ __('Corps de la Lettre') }}</label>
                    <textarea id="corps" name="corps" class="form-control" rows="10" placeholder="contenu de votre lettre..." required></textarea>
                </div>
                <div class="form-group">
                    <label for="salutation">{{ __('Salutation') }}</label>
                    <input type="text" id="salutation" name="salutation" class="form-control" value="Veuillez agréer, Monsieur, mes salutations distinguées." required>
                </div>
                <div class="form-group">
                    <label for="signature">{{ __('Signature') }}</label>
                    <input type="text" id="signature" name="signature" class="form-control" placeholder="Votre signature" required>
                </div>
                @can('create_lettre')  
                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-print"></i>
                    {{ __('Imprimer la Lettre') }}
                </button>
                @endcan
            </form>
            
        </div>
    </div>
@endsection

@section('js')
<script>
    // Ajoutez ici des scripts spécifiques si nécessaire
</script>
@endsection
