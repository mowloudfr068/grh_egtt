<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lettre à imprimer</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        .header {
            display: flex;
            justify-content: space-between;
            align-items: flex-start;
        }
        .header .title {
            font-size: 48px;
            color: blue;
            letter-spacing: 10px; /* Ajoute de l'espace entre les lettres */
        }
        hr {
            border: 2px solid blue;
            width: 100%;
            margin-top: 10px;
            margin-bottom: 50px; /* Augmente l'espacement sous la ligne */
        }
        .content {
            margin-top: 80px; /* Augmente l'espace sous l'en-tête */
            font-size: 18px;
            line-height: 1.6; /* Améliore l'espacement entre les lignes */
        }
        .signature {
            margin-top: 50px;
            text-align: right;
            font-size: 18px;
        }
        .date {
            font-size: 16px;
            margin-top: 10px;
        }
    </style>
</head>
<body onload="printing()">
    <!-- En-tête -->
    <div class="header">
        <div>
            <div class="title">E G T T</div>
        </div>
        <div class="date">
            <p>Le : {{ now()->format('d/m/Y') }}</p>
        </div>
    </div>
    <hr> <!-- Ligne bleue pleine largeur -->

    <!-- Contenu de la lettre -->
    <div class="content">
        <p>À Monsieur {{ $destinataire }}</p>
        <p>{{ $corps }}</p>
        <p>{{ $salutation }}</p>
    </div>

    <!-- Signature -->
    <div class="signature">
        <p>{{ $signature }}</p>
    </div>

<script>
    function printing() {
            window.print();
            setTimeout(function() {
                window.location.href = "{{ route('lettre.create') }}";
            }, 1000);
        }
</script>
</body>
</html>
