@extends('layouts.master')

@section('title', __('Modifier Présence'))

@section('content')
    <h3 class="mb-4">{{ __('Modifier la présence pour : ') }} {{ $employee->first_name }} {{ $employee->last_name }}</h3>

    <form action="{{ route('attendances.update', $attendance->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <input type="hidden" name="employee_id" value="{{ $employee->id }}">
        <input type="hidden" name="month_id" value="{{ $month->id }}">
        <input type="hidden" name="anne" value="{{ $year }}">

        <div class="form-group">
            <label class="font-weight-bold">{{ __('Mois : ') }} {{ $month->libelle }} - {{ $year }}</label>
        </div>

        <div class="form-group">
            <label class="font-weight-bold">{{ __('Jours : ') }}</label>
            <div class="row">
                @for ($day = 1; $day <= $selectedMonthDays; $day++)
                    <div class="col-2 col-md-1 text-center mb-3">
                        <div class="form-check">
                            <input 
                                type="checkbox" 
                                name="days[]" 
                                value="{{ $day }}" 
                                id="day-{{ $day }}" 
                                class="form-check-input"
                                {{ in_array($day, $days) ? 'checked' : '' }}
                            >
                            <label for="day-{{ $day }}" class="form-check-label">{{ $day }}</label>
                        </div>
                    </div>
                @endfor
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success"><i class="fas fa-add"></i> {{ __('Enregistrer') }}</button>
            <a href="{{ route('attendances.list') }}" class="btn btn-secondary">{{ __('Annuler') }}</a>
        </div>
    </form>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('table').DataTable({
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "{{ __('Affichage de la page _PAGE_ sur _PAGES_') }}",
                "sSearch": "{{ __('Recherche') }}",
                "sLengthMenu": "{{ __('Afficher _MENU_ éléments') }}",
                "sInfoEmpty": "{{ __('Aucun résultat disponible') }}",
                "sInfoFiltered": "{{ __('(filtré à partir de _MAX_ éléments)') }}",
            },
            "stripeClasses": []
        });
    });
</script>
@endsection
