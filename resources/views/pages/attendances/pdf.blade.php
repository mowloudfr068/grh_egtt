<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Détails Présences</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 10px;
            margin: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            table-layout: fixed;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 4px;
            text-align: left;
            word-wrap: break-word;
        }

        .header h1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="header">
        <h1>{{ __('Détails des Présences') }}</h1>
        <p><strong>Mois:</strong> {{ $selected_month_name }}</p>
        <p><strong>Année:</strong> {{ $selected_year }}</p>
    </div>

    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Nom Complet') }}</th>
                <th>{{ __('Jour(s) Travaillé(s)') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($attendances as $attendance)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $attendance->employee->matricule }}</td>
                    <td>{{ $attendance->employee->first_name . ' ' . $attendance->employee->last_name }}</td>
                    <td>{{ count(json_decode($attendance->days, true)) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
