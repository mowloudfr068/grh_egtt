    @extends('layouts.master')
@section('css')
<style>
     .select2-container {
        width: 100% !important;
    }

    .select2-selection {
        height: 45px !important;
    }
</style>
@endsection
    @section('title')
        {{ __('Liste des Pointages') }}
    @endsection

    @section('content-header')
        <div class="widget-header p-2">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">{{ __('Liste des Pointages') }}</h3>
                </div>
            </div>
        </div>
    @endsection

    @section('content')
    <form action="{{ route('attendances.list') }}" method="get" class="mb-4">
        <div class="form-row">
            <div class="col-md-4">
                <label for="month_id">{{ __('Mois') }}</label>
                <select class="form-control select2" id="month_id" name="month_id" onchange="this.form.submit()">
                    @foreach ($months as $month)
                        <option value="{{ $month->id }}" {{ $month->id == $selectedMonthId ? 'selected' : '' }}>
                            {{ $month->libelle }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label for="year">{{ __('Année') }}</label>
                <input type="number" class="form-control" id="year" name="year" value="{{ $selectedYear }}" min="2000" onchange="this.form.submit()">
            </div>
        </div>
    </form>
    
        <form action="{{ route('attendances.downloadPDF') }}" method="get">
            <input type="hidden" name="month_id" value="{{ request('month_id',now()->month) }}">
            <input type="hidden" name="year" value="{{ request('year', date('Y')) }}">
            <button type="submit" class="btn btn-primary" title="{{ __('Télécharger PDF') }}">
                <i class="fas fa-download"></i> {{ __('Télécharger PDF') }}
            </button>
        </form>

        <table class="table table-striped table-bordered datatable">
            <thead>
                <tr>
                    <th>{{ __('Nom complet') }}</th>
                    <th>{{ __('Matricule') }}</th>
                    <th>{{ __('Jours travaillés') }}</th>
                    <th>{{ __('Détails') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)
                    <tr>
                        <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                        <td>{{ $employee->matricule }}</td>
                    
                        @php
                            $workDays = 0; 
                            $dailyDetails = []; 
                            $employeeDays = $attendanceData[$employee->id] ?? [];
                        @endphp
                        @for ($day = 1; $day <= $selectedMonthDays; $day++)
                            @php
                                $isPresent = in_array($day, $employeeDays);
                                if ($isPresent) {
                                    $workDays++;
                                    $dailyDetails[$day] = 'W';
                                } else {
                                    $dailyDetails[$day] = 'R';
                                }
                            @endphp
                        @endfor
                        <td>{{ $workDays }}</td>
                        <td>
                            <!-- Bouton pour ouvrir la modal -->
                            <div class="d-inline-flex align-items-center">
                                <!-- Bouton pour ouvrir la modal -->
                                <button type="button" class="btn btn-info btn-sm mr-2" data-toggle="modal" data-target="#detailsModal-{{ $employee->id }}">
                                    {{ __('Voir détails') }}
                                </button>
                            
                                <!-- Formulaire pour le bouton Modifier -->
                                @can('modifier_presences')  
                                <form action="{{ route('attendances.editAttendance') }}" method="GET" class="m-0">
                                    <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                                    <input type="hidden" name="month_id" value="{{ $selectedMonthId }}">
                                    <input type="hidden" name="anne" value="{{ $selectedYear }}">
                                    <button type="submit" class="btn btn-success btn-sm">
                                        {{ __('Modifier') }}
                                    </button>
                                </form>
                                @endcan
                            </div>
                            
                            
                            

                            <!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="detailsModal-{{ $employee->id }}" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel-{{ $employee->id }}" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title" id="detailsModalLabel-{{ $employee->id }}">
                    {{ __('Détails des présences pour : ') }} {{ $employee->first_name }} {{ $employee->last_name }}
                </h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="max-height: 70vh; overflow-y: auto;">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th>{{ __('Jour') }}</th>
                                <th>{{ __('Statut') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($dailyDetails as $dayNumber => $status)
                                <tr>
                                    <td>Jour {{ $dayNumber }}</td>
                                    <td>
                                        @if ($status == 'W')
                                            <span class="badge badge-success">{{ __('Présent (W)') }}</span>
                                        @else
                                            <span class="badge badge-warning">{{ __('Absent (R)') }}</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
            </div>
        </div>
    </div>
</div>



                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endsection


    @section('js')
        <script>
        
            function toggleAll(source) {
                const checkboxes = document.querySelectorAll('input[type="checkbox"]:not(#checkAll)');
                checkboxes.forEach(checkbox => checkbox.checked = source.checked);
            }

            function toggleRow(source, employeeId) {
                const rowCheckboxes = document.querySelectorAll(`.checkbox-${employeeId}`);
                rowCheckboxes.forEach(checkbox => checkbox.checked = source.checked);
            }

            $(document).ready(function() {

        $('.select2').select2({
            placeholder: "{{ __('Sélectionner ...') }}",  
            allowClear: true,  
            width: '100%', 
        });
                $('.datatable').DataTable({
                    "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                        "<'table-responsive'tr>" +
                        "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                    "oLanguage": {
                        "oPaginate": {
                            "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                            "sNext": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                        },
                        "sInfo": "{{ __('Affichage de la page _PAGE_ sur _PAGES_') }}",
                        "sSearch": "{{ __('Recherche') }}",
                        "sLengthMenu": "{{ __('Afficher _MENU_ éléments') }}",
                        "sInfoEmpty": "{{ __('Aucun résultat disponible') }}",
                        "sInfoFiltered": "{{ __('(filtré à partir de _MAX_ éléments)') }}",
                    },
                    "stripeClasses": []
                });
            });
        </script>
    @endsection
