@extends('layouts.master')

@section('css')
<style>
    input[type="checkbox"] {
        width: 25px;
        height: 25px;
        margin: 0;
        appearance: none;
        border-radius: 5px;
        border: 2px solid #ccc;
        transition: background-color 0.3s ease, transform 0.2s ease;
        cursor: pointer;
    }

    input[type="checkbox"].unchecked {
        background-color: rgb(237, 115, 115);
    }

    input[type="checkbox"]:checked {
        background-color: rgb(146, 255, 146);
    }

    .true-symbol {
        font-weight: bold;
        color: green;
        margin-left: 5px;
    }

    .scrollable-table-wrapper {
        position: relative;
        overflow: hidden;
        width: 100%;
    }

    .scrollable-table {
        overflow-x: auto;
        white-space: nowrap;
    }

    thead th {
        position: sticky;
        top: 0;
        background-color: #f8f9fa;
        z-index: 1;
    }

    .select2-container {
        width: 100% !important;
    }

    .select2-selection {
        height: 45px !important;
    }

    .badge {
        padding: 5px 10px;
        font-size: 1rem;
    }

    .btn {
        font-size: 1.1rem;
        padding: 10px 20px;
        border-radius: 5px;
        transition: background-color 0.3s ease;
    }

    .btn-success {
        background-color: #28a745;
    }

    .btn-success:hover {
        background-color: #218838;
    }

    .form-control {
        font-size: 1rem;
    }

    .datatable-wrapper {
        margin-top: 20px;
    }

    .form-control.select2 {
        max-width: 300px;
        display: inline-block;
        margin-right: 20px;
    }
</style>
@endsection

@section('title')
    {{ __('Présences') }}
@endsection

@section('content-header')
<div class="widget-header p-2">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center">
                {{ __('Présences pour le mois de :') }}
                <form action="{{ route('attendances.index') }}" method="get" class="d-inline-block">
                    <select name="month_id" onchange="this.form.submit()" class="form-control select2">
                        @foreach ($months as $month)
                            <option value="{{ $month->id }}" 
                                {{ (old('month_id', $selectedMonth->id ?? null) == $month->id) ? 'selected' : '' }}>{{ $month->libelle }}</option>
                        @endforeach
                    </select>
                </form>
            </h3>
        </div>
    </div>
</div>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($selectedMonth && count($days) > 0)
    <form action="{{ route('attendances.store') }}" method="post">
        @csrf
        <input type="hidden" name="month_id" value="{{ $selectedMonth->id }}">
        <input type="hidden" name="year" value="{{ now()->year }}">

        <div class="scrollable-table-wrapper">
            <div class="scrollable-table" id="scrollable-table">
                <table class="table datatable table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('Nom complet') }}</th>
                            <th>{{ __('Matricule') }}</th>
                            <th>{{ __('Statut') }}</th>
                            <th>{{ __('Tout sélectionner') }}</th>
                            @foreach ($days as $day)
                                <th>Jour {{ $day }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                            @php
                                $isAttended = isset($attendanceData[$employee->id]) && count($attendanceData[$employee->id]) > 0;
                            @endphp
                            <tr>
                                <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                                <td>{{ $employee->matricule }}</td>
                                <td>
                                    <span class="badge {{ $isAttended ? 'badge-success' : 'badge-danger' }}">
                                        {{ $isAttended ? __('Déjà pointé') : __('En attente') }}
                                    </span>
                                </td>
                                <td>
                                    <input type="checkbox" class="select-all-row" onclick="toggleRow(this, '{{ $employee->id }}')" {{ $isAttended ? 'disabled' : '' }}>
                                </td>
                                @foreach ($days as $day)
                                    <td>
                                        <input type="checkbox" name="attendance[{{ $employee->id }}][{{ $day }}]" value="present" class="checkbox-{{ $employee->id }}" {{ $isAttended ? 'disabled' : '' }} 
                                            onclick="showTrueSymbol(this, '{{ $employee->id }}', '{{ $day }}')">
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success"> <i class="fas fa-check"></i> {{ __('Enregistrer les présences') }}</button>
        </div>
    </form>
@else
    <div class="alert alert-info text-center">
        {{ __('Aucun jour disponible pour ce mois.') }}
    </div>
@endif
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "{{ __('Sélectionner un mois...') }}",
            allowClear: true,
            width: '100%'
        });

        $('.datatable').DataTable({
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<i class="fas fa-arrow-left"></i>',
                    "sNext": '<i class="fas fa-arrow-right"></i>'
                },
                "sInfo": "{{ __('Affichage de la page _PAGE_ sur _PAGES_') }}",
                "sSearch": "{{ __('Recherche') }}",
                "sLengthMenu": "{{ __('Afficher _MENU_ éléments') }}",
            }
        });
    });

    function showTrueSymbol(checkbox, employeeId, day) {
        const trueSymbol = document.querySelector(`#true-symbol-${employeeId}-${day}`);
        if (checkbox.checked) {
            if (!trueSymbol) {
                const symbolSpan = document.createElement('span');
                symbolSpan.id = `true-symbol-${employeeId}-${day}`;
                symbolSpan.className = 'true-symbol';
                symbolSpan.textContent = '✔';
                checkbox.parentElement.appendChild(symbolSpan);
            }
        } else {
            if (trueSymbol) {
                trueSymbol.remove();
            }
        }
    }

    function toggleRow(source, employeeId) {
        const checkboxes = document.querySelectorAll(`.checkbox-${employeeId}`);
        checkboxes.forEach(checkbox => {
            checkbox.checked = source.checked;
            showTrueSymbol(checkbox, employeeId, checkbox.name.split('[')[1].split(']')[0]);
        });
    }

    function handleCheckboxChange(checkbox, employeeId, day) {
        showTrueSymbol(checkbox, employeeId, day);

        const checkboxes = document.querySelectorAll(`.checkbox-${employeeId}`);
        const allChecked = Array.from(checkboxes).every(checkbox => checkbox.checked);
        const selectAllCheckbox = document.querySelector(`#select-all-${employeeId}`);
        selectAllCheckbox.checked = allChecked;
    }

    document.querySelectorAll('input[type="checkbox"]').forEach(checkbox => {
        checkbox.addEventListener('change', function() {
            const employeeId = this.name.split('[')[1].split(']')[0]; 
            const day = this.name.split('[')[2].split(']')[0]; 
            handleCheckboxChange(this, employeeId, day);
        });
    });
</script>

@endsection
