@extends('layouts.master')

@section('css')
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-selection {
        height: 45px !important;
    }

    .form-group {
        flex: 1;
        margin-bottom: 0;
    }

    .form-control {
        height: 45px;
    }

    .d-flex {
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-wrap: wrap;
    }

    .form-group {
        margin-right: 1rem;
        margin-bottom: 1rem;
    }

    @media (max-width: 768px) {
        .form-group {
            flex: 1 1 100%; 
            margin-right: 0;
        }

        .d-flex {
            flex-direction: column; /* Empiler les éléments sur les écrans plus petits */
        }

        .form-control {
            width: 100%;
        }
    }
</style>
@endsection

@section('title')
    {{ __('Détails Paiements') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <!-- Breadcrumb Navigation -->
            <div class="col-12">
                <div class="mt-2">
                    <a href="#">{{ __('Salaires/Details') }}</a> / <a href="#">{{ __('Employés') }}</a>
                </div>
            </div>
        </div>

        <!-- Formulaire pour la sélection du mois, année et boutons d'action sur une seule ligne -->
        <div class="row mt-3">
            <!-- Sélection du Mois, de l'Année, et Boutons d'action sur la même ligne -->
            <div class="col-md-12 col-12">
                <form action="{{ route('payments.Details') }}" method="get" class="d-flex">
                    <!-- Sélection du Mois -->
                    <div class="form-group mt-4">
                        <label for="month_id" class="sr-only">{{ __('Mois') }}</label>
                        <select class="form-control select2" id="month_id" name="month_id" onchange="this.form.submit()">
                            @foreach ($months as $month)
                                <option value="{{ $month->id }}" {{ $month->id == $selectedMonthId ? 'selected' : '' }}>
                                    {{ $month->libelle }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Saisie de l'Année -->
                    <div class="form-group">
                        <label for="year" class="sr-only">{{ __('Année') }}</label>
                        <input type="number" class="form-control" id="year" name="year" value="{{ $selectedYear }}" min="2000" onchange="this.form.submit()">
                    </div>

                    <!-- Voir les Totaux -->
                    <div class="form-group">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#totalsModal">
                            <i class="fas fa-calculator"></i> {{ __('Voir les Totaux') }}
                        </button>
                    </div>

                    
                    </div>
                </form>
                <!-- Télécharger PDF -->
                <div class="form-group">
                    <form action="{{ route('payments.downloadPDF') }}" method="get" style="display: inline;">
                        <input type="hidden" name="month_id" value="{{ request('month_id', now()->month) }}">
                        <input type="hidden" name="year" value="{{ request('year', date('Y')) }}">
                        <button type="submit" class="btn btn-primary" title="{{ __('Télécharger PDF') }}">
                            <i class="fas fa-download"></i> {{ __('Télécharger PDF') }}
                        </button>
                    </form>
            </div>
        </div>

        <!-- Modal pour afficher les Totaux -->
        <div class="modal fade" id="totalsModal" tabindex="-1" aria-labelledby="totalsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius: 8px; border: 2px solid #388e3c; overflow: hidden;">
                    <div class="modal-header" style="background-color: #e8f5e9; color: #388e3c;">
                        <h5 class="modal-title" id="totalsModalLabel">
                            <i class="fas fa-info-circle"></i> {{ __('Totaux des Salaires') }}
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="background-color: #f1f8e9;">
                        <div class="totals-details">
                            <p class="d-flex align-items-center">
                                <span class="text-success mr-2"><i class="fas fa-check-circle"></i></span>
                                <strong>{{ __('Sous Total :') }}</strong> {{ number_format($sousTotal, 2) }} UM
                            </p>
                            <p class="d-flex align-items-center">
                                <span class="text-success mr-2"><i class="fas fa-check-circle"></i></span>
                                <strong>{{ __('Majoration 13% :') }}</strong> {{ number_format($majoration13, 2) }} UM
                            </p>
                            <p class="d-flex align-items-center">
                                <span class="text-success mr-2"><i class="fas fa-check-circle"></i></span>
                                <strong>{{ __('Total Général :') }}</strong> {{ number_format($totalGeneral, 2) }} UM
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer" style="background-color: #e8f5e9;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection










@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>{{ __('ORD') }}</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Nom Complet') }}</th>
                <th>{{ __('Mois') }}</th>
                <th>{{ __('Jour travaille') }}</th>
                <th>{{ __('Sal.Brut Mensuel (MRU)') }}</th>
                <th>{{ __('Cnam Employé') }}</th>
                <th>{{ __('Cnss Employé') }}</th>
                <th>{{ __('ITS') }}</th>
                <th>{{ __('Salaire Net') }}</th>
                <th>{{ __('Cnam Patron') }}</th>
                <th>{{ __('Médecin Travail') }}</th>
                <th>{{ __('Cnss Patron') }}</th>
                <th>{{ __('Congé') }}</th>
                <th>{{ __('Total Coût') }}</th>
                <th>{{ __('Actions') }}</th>  <!-- Ajout d'une colonne pour les actions -->
            </tr>
        </thead>
        <tbody>
            @foreach ($payments as $payment) 
            <tr>
                <td>{{ $payment->ordre ?? 'N/A' }}</td>
                <td>{{ $payment->attendance->employee->matricule }}</td>
                <td>{{ $payment->attendance->employee->first_name." ".$payment->attendance->employee->last_name }}</td>
                <td>{{ $payment->attendance->month->libelle }}</td>
                <td>
                    @php
                        $daysArray = json_decode($payment->attendance->days, true);
                        $joursTravailles = is_array($daysArray) ? count($daysArray) : 0;
                    @endphp
                    {{ $joursTravailles }} <!-- Affiche le nombre de jours travaillés -->
                </td>
                
                <td bgcolor="#009688">
                    <p class="text text-white">{{ $payment->salaire_brut_mensuel }}</p>
                </td>
                <td>{{ $payment->cnam_employee ?? 'N/A' }}</td>
                <td>{{ $payment->cnss_employee }}</td>
                <td>{{ $payment->its }}</td>
                <td bgcolor="#009688">
                    <p class="text text-white">{{ $payment->salaire_net }}</p>
                </td>
                <td>{{ $payment->cnam_patron }}</td>
                <td>{{ $payment->medecin_travail }}</td>
                <td>{{ $payment->cnss_patron }}</td>
                <td>{{ $payment->conges }}</td>
                <td bgcolor="#FF5252">
                    <p class="text text-white">{{ $payment->total_cout }}</p>
                </td>
                <td>  <!-- Bouton d'annulation -->
                   
                    @can('annuler_paiements')
                        
                    <!-- Bouton pour annuler un paiement -->
<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#cancelPaymentModal">
<i class="fas fa-times"></i>
</button>
                    @endcan
              
                </td>


<!-- Modal de confirmation d'annulation -->
<div class="modal fade" id="cancelPaymentModal" tabindex="-1" aria-labelledby="cancelPaymentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Header de la modal -->
            <div class="modal-header" style="background-color: #efefef; color: white;">
                <h5 class="modal-title" id="cancelPaymentModalLabel">
                    <i class="fas fa-times"></i> {{ __('Êtes-vous sûr de vouloir annuler ce paiement ?') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- Corps de la modal -->
            <div class="modal-body">
                {{ __('Si vous annulez, cette action ne peut pas être inversée.') }}
            </div>
            <!-- Footer de la modal -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                <form action="{{ route('payments.destroy',$payment->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">{{ __('Annuler le paiement') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>


            </tr>
            @endforeach
        </tbody>
    </table>
@endsection


@section('js')
<script>
    $(document).ready(function() {

        // Initialiser Select2
        $('.select2').select2({
            placeholder: "{{ __('Sélectionner ...') }}",  
            allowClear: true,  
            width: '100%', 
        });

         $('table.multi-table').DataTable({
             "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                 "<'table-responsive'tr>" +
                 "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count mb-sm-0 mb-3'i><'dt--pagination'p>>",
             "oLanguage": {
                 "oPaginate": {
                     "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                     "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                 },
                 "sInfo": "Showing page _PAGE_ of _PAGES_",
                 "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                 "sSearchPlaceholder": "Search...",
                 "sLengthMenu": "Results :  _MENU_",
             },
             "stripeClasses": [],
             "lengthMenu": [7, 10, 20, 50],
             "pageLength": 7,
             "buttons": [
                 {
                     "extend": 'copy', 
                     "className": 'btn btn-primary',
                     "text": '<i class="fas fa-copy"></i> Copy'
                 },
                 {
                     "extend": 'csv', 
                     "className": 'btn btn-info',
                     "text": '<i class="fas fa-file-csv"></i> CSV'
                 },
                 {
                     "extend": 'excel', 
                     "className": 'btn btn-success',
                     "text": '<i class="fas fa-file-excel"></i> Excel'
                 },
                 {
                     "extend": 'pdf', 
                     "className": 'btn btn-danger',
                     "text": '<i class="fas fa-file-pdf"></i> PDF',
                     "title": 'Détails Paiements',
                     "messageTop": 'Détails des paiements des employés'
                 },
                 {
                     "extend": 'print', 
                     "className": 'btn btn-secondary',
                     "text": '<i class="fas fa-print"></i> Print'
                 }
             ],
             "drawCallback": function() {
                 $('.t-dot').tooltip({
                     template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                 })
                 $('.dataTables_wrapper table').removeClass('table-striped');
             }
         });
     });
 </script>
 
@endsection
