<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>{{ __('Détails Paiements') }}</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            font-size: 10px;
            margin: 20px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            table-layout: fixed;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 4px;
            text-align: left;
            word-wrap: break-word;
        }

        .totals-details {
            margin-bottom: 20px;
            padding: 10px;
            background-color: #f3f3f3;
            border: 1px solid #ffffff;
            border-radius: 5px;
            font-size: 12px;
        }

        .totals-details p {
            margin: 5px 0;
            display: flex;
            align-items: center;
        }

        .totals-details span {
            color: #b1d9b3;
            margin-right: 10px;
            font-size: 14px;
        }
    </style>
</head>
<body>
    <div class="header">
        <h1>{{ __('Calcul de Salaire détaillés pour :') }}</h1>
        <p><strong>Mois:</strong> {{ request('month_id') ? $months->find(request('month_id'))->libelle : 'Tous les mois' }}</p>
        <p><strong>Année:</strong> {{ request('year', date('Y')) }}</p>
    </div>

    <!-- Totaux affichés au-dessus de la table -->
    <div class="totals-details">
        <p>
         
            <strong>{{ __('Sous Total :') }}</strong> {{ number_format($sousTotal, 2) }} UM
        </p>
        <p>
        
            <strong>{{ __('Majoration 13% :') }}</strong> {{ number_format($majoration13, 2) }} UM
        </p>
        <p>
            
            <strong>{{ __('Total Général :') }}</strong> {{ number_format($totalGeneral, 2) }} UM
        </p>
    </div>

    <!-- Tableau des paiements -->
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Nom Complet') }}</th>
                <th>{{ __('Jour travaillé') }}</th>
                <th>{{ __('ORD') }}</th>
                <th>{{ __('Sal.Brut Mensuel (MRU)') }}</th>
                <th>{{ __('Cnam Employé') }}</th>
                <th>{{ __('Cnss Employé') }}</th>
                <th>{{ __('ITS') }}</th>
                <th>{{ __('Salaire Net') }}</th>
                <th>{{ __('Cnam Patron') }}</th>
                <th>{{ __('Médecin Travail') }}</th>
                <th>{{ __('Cnss Patron') }}</th>
                <th>{{ __('Congé') }}</th>
                <th>{{ __('Total Coût') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($payments as $payment)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $payment->attendance->employee->matricule }}</td>
                    <td>{{ $payment->attendance->employee->first_name . ' ' . $payment->attendance->employee->last_name }}</td>
                    <td>{{ count(json_decode($payment->attendance->days, true)) }}</td>
                    <td>{{ $payment->ordre ?? 'N/A' }}</td>
                    <td bgcolor="#009688">{{ $payment->salaire_brut_mensuel }}</td>
                    <td>{{ $payment->cnam_employee ?? 'N/A' }}</td>
                    <td>{{ $payment->cnss_employee }}</td>
                    <td>{{ $payment->its }}</td>
                    <td bgcolor="#009688">{{ $payment->salaire_net }}</td>
                    <td>{{ $payment->cnam_patron }}</td>
                    <td>{{ $payment->medecin_travail }}</td>
                    <td>{{ $payment->cnss_patron }}</td>
                    <td>{{ $payment->conges }}</td>
                    <td bgcolor="#FF5252">{{ $payment->total_cout }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
