@extends('layouts.master') 

@section('css')
<style>
    .select2-container {
        width: 100% !important;
    }

    .select2-selection {
        height: 45px !important;
    }
</style>
@endsection

@section('title')
    {{ __('Effectuer Paiement') }}
@endsection



@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Salaires') }}</a>/<a href="#">{{ __('Effectuer Paiement') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- Section du filtre par mois et année -->
    <form action="{{ route('payments.index') }}" method="get" class="mb-4">
        <div class="form-row">
            <div class="col-md-4">
                <label for="month_id">{{ __('Mois') }}</label>
                <select class="form-control select2" id="month_id" name="month_id" onchange="this.form.submit()">
                    @foreach ($months as $month)
                        <option value="{{ $month->id }}" {{ $month->id == $selectedMonthId ? 'selected' : '' }}>
                            {{ $month->libelle }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label for="year">{{ __('Année') }}</label>
                <input type="number" class="form-control" id="year" name="year" value="{{ $selectedYear }}" min="2000" onchange="this.form.submit()">
            </div>
        </div>
    </form>

    <!-- Bouton "Tout payer" -->
    <div class="justify-content-between align-items-center mb-4 ml-3">
        @if ($employees->count() > 0)
            <form action="{{ route('payments.mass_store') }}" method="post">
                @csrf
                <input type="hidden" name="month_id" value="{{ $selectedMonthId }}">
                <input type="hidden" name="year" value="{{ $selectedYear }}">
                <button type="submit" class="btn btn-success">
                    <i class="fas fa-credit-card"></i>  {{ __('Tout payer') }}
                </button>
            </form>
        @endif
    </div>
    <!-- Table des employés avec leurs jours de présence -->
    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Nni') }}</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Nom') }}</th>
                <th>{{ __('Salaire') }}</th>
                <th>{{ __('Jours travaillés') }}</th>
                <th>{{ __('Statut') }}</th> <!-- Nouvelle colonne pour le statut -->
                <th class="text-center">{{ __('Opérations') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                @php
                    // Récupérer les jours de présence pour cet employé
                    $employeeDays = $attendanceData[$employee->id] ?? [];
                    $workDays = 0; // Compteur des jours travaillés pour cet employé
                    $dailyDetails = []; // Stocker les détails des jours
                    $selectedMonthDays = cal_days_in_month(CAL_GREGORIAN, $selectedMonthId, $selectedYear); // Nombre de jours dans le mois sélectionné
                    $paymentStatus = 'Non payé'; // Valeur par défaut pour le statut
                @endphp
                
                @for ($day = 1; $day <= $selectedMonthDays; $day++)
                    @php
                        $isPresent = in_array($day, $employeeDays);
                        if ($isPresent) {
                            $workDays++;
                        } 
                    @endphp
                @endfor

                <!-- Vérification du paiement -->
                @php
                    $attendance = \App\Models\Attendance::where('employee_id', $employee->id)
                        ->where('month_id', $selectedMonthId)
                        ->where('anne', $selectedYear)
                        ->first();

                    if ($attendance) {
                        $payment = \App\Models\Payment::where('attendance_id', $attendance->id)->first();
                        if ($payment) {
                            $paymentStatus = 'Déjà payé';
                        }
                    }
                @endphp

                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $employee->nni }}</td>
                    <td>{{ $employee->matricule }}</td>
                    <td>{{ $employee->first_name ." ".$employee->last_name }}</td>
                    <td>{{ $employee->position->base_salary . ' MRU' }}</td>
                    <td>{{ $workDays }}</td>
                    <td><span class="badge badge-warning">{{ $paymentStatus }}</span></td> <!-- Afficher le statut ici -->
                    <td class="text-center">
                        @if($paymentStatus == 'Non payé' && $workDays!=0)
                         <!-- Si non payé, afficher le bouton -->
                            @can('ajout_paiements')
                                
                            <form action="{{ route('payments.store') }}" method="post">
                                @csrf
                                <input type="hidden" name="employee_id" value="{{ $employee->id }}">
                                <input type="hidden" name="month_id" value="{{ $selectedMonthId }}">
                                <input type="hidden" name="year" value="{{ $selectedYear }}">
                                <input type="hidden" name="jour" value="{{ $workDays }}">
                                <button type="submit" class="btn btn-success btn-bg" title="{{ __('Payer le paiement') }}">
                                    <i class="fas fa-credit-card"></i> {{ __('Payer') }} 
                                </button> 
                            </form>
                            
                            @endcan
                        @elseif ($workDays==0)
                            <!-- Optionally, add a message or button to show a view or acknowledgment if already paid -->
                            <span class="badge badge-success">{{ __('pas encore pointe') }}</span>
                        @else
                        <span class="badge badge-success">{{ __('Déjà payé') }}</span>

                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
        
    </table>
@endsection

@section('js')
    <script>



        $(document).ready(function() {
            // Initialiser Select2
        $('.select2').select2({
            placeholder: "{{ __('Sélectionner ...') }}",  
            allowClear: true,  
            width: '100%', 
        });

            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
