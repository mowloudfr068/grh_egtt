@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Gestion des Rôles</h2>
        </div>
        <div class="pull-right">
            @can('ajout_roles_utilisateur')
             <button class="btn btn-success" data-toggle="modal" data-target="#createRoleModal"><i class="fas fa-add"></i> Créer un rôle</button>
            @endcan
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table multi-table">
    <thead>
        <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($roles as $key => $role)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $role->name }}</td>
            <td>
              @can('voir_roles_utilisateur')
              <button class="btn btn-info" data-toggle="modal" data-target="#showRoleModal{{ $role->id }}">
                <i class="fas fa-eye"></i>
              </button>
              @endcan
          
              {{-- Vérifiez si le rôle est "owner" avant d'afficher les boutons de modification et suppression --}}
              @if ($role->name !== 'owner')
                  @can('modifier_roles_utilisateur')
                  <button class="btn btn-primary" data-toggle="modal" data-target="#editRoleModal{{ $role->id }}">
                    <i class="fas fa-edit"></i>
                  </button>
                  @endcan
          
                  @can('supprimer_roles_utilisateur')
                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDeleteModal{{ $role->id }}">
                      <i class="fas fa-trash-alt"></i>
                  </button>
              @endcan
              @endif
          </td>
          
        </tr>
        {{-- Modale de confirmation --}}
<div class="modal fade" id="confirmDeleteModal{{ $role->id }}" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmer la suppression</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              Êtes-vous sûr de vouloir supprimer ce rôle ? Cette action est irréversible.
          </div>
          <div class="modal-footer">
              {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id]]) !!}
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              <button type="submit" class="btn btn-danger">Supprimer</button>
              {!! Form::close() !!}
          </div>
      </div>
  </div>
</div>


        {{-- Modale pour afficher les détails d'un rôle --}}
<div class="modal fade" id="showRoleModal{{ $role->id }}" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title">Détails du rôle</h5>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
              <div class="mb-3">
                  <strong>Nom :</strong> <span class="text-primary">{{ $role->name }}</span>
              </div>
              <div>
                  <strong>Permissions :</strong>
                  <div class="mt-2">
                      {{-- Affichage en grille avec des badges --}}
                      <div class="row">
                          @foreach($role->permissions as $permission)
                              <div class="col-md-4 mb-2">
                                  <span class="badge badge-success p-2 w-100">{{ $permission->name }}</span>
                              </div>
                          @endforeach
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
          </div>
      </div>
  </div>
</div>


        {{-- Modale pour éditer un rôle --}}
        <div class="modal fade" id="editRoleModal{{ $role->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::model($role, ['method' => 'PATCH', 'route' => ['roles.update', $role->id]]) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">Modifier le rôle</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <strong>Nom :</strong>
                            {!! Form::text('name', null, ['placeholder' => 'Nom', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <strong>Permissions :</strong><br>
                          
                            @foreach($permissions as $permission)
                            <label>
                              {{ Form::checkbox(
                                  'permission[]', 
                                  $permission->id, 
                                  $role->permissions->contains('id', $permission->id) // Vérifie si la permission est liée au rôle
                              ) }}
                              {{ $permission->name }}
                          </label><br><br>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endforeach
    </tbody>
</table>

{!! $roles->links() !!}

{{-- Modale pour créer un nouveau rôle --}}
<div class="modal fade" id="createRoleModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['route' => 'roles.store', 'method' => 'POST']) !!}
            <div class="modal-header">
                <h5 class="modal-title">Créer un rôle</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <strong>Nom :</strong>
                    {!! Form::text('name', null, ['placeholder' => 'Nom', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <strong>Permissions :</strong><br>
                    @foreach($permissions as $permission)
                    <label>
                        {{ Form::checkbox('permission[]', $permission->id, false) }}
                        {{ $permission->name }}
                    </label><br>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-primary">Créer</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
