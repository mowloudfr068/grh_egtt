    @extends('layouts.site.master')

    @section('css')
    <style>
         .select2-container {
        width: 50% !important;
    }

    .select2-selection {
        height: 50px !important;
    }
    </style>
    @endsection

    @section('title')
        {{ __('Liste des Pointages') }}
    @endsection

    @section('content-header')
        <div class="widget-header p-2">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center">{{ __('Liste des Pointages') }}</h3>
                </div>
            </div>
        </div>
    @endsection

    @section('content')
        <form action="{{ route('employee.pointages') }}" method="get">
            <div class="form-row mb-3">
                <div class="col-md-4">
                    <label for="month_id">{{ __('Mois') }}</label>
                    <select class="form-control select2" id="month_id" onchange="this.form.submit()" name="month_id">
                        @foreach ($months as $month)
                            <option value="{{ $month->id }}" {{ $month->id == $selectedMonthId ? 'selected' : '' }}>
                                {{ $month->libelle }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="year">{{ __('Année') }}</label>
                    <input type="number" class="form-control" id="year" name="year" value="{{ $selectedYear }}" min="2000">
                </div>
                

                @if ($message = Session::get('error'))
                            <div class="alert alert-success alert-dismissible mb-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <span class="text-center">{{ $message }}</span>
                            </div>
                        @endif
            </div>
        </form>

        <table class="table table-striped table-bordered datatable">
            <thead>
                <tr>
                    <th>{{ __('Nom complet') }}</th>
                    <th>{{ __('Matricule') }}</th>
                    <th>{{ __('Jours travaillés') }}</th>
                    <th>{{ __('Statut') }}</th>
                    <th>{{ __('Détails') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($employees as $employee)
                    <tr>
                        <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                        <td>{{ $employee->matricule }}</td>
            
                        @php

                            // Vérifier si l'employé a des présences enregistrées pour ce mois
                            $isAttended = isset($attendanceData[$employee->id]) && count($attendanceData[$employee->id]) > 0;
                     
                            $workDays = 0; // Compteur des jours travaillés pour cet employé
                            $dailyDetails = []; // Stocker les détails des jours
                            $employeeDays = $attendanceData[$employee->id] ?? []; // Récupérer les jours de cet employé
                       
                       @endphp
                        @for ($day = 1; $day <= $selectedMonthDays; $day++)
                            @php
                                $isPresent = in_array($day, $employeeDays);
                                if ($isPresent) {
                                    $workDays++;
                                    $dailyDetails[$day] = 'W'; // Ajouter "W" pour présent
                                } else {
                                    $dailyDetails[$day] = 'R'; // Ajouter "R" pour absent
                                }
                            @endphp
                        @endfor
                        <td>{{ $workDays }}</td>
                        <td>
                           
                            <span class="badge {{ $isAttended ? 'badge-success' : 'badge-warning' }}">
                                {{ $isAttended ? __('Déjà pointé') : __('En attente') }}
                            </span>
                        </td>
                        <td>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#detailsModal-{{ $employee->id }}">
                                <i class="fas fa-info-circle"></i>
                                {{ __('Voir détails') }}
                            </button>
                            <!-- Modal et autres détails comme dans votre code -->
                            <div class="modal fade" id="detailsModal-{{ $employee->id }}" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel-{{ $employee->id }}" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-primary text-white">
                                            <h5 class="modal-title" id="detailsModalLabel-{{ $employee->id }}">
                                                {{ __('Détails des présences pour : ') }} {{ $employee->first_name }} {{ $employee->last_name }}
                                            </h5>
                                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style="max-height: 70vh; overflow-y: auto;">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th>{{ __('Jour') }}</th>
                                                            <th>{{ __('Statut') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($dailyDetails as $dayNumber => $status)
                                                            <tr>
                                                                <td>Jour {{ $dayNumber }}</td>
                                                                <td>
                                                                    @if ($status == 'W')
                                                                        <span class="badge badge-success">{{ __('Présent (W)') }}</span>
                                                                    @else
                                                                        <span class="badge badge-warning">{{ __('Absent (R)') }}</span>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>
                                                {{ __('Fermer') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @empty
                <td colspan="16">{{ __('Aucun pointage trouvé pour ce mois et cette année.') }}</td>
                @endforelse
            </tbody>
            
        </table>
    @endsection


    @section('js')
        <script>
        
            // Tout sélectionner/désélectionner pour toutes les cases
            function toggleAll(source) {
                const checkboxes = document.querySelectorAll('input[type="checkbox"]:not(#checkAll)');
                checkboxes.forEach(checkbox => checkbox.checked = source.checked);
            }

            // Tout sélectionner/désélectionner pour une ligne spécifique
            function toggleRow(source, employeeId) {
                const rowCheckboxes = document.querySelectorAll(`.checkbox-${employeeId}`);
                rowCheckboxes.forEach(checkbox => checkbox.checked = source.checked);
            }

            $(document).ready(function() {

                $('.select2').select2({
    placeholder: "{{ __('Sélectionner ...') }}",  
    allowClear: true,  
    width: '100%', 
});
                $('.datatable').DataTable({
                    
                    "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                        "<'table-responsive'tr>" +
                        "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                    "oLanguage": {
                        "oPaginate": {
                            "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                            "sNext": '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                        },
                        "sInfo": "{{ __('Affichage de la page _PAGE_ sur _PAGES_') }}",
                        "sSearch": "{{ __('Recherche') }}",
                        "sLengthMenu": "{{ __('Afficher _MENU_ éléments') }}",
                        "sInfoEmpty": "{{ __('Aucun résultat disponible') }}",
                        "sInfoFiltered": "{{ __('(filtré à partir de _MAX_ éléments)') }}",
                    },
                    "stripeClasses": []
                });
            });
        </script>
    @endsection
