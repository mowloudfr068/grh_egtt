@extends('layouts.site.master')

@section('css')
<style>
    .table th, .table td {
    vertical-align: middle;
    text-align: left;
}

.thead-dark th {
    background-color: #343a40;
    color: white;
}

.table td span {
    border-radius: 3px;
    padding: 2px 5px;
    display: inline-block;
}

.select2-container {
        width: 50% !important;
    }

    .select2-selection {
        height: 50px !important;
    }

</style>
@endsection

@section('title')
    {{ __('Détails Paiements') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                
                <div class="mt-2">
                    <a href="#">{{ __('Salaires/Details') }}</a>/<a href="#">{{ __('Employés') }}</a>
                
                
                </div>

                <form action="{{ route('employee.paiements.details') }}" method="get"> 
                    <div class="form-row mb-3">
                        <div class="col-md-4">
                            <label for="month_id">{{ __('Mois') }}</label>
                            <select class="form-control select2" onchange="this.form.submit()" id="month_id" name="month_id">
                                @foreach ($months as $month)
                                    <option value="{{ $month->id }}" {{ request('month_id') == $month->id ? 'selected' : '' }}>
                                        {{ $month->libelle }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="year">{{ __('Année') }}</label>
                            <input type="number" class="form-control" id="year" name="year" value="{{ request('year', date('Y')) }}" min="2000">
                        </div>
                    
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Filtre par mois -->

    <!-- Affichage des totaux -->
    {{-- <div class="totals-summary mt-3 p-2 d-flex justify-content-start align-items-center" style="background-color: #e8f5e9; border-radius: 5px; color: #388e3c;">
        <h5 class="mr-4">
            <strong>Sous Total :</strong> {{ number_format($sousTotal, 2) }} UM
        </h5>
        <h5 class="mr-4">
            <strong>Majoration 13% :</strong> {{ number_format($majoration13, 2) }} UM
        </h5>
        <h5>
            <strong>Total Général :</strong> {{ number_format($totalGeneral, 2) }} UM
        </h5>
    </div> --}}

    <table class="table table-bordered" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>{{ __('Matricule') }}</th>
                <th>{{ __('Nom Complet') }}</th>
                <th>{{ __('Détails Salaire') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($payments as $payment)
                @php
                    // Calcul des jours travaillés
                    $daysArray = json_decode($payment->attendance->days, true);
                    $joursTravailles = is_array($daysArray) ? count($daysArray) : 0;
                @endphp
                <tr>
                    <!-- Informations générales -->
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $payment->attendance->employee->matricule }}</td>
                    <td>{{ $payment->attendance->employee->first_name." ".$payment->attendance->employee->last_name }}</td>
                    <td>
                        <table class="table table-sm">
                            <tr>
                                <td><strong>{{ __('Mois') }}</strong></td>
                                <td>{{ $payment->attendance->month->libelle }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Jour Travaille') }}</strong></td>
                                <td>{{ $joursTravailles }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('ORD') }}</strong></td>
                                <td>{{ $payment->ordre ?? 'N/A' }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Sal. Brut Mensuel (MRU)') }}</strong></td>
                                <td>
                                    <span style="color: white; background-color: #009688; padding: 2px;">
                                        {{ number_format($payment->salaire_brut_mensuel, 2) }}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Cnam Employé') }}</strong></td>
                                <td>{{ number_format($payment->cnam_employee, 2) ?? 'N/A' }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Cnss Employé') }}</strong></td>
                                <td>{{ number_format($payment->cnss_employee, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('ITS') }}</strong></td>
                                <td>{{ number_format($payment->its, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Sal. Net') }}</strong></td>
                                <td>
                                    <span style="color: white; background-color: #009688; padding: 2px;">
                                        {{ number_format($payment->salaire_net, 2) }}
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Cnam Patron') }}</strong></td>
                                <td>{{ number_format($payment->cnam_patron, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Médecin Travail') }}</strong></td>
                                <td>{{ number_format($payment->medecin_travail, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Cnss Patron') }}</strong></td>
                                <td>{{ number_format($payment->cnss_patron, 2) }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Congé') }}</strong></td>
                                <td>{{ $payment->conges }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('Cout Total') }}</strong></td>
                                <td>
                                    <span style="color: white; background-color: #FF5252; padding: 2px;">
                                        {{ number_format($payment->total_cout, 2) }}
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">{{ __('Aucun paiement trouvé pour ce mois et cette année.') }}</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    
    
@endsection

@section('js')
<script>
   $(document).ready(function() {

    $('.select2').select2({
    placeholder: "{{ __('Sélectionner ...') }}",  
    allowClear: true,  
    width: '100%', 
});

        $('table.multi-table').DataTable({
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7,
            drawCallback: function() {
                $('.t-dot').tooltip({
                    template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                })
                $('.dataTables_wrapper table').removeClass('table-striped');
            }
        });
    });

</script>
@endsection
