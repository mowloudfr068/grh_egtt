@extends('layouts.site.master')
@section('title')
    dashboard 
@endsection
@section('css')
<style>
    .dashboard-background {
    background-image: url({{ asset('images/EGTT.jpg') }}); /* Remplacez cette URL par le chemin de votre image */
    background-size: cover; /* Couvre toute la zone */
    background-position: center center; /* Centre l'image */
    background-repeat: no-repeat; /* Empêche la répétition de l'image */
    height: 100vh; /* Ajuste la hauteur pour occuper toute la page */
}

</style>

@endsection
@section('content')
<div class="layout-px-spacing dashboard-background"> <!-- Ajoutez la classe ici -->
    <div class="row layout-top-spacing">

        <!-- Les autres widgets ici restent inchangés -->

    </div>
</div>
@endsection