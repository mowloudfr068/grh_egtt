@extends('layouts.site.master')

@section('css')
<style>
    .section-title {
        font-size: 1.5rem;
        font-weight: bold;
        text-align: center;
        margin-bottom: 20px;
    }
    .info-section {
        margin-bottom: 30px;
    }
    .info-item {
        display: flex;
        justify-content: space-between;
        margin-bottom: 10px;
        padding: 10px;
        border: 1px solid #ddd;
        border-radius: 5px;
        background-color: #f9f9f9;
    }
    .info-item strong {
        margin-right: 10px;
    }

    .photo-section {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 20px;
}

.profile-photo {
    width: 150px;
    height: 150px;
    border-radius: 50%;
    object-fit: cover;
    border: 3px solid #ddd;
    box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
}

.default-photo {
    width: 150px;
    height: 150px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #f0f0f0;
    color: #aaa;
    font-size: 14px;
    border: 3px solid #ddd;
    box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
}

</style>
@endsection

@section('title')
    {{ __('Détails Employé') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-9 mt-2">
                <a href="#">{{ __('Accueil') }}</a> / <a href="#">{{ __('Détails Employé') }}</a>
            </div>
            <div class="col-3 text-right">
                <a href="{{ route('employees.index') }}" class="btn btn-info btn-sm">
                    <i class="fas fa-arrow-left"></i>
    {{ __('Retour') }}
                </a>
                <!-- Bouton Modifier -->
                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal">
                   <i class="fas fa-edit"></i> {{ __('Modifier') }}
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="container">
    
    <!-- Photo de Profil -->
    <div class="photo-section text-center mb-4">
        @if ($employee->photo)
        <img src="{{ asset('storage/' . $employee->photo) }}" alt="Photo de Profil" class="profile-photo">
        @else
            <div class="default-photo">
                <span>{{ __('Aucune photo disponible') }}</span>
            </div>
        @endif
    </div>

    <!-- Informations Personnelles -->
    <div class="info-section">
        <div class="section-title">{{ __('Informations Personnelles') }}</div>
        <div class="info-item">
            <strong>{{ __('Prénom :') }}</strong>
            <span>{{ $employee->first_name }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Nom :') }}</strong>
            <span>{{ $employee->last_name }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Date de naissance :') }}</strong>
            <span>{{ \Carbon\Carbon::parse($employee->date_of_birth)->format('d/m/Y') }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Adresse :') }}</strong>
            <span>{{ $employee->address }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Téléphone :') }}</strong>
            <span><a href="tel:{{ $employee->phone }}">{{ $employee->phone }}</a></span>
        </div>
        <div class="info-item">
            <strong>{{ __('Email :') }}</strong>
            <span><a href="mailto:{{ $employee->email }}">{{ $employee->email }}</a></span>
        </div>
    </div>

    <!-- Informations Professionnelles -->
    <div class="info-section">
        <div class="section-title">{{ __('Informations Professionnelles') }}</div>
        <div class="info-item">
            <strong>{{ __('NNI :') }}</strong>
            <span>{{ $employee->nni }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Matricule :') }}</strong>
            <span>{{ $employee->matricule }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Position :') }}</strong>
            <span>{{ $employee->position->title }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Département :') }}</strong>
            <span>{{ $employee->position->department->name }}</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Salaire :') }}</strong>
            <span>{{ $employee->position->base_salary }} MRU</span>
        </div>
        <div class="info-item">
            <strong>{{ __('Date d\'embauche :') }}</strong>
            <span>{{ \Carbon\Carbon::parse($employee->date_of_hire)->format('d/m/Y') }}</span>
        </div>

        <!-- Type de contrat -->
        <div class="info-item">
            <strong>{{ __('Type de contrat :') }}</strong>
            <span>{{ $employee->contract_type }}</span>
        </div>

        <!-- Durée du contrat (affiché uniquement pour CDD) -->
        @if ($employee->contract_type === 'CDD')
            <div class="info-item">
                <strong>{{ __('Durée du contrat :') }}</strong>
                <span>{{ $employee->contract_duration }} ans</span>
            </div>


        @endif
    </div>
    <!-- Télécharger le contrat -->
    <div class="info-section text-center">
        @if($employee->contract_type === 'CDD')
            <a href="{{ route('employee.contracts.cdd', $employee->id) }}" class="btn btn-primary" title="Télécharger Contrat CDD">
                <i class="fas fa-download"></i> Telecharger Votre Contrat CDD en PDF
            </a>
        @else
            <a href="{{ route('employee.contracts.cdi', $employee->id) }}" class="btn btn-primary" title="Télécharger Contrat CDI">
                <i class="fas fa-download"></i>Telecharger Votre Contrat CDI en PDF
            </a>
        @endif
    </div>

    
</div>

<!-- Modal Modifier -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">{{ __('Modifier les Informations') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('employee.EspaceEmpUpdate', $employee->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <!-- Champ pour la photo -->
                    <div class="form-group">
                        <label for="photo">{{ __('Photo de Profil') }}</label>
                        <div class="custom-file">
                            <input type="file" name="photo" id="photo" class="custom-file-input" onchange="previewImage(event)">
                            <label class="custom-file-label" for="photo">{{ __('Choisir une photo') }}</label>
                        </div>
                        <div id="photoPreview" class="mt-3">
                            @if ($employee->photo)
                                <img src="{{ asset('storage/' . $employee->photo) }}" alt="Photo" class="img-thumbnail" style="max-width: 150px; height: auto;">
                            @else
                                <img id="previewImage" src="#" alt="Aucune image" class="img-thumbnail" style="max-width: 150px; height: auto; display: none;">
                            @endif
                        </div>
                    </div>

                    <!-- Champ pour le téléphone -->
                    <div class="form-group">
                        <label for="phone">{{ __('Téléphone') }}</label>
                        <input type="text" name="phone" id="phone" class="form-control" value="{{ $employee->phone }}" required>
                    </div>

                    <!-- Champ pour l'adresse -->
                    <div class="form-group">
                        <label for="address">{{ __('Adresse') }}</label>
                        <input type="text" name="address" id="address" class="form-control" value="{{ $employee->address }}" required>
                    </div>

                    <!-- Champ pour le mot de passe -->
                    <div class="form-group">
                        <label for="password">{{ __('Mot de passe') }}</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Nouveau mot de passe') }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Sauvegarder') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Script pour prévisualisation d'image -->
<script>
    function previewImage(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('previewImage');
            output.style.display = 'block';
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    }
</script>
@endsection
