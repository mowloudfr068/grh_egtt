<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Code de Validation</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 0;
        }
        .email-container {
            max-width: 600px;
            margin: 20px auto;
            background: #ffffff;
            border: 1px solid #ddd;
            border-radius: 8px;
            overflow: hidden;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .header {
            background-color: #007bff;
            color: white;
            padding: 20px;
            text-align: center;
            font-size: 24px;
        }
        .content {
            padding: 20px;
            color: #333;
            line-height: 1.6;
        }
        .code-box {
            text-align: center;
            background-color: #f8f9fa;
            padding: 15px;
            border-radius: 5px;
            font-size: 18px;
            font-weight: bold;
            letter-spacing: 2px;
            color: #007bff;
            margin: 20px 0;
        }
        .footer {
            background-color: #f8f9fa;
            text-align: center;
            padding: 10px;
            font-size: 14px;
            color: #666;
        }
        .btn {
            display: inline-block;
            margin: 20px auto;
            padding: 10px 20px;
            background-color: #007bff;
            color: white;
            text-decoration: none;
            font-size: 16px;
            border-radius: 5px;
        }
        .btn:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="email-container">
        <!-- Header -->
        <div class="header">
            Code de Validation
        </div>

        <!-- Content -->
        <div class="content">
            <p>Bonjour,</p>
            <p>Vous avez demandé à modifier vos informations. Veuillez utiliser le code ci-dessous pour valider vos modifications :</p>
            <div class="code-box">
                {{ $validationCode }}
            </div>
            <p>Entrez ce code dans le formulaire pour finaliser le processus de validation.</p>
            <p>Si vous n'êtes pas à l'origine de cette demande, veuillez ignorer cet email.</p>
        </div>

        <!-- Footer -->
        <div class="footer">
            <p>&copy; {{ date('Y') }} Votre Application. Tous droits réservés.</p>
        </div>
    </div>
</body>
</html>
