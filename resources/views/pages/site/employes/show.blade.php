@extends('layouts.site.master')

@section('title')
    {{ __('Validation de Modifications') }}
@endsection

@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-9 mt-2">
                <a href="#">{{ __('Accueil') }}</a> / <a href="#">{{ __('Validation de Modifications') }}</a>
            </div>
            <div class="col-3 text-right">
                <a href="{{ route('employee.info') }}" class="btn btn-secondary btn-sm">
                    {{ __('Retour') }}
                </a>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="container">
    <h3 class="text-center">{{ __('Validation de vos Modifications') }}</h3>

    @if(session('message'))
        <div class="alert alert-info">
            {{ session('message') }}
        </div>
    @endif


 

    <form action="{{ route('employee.validate', $employee->id) }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="validation_code">{{ __('Code de Validation') }}</label>
            <input type="text" name="validation_code" id="validation_code" class="form-control" placeholder="Entrez le code reçu" required>
        </div>

        <div class="text-center mt-4">
            <button type="submit" class="btn btn-primary">{{ __('Valider les Modifications') }}</button>
        </div>
    </form>
</div>
@endsection
