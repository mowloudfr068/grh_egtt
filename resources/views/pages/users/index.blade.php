@extends('layouts.master')
@section('css')
<style>
    .select2-container {
    width: 100% !important;
}
.select2-selection {
    height: 50px !important;
}
</style>
@endsection
@section('title')
    {{ __('Gestion des utilisateurs') }}
@endsection
@section('content-header')
    <div class="widget-header p-2">
        <div class="row">
            <div class="col-xl-9 col-md-9 col-sm-9 col-9">
                <div class="mt-2">
                    <a href="#">{{ __('Accueil') }}</a> / <a href="#">{{ __('Utilisateurs') }}</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-3 col-3 text-right">
                @can('ajout_utilisateur')
                 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addUserModal">
                    <i class="fas fa-add"></i>     {{ __('Ajouter Utilisateur') }}
                    </button>
                @endcan
            </div>
        </div>
    </div>
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="multi-table table table-striped table-bordered table-hover non-hover" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('Nom') }}</th>
                <th>{{ __('Email') }}</th>
                <th>{{ __('Roles') }}</th>
                <th>{{ __('Statut') }}</th>
                <th class="text-center">{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach($user->getRoleNames() as $role)
                            <label class="badge badge-success">{{ $role }}</label>
                        @endforeach
                    </td>
                    <td>
                        @if($user->status === 'activer')
                            <span class="badge badge-success">{{ __('Activer') }}</span>
                        @else
                            <span class="badge badge-danger">{{ __('Désactiver') }}</span>
                        @endif
                    </td>
                    <td class="text-center">
                        @can('voir_utilisateur')
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#showUserModal{{ $user->id }}">
                                <i class="fas fa-eye"></i>
                            </button>
                        @endcan
                        {{-- Vérifier si l'utilisateur n'est pas "Owner" pour afficher le bouton de modification --}}
    @if(!in_array('owner', $user->getRoleNames()->toArray()))
    @can('modifier_utilisateur')
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editUserModal{{ $user->id }}">
            <i class="fas fa-edit"></i>
        </button>
    @endcan
@endif
                        {{-- Vérifier si l'utilisateur n'est pas "Owner" pour afficher le bouton de suppression --}}
    @if(!in_array('owner', $user->getRoleNames()->toArray()))
    @can('supprimer_utilisateur')
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteUserModal{{ $user->id }}">
            <i class="fas fa-trash"></i>
        </button>
    @endcan
@endif
                    </td>
                </tr>

                <!-- Modal Show User -->
                <div class="modal fade" id="showUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ __('Détails de l’utilisateur') }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p><strong>{{ __('Nom:') }}</strong> {{ $user->name }}</p>
                                <p><strong>{{ __('Email:') }}</strong> {{ $user->email }}</p>
                                <p><strong>{{ __('Roles:') }}</strong> 
                                    @foreach($user->getRoleNames() as $role)
                                        <label class="badge badge-success">{{ $role }}</label>
                                    @endforeach
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Edit User -->
                <div class="modal fade" id="editUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ __('Modifier Utilisateur') }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
            {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
                                    <div class="form-group">
                                    <strong>{{ __('Nom:') }}</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                    </div>
                                <div class="form-group">
                                    <strong>{{ __('Email:') }}</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                                    </div>
                                <div class="form-group">
                                    <strong>{{ __('mot de passe:') }}</strong>
            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                                </div>
                                            <div class="form-group">
                                    <strong>{{ __('confirmer mot de passe:') }}</strong>
            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                    </div>
                                <div class="form-group">
                                    <strong>{{ __('Roles:') }}</strong>
            {!! Form::select('roles[]', $roles,$user->roles->pluck('name','name')->all(), array('class' => 'form-control select2','multiple')) !!}
                                    </div>
                                <div class="form-group">
                                    <strong>{{ __('Statut:') }}</strong>
                                    <select name="status" class="form-control select2">
                                        <option value="activer" {{ $user->status === 'activer' ? 'selected' : '' }}>{{ __('Activer') }}</option>
                                        <option value="desactiver" {{ $user->status === 'desactiver' ? 'selected' : '' }}>{{ __('Désactiver') }}</option>
                                    </select>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">{{ __('Modifier') }}</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Delete User -->
                <div class="modal fade" id="deleteUserModal{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ __('Supprimer Utilisateur') }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>{{ __('Êtes-vous sûr de vouloir supprimer cet utilisateur ?') }}</p>
                            </div>
                            <div class="modal-footer">
                                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                <button type="submit" class="btn btn-danger">{{ __('Supprimer') }}</button>
                                {!! Form::close() !!}
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Annuler') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </tbody>
    </table>

    <!-- Modal Add User -->
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Ajouter Utilisateur') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        <strong>{{ __('Nom:') }}</strong>
                        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>{{ __('Email:') }}</strong>
                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                    </div>
                    <div class="form-group">
                        <strong>{{ __('Mot de passe:') }}</strong>
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Mot de passe']) !!}
                    </div>
                    <div class="form-group">
                        <strong>{{ __('Confirmer mot de passe :') }}</strong>
                        {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}            
                     </div>
                     <div class="form-group">
                        <strong>statut:</strong>
                        <select name="status" id="" class="form-control select2">
                            <option value="activer">activer</option>
                            <option value="desactiver">desactiver</option>
                        </select>
                        </div>
                    <div class="form-group">
                        <strong>{{ __('Roles:') }}</strong>
                        {!! Form::select('roles_name[]', $roles,[], array('class' => 'form-control select2','multiple')) !!}
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">{{ __('Ajouter') }}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
        // Initialiser Select2
        $('.select2').select2({
            placeholder: "Sélectionner ...",  // Placeholder pour le champ
            allowClear: true,  // Permet de supprimer la sélection
            width: '100%', 
        });
    });

        $(document).ready(function() {
            $('table.multi-table').DataTable({
                "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                    "<'table-responsive'tr>" +
                    "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [7, 10, 20, 50],
                "pageLength": 7,
                drawCallback: function() {
                    $('.t-dot').tooltip({
                        template: '<div class="tooltip status" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                    })
                    $('.dataTables_wrapper table').removeClass('table-striped');
                }
            });
        });
    </script>
@endsection
