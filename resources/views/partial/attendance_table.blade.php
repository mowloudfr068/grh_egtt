@if ($selectedMonth && count($days) > 0)
    <form action="{{ route('attendances.store') }}" method="post">
        @csrf
        <input type="hidden" name="month_id" value="{{ $selectedMonth->id }}">
        <input type="hidden" name="year" value="{{ now()->year }}">

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>{{ __('Nom complet') }}</th>
                    <th>{{ __('Matricule') }}</th>
                    <th>{{ __('Statut') }}</th>
                    <th>{{ __('Tout sélectionner') }}</th>
                    @foreach ($days as $day)
                        <th>jr {{ $day }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)
                    @php
                        $isAttended = isset($attendanceData[$employee->id]) && count($attendanceData[$employee->id]) > 0;
                    @endphp
                    <tr>
                        <td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
                        <td>{{ $employee->matricule }}</td>
                        <td>
                            <span class="badge {{ $isAttended ? 'badge-success' : 'badge-warning' }}">
                                {{ $isAttended ? __('Déjà pointé') : __('En attente') }}
                            </span>
                        </td>
                        <td>
                            <input type="checkbox" class="select-all-row" onclick="toggleRow(this, '{{ $employee->id }}')" {{ $isAttended ? 'disabled' : '' }}>
                        </td>
                        @foreach ($days as $day)
                            <td>
                                <input type="checkbox" 
                                       name="attendance[{{ $employee->id }}][{{ $day }}]" 
                                       value="present" 
                                       class="checkbox-{{ $employee->id }}" 
                                       {{ $isAttended ? 'disabled' : '' }}>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="text-center mt-3">
            <button type="submit" class="btn btn-success">{{ __('Enregistrer les présences') }}</button>
        </div>
    </form>
@else
    <div class="alert alert-info text-center">
        {{ __('Aucun jour disponible pour ce mois.') }}
    </div>
@endif
