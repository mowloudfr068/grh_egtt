
<style>
    .sidebar-wrapper .menu {
    margin-bottom: 15px; /* Ajoute un espacement vertical entre les menus */
}

.sidebar-wrapper .menu a {
    padding: 10px 15px; /* Augmente la taille du padding pour un espacement intérieur plus confortable */
    border-radius: 5px; /* Ajoute un léger arrondi pour un design plus moderne */
    transition: background-color 0.3s ease; /* Ajoute une transition pour un effet visuel fluide */
}

.sidebar-wrapper .menu a:hover {
    background-color: #f4f4f4; /* Change le fond au survol */
}

</style>
<div class="sidebar-wrapper sidebar-theme">
    <nav id="sidebar">
        <ul class="navbar-nav theme-brand flex-row text-center">
            <li class="nav-item theme-text">
                <a href="index.html" class="nav-link">Espace Employés</a>
            </li>
            <li class="nav-item toggle-sidebar">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather sidebarCollapse feather-chevrons-left">
                    <polyline points="11 17 6 12 11 7"></polyline>
                    <polyline points="18 17 13 12 18 7"></polyline>
                </svg>
            </li>
        </ul>
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu active">
                <a href="#dashboard" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                            stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                            class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>{{ __('Tableau De Bord') }}</span>
                    </div>
                </a>
            </li>
            <li class="menu">
                <a href="#info" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor"
    stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
    <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
    <circle cx="12" cy="7" r="4"></circle>
</svg>

                        <span>{{ __('Profil') }}</span>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="info" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('employee.info') }}">{{ __('Voir mes informations') }}</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#conges" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar">
                            <rect x="3" y="4" width="18" height="16" rx="2" ry="2"></rect>
                            <line x1="16" y1="2" x2="16" y2="6"></line>
                            <line x1="8" y1="2" x2="8" y2="6"></line>
                            <line x1="3" y1="10" x2="21" y2="10"></line>
                          </svg>
                          
                        <span>{{ __('Congés') }}</span>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="conges" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('employee.conges') }}">{{ __('Demander un congé') }}</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#pointages" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock">
                            <circle cx="12" cy="12" r="10"></circle>
                            <line x1="12" y1="6" x2="12" y2="12"></line>
                            <line x1="12" y1="12" x2="16" y2="10"></line>
                          </svg>
                          
                        <span>{{ __('Pointages') }}</span>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="pointages" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('employee.pointages') }}">{{ __('Voir mes pointages') }}</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#payments" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-credit-card">
                            <rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect>
                            <line x1="1" y1="10" x2="23" y2="10"></line>
                          </svg>
                          
                          
                          
                        <span>{{ __('Paiements') }}</span>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="payments" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('employee.paiements.details') }}">{{ __('Voir mes détails des paiements') }}</a>
                    </li>
                </ul>
            </li>
            <li class="menu">
                <a href="#bultin" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8l-6-6z"></path>
                            <line x1="14" y1="2" x2="14" y2="8"></line>
                            <line x1="6" y1="10" x2="18" y2="10"></line>
                            <line x1="6" y1="14" x2="18" y2="14"></line>
                            <line x1="6" y1="18" x2="18" y2="18"></line>
                          </svg>
                          
                        <span>{{ __('Bulletins') }}</span>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="bultin" data-parent="#accordionExample">
                    <li>
                        <a href="{{ route('employee.bultin') }}">{{ __('Voir bulletin de salaire') }}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
