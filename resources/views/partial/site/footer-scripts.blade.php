 <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
 <script src="{{ asset('assets/js/libs/jquery-3.1.1.min.js') }}"></script>
 <script src="{{ asset('assets/js/jqueryRepeater.js') }}"></script>
 <script src="{{ asset('bootstrap/js/popper.min.js') }}"></script>
 <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
 <script src="{{ asset('plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
 <script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/loader.js') }}"></script>

 <script>
     $(document).ready(function() {
         App.init();
     });
 </script>

 <!-- END GLOBAL MANDATORY SCRIPTS -->
 <script src="{{ asset('plugins/table/datatable/datatables.js') }}"></script>


 <script src="{{ asset('assets/js/custom.js') }}"></script>
 <!-- END GLOBAL MANDATORY SCRIPTS -->

 <!-- BEGIN PAGE LEVEL SCRIPTS -->
 <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
 <script src="{{ asset('assets/js/components/ui-accordions.js') }}"></script>
 <script src="{{ asset('plugins/select2/custom-select2.js') }}"></script>
 <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
 <script src="{{ asset('plugins/apex/apexcharts.min.js') }}"></script>
 <script src="{{ asset('assets/js/dashboard/dash_1.js') }}"></script>

