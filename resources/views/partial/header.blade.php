<!-- BEGIN LOADER -->
<style>
.notification-indicator {
    display: inline-block;
}

.notification-count {
    position: relative;
    display: inline-block;
    width: 22px; 
    height: 22px; 
    line-height: 22px;
    font-size: 14px;
    color: #fff;
    text-align: center;
    background-color: #dc3545;
    border-radius: 50%;
    z-index: 2;
    box-shadow: 0 0 15px rgba(220, 53, 69, 0.7);
}

.ripple::after {
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    width: 20px; 
    height: 20px;
    background-color: rgba(220, 53, 69, 0.3); 
    border-radius: 50%;
    transform: translate(-50%, -50%) scale(1);
    z-index: 1;
    animation: ripple-animation 2s infinite;
    pointer-events: none; 
}

@keyframes ripple-animation {
    0% {
        transform: translate(-50%, -50%) scale(1);
        opacity: 0.8;
    }
    50% {
        transform: translate(-50%, -50%) scale(1.8); 
        opacity: 0.4;
    }
    100% {
        transform: translate(-50%, -50%) scale(2.5); 
        opacity: 0;
    }
}



.logo_app {
    width: 70%; 
    max-width: 100px;
    display: block;
    border-radius: 10%;
    margin-left:0;

}




</style>
<div id="load_screen">
    <div class="loader">
        <div class="loader-content">
            <div class="spinner-grow align-self-center"></div>
        </div>
    </div>
</div>
<!-- END LOADER -->

<!-- BEGIN NAVBAR -->
<div class="header-container fixed-top">
    <header class="header navbar navbar-expand-sm">
        <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-menu">
                <line x1="3" y1="12" x2="21" y2="12"></line>
                <line x1="3" y1="6" x2="21" y2="6"></line>
                <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg>
        </a>
        <ul class="navbar-item flex-row search-ul">
            <li class="nav-item align-self-center search-animated">
                <img src="{{ asset('images/egtt_logo.png') }}" class="logo_app" alt="logo App">

            </li>
        </ul>
        <ul class="navbar-item flex-row navbar-dropdown">
            @can('notifications')
                
            <li class="nav-item dropdown notification-dropdown mt-2">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle d-flex align-items-center" id="notificationDropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell me-2">
                        <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                        <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                    </svg>
                    <span id="indicateurNotification">
                    @if (auth()->user()->unreadNotifications->count() > 0)
    <div class="notification-indicator ">
        <span class="badge bg-danger rounded-pill notification-count ripple" id="indicateurNotification">
            {{ auth()->user()->unreadNotifications->count() }}
        </span>
    </div>
@endif
</span>
  </a>
            
                <div class="dropdown-menu dropdown-menu-end p-3 shadow-lg" aria-labelledby="notificationDropdown">
                    <div class="notification-title">
                        <h6 class="mb-3 text-center text-primary">
                            Notifications
                            <span class="badge bg-danger rounded-pill" id="notifications_count">
                                {{ auth()->user()->unreadNotifications->count() }}
                            </span>
                        </h6>
                    </div>
                    <div id="unreadNotifications">
                    @foreach (auth()->user()->unreadNotifications as $notification)
                        <div class="notification-list list-group">
                            <a href="{{ route('notification.read',$notification->id) }}" 
                               class="list-group-item list-group-item-action notification-link">
                                <div class="d-flex">
                                    <div class="me-3">
                                        <i class="bi bi-check-circle text-success"></i>
                                    </div>
                                    <div>
                                        <h6 class="mb-1">{{ $notification->data['title'] }} {{ $notification->data['user'] }}</h6>
                                        <small class="text-muted">{{ $notification->created_at->diffForHumans() }}</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    </div>
                    <div class="dropdown-footer mt-3 text-center">
                        <a href="{{ route('MarkAsRead_all') }}" class="btn btn-link">Considérer comme tout lu</a>
                    </div>
                </div>
            </li>
            @endcan

            <li class="nav-item dropdown user-profile-dropdown order-lg-0 order-1">
                <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{ asset('images/logo_user.png') }}" alt="Photo de Profil" class="img-fluid mr-2">
                </a>
                <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                    <div class="user-profile-section">
                        <div class="media mx-auto">
                            <img src="{{ asset('images/logo_user.png') }}" alt="Photo de Profil" class="img-fluid mr-2">
                            <div class="media-body">
                                <h5>@if (Auth::guard('employee')->check())
                                    {{ Auth::guard('employee')->user() }}
                                @elseif (Auth::check()) 
                                    {{ Auth::user()->name }}  <!-- Pour les utilisateurs authentifiés via le guard `web` -->
                                @endif
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-item">
                        <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out">
                                <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                <polyline points="16 17 21 12 16 7"></polyline>
                                <line x1="21" y1="12" x2="9" y2="12"></line>
                            </svg>
                            <span>Log Out</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </header>
</div>
<!-- END NAVBAR -->
