<div class="sidebar-wrapper sidebar-theme">
    <nav id="sidebar">
        <ul class="navbar-nav theme-brand flex-row  text-center">
            <li class="nav-item theme-text">
                <a href="index.html" class="nav-link">EGTT Admin</a>
            </li>
            <li class="nav-item toggle-sidebar">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather sidebarCollapse feather-chevrons-left">
                    <polyline points="11 17 6 12 11 7"></polyline>
                    <polyline points="18 17 13 12 18 7"></polyline>
                </svg>
            </li>
        </ul>
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu active">
                <a href="{{ route('dashboard.statistique') }}"  aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>{{__('Tableau De Bord')}}</span>
                    </div>
                    <div>
                      
                    </div>
                </a>
              
            </li>
            @can('departements') 
                
            <li class="menu">
                <a href="#components" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-briefcase">
                            <path d="M7 2H5a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2h-2" />
                            <path d="M7 2h10M12 8v8" />
                          </svg>
                          
                                                  
                        <span>{{ __('Départements') }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="components" data-parent="#accordionExample">
                    @can('list_departement')
                      <li>
                        <a href="{{ route('departments.index') }}">{{ __('Liste des départements') }}</a>
                    </li>  
                    @endcan
                    
                </ul>
            </li>
             @endcan
            @can('positions')
            <li class="menu">
                <a href="#position" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin">
                            <path d="M12 2C8.13 2 5 5.13 5 8c0 2.5 2.5 6 5 10s5-7.5 5-10c0-2.87-3.13-6-5-6z" />
                          </svg>
                          

                        <span>{{__('Positions')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="position" data-parent="#accordionExample">
                   @can('list_position')
                    <li>
                        <a href="{{ route('positions.index') }}">  {{__('Liste des Positions')}} </a>
                    </li>   
                   @endcan
                    
                </ul>
            </li>
            @endcan
            @can('emploiyes')
            <li class="menu">
                <a href="#elements" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
                            <circle cx="12" cy="7" r="4" />
                            <path d="M14 14l1 4H9l1-4" />
                          </svg>
                          

                        <span>{{__('Employés')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="elements" data-parent="#accordionExample">
                   @can('list_emploiye')
                    <li>
                        <a href="{{ route('employees.index') }}"> {{__('Liste des employés')}} </a>
                    </li>   
                   @endcan
                    
                </ul>
            </li>    
            @endcan
            @can('utilisateurs')
            <li class="menu">
                <a href="#Users" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
                            <circle cx="12" cy="7" r="4" />
                          </svg>
                          

                        <span>{{__('Utilisateurs')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Users" data-parent="#accordionExample">
                   @can('list_utilisateur')
                     <li>
                        <a href="{{ route('users.index') }}"> {{__('Liste des utilisateurs')}} </a>
                    </li>  
                   @endcan
                    @can('list_roles_utilisateur')
                     <li>
                        <a href="{{ route('roles.index') }}"> {{__('les droit d\'acces des utilisateurs ')}} </a>
                    </li>   
                    @endcan
                    
                </ul>
            </li>
            @endcan
            @can('conges')
            <li class="menu">
                <a href="#section" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar">
                            <rect x="3" y="4" width="18" height="16" rx="2" ry="2"></rect>
                            <line x1="16" y1="2" x2="16" y2="6"></line>
                            <line x1="8" y1="2" x2="8" y2="6"></line>
                            <line x1="3" y1="10" x2="21" y2="10"></line>
                          </svg>                                                  
                        <span>{{__('Congés')}}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="section" data-parent="#accordionExample">
                    @can('list_conges')
                       <li>
                        <a href="{{ route('leaves.index') }}"> {{ __('Liste des congés') }} </a>
                    </li>  
                    @endcan
                   
                </ul>
            </li>
            @endcan
            @can('presences')
            <li class="menu">
                <a href="#etudiant" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clock">
                            <circle cx="12" cy="12" r="10"></circle>
                            <line x1="12" y1="6" x2="12" y2="12"></line>
                            <line x1="12" y1="12" x2="16" y2="10"></line>
                          </svg>

                        <span>{{ __('Presence')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="etudiant" data-parent="#accordionExample">
                    @can('ajout_presences')
                     <li>
                        <a href="{{ route('attendances.index') }}"> {{ __('Ajout des presences')  }} </a>
                    </li>   
                    @endcan
                    @can('list_presences')
                      <li>
                        <a href="{{ route('attendances.list') }}"> {{ __('Liste des pointages')  }} </a>
                    </li>  
                    @endcan
                    
                </ul>
            </li>
            @endcan
            @can('paiements')
            <li class="menu">
                <a href="#Teachers" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-credit-card">
                            <rect x="1" y="4" width="22" height="16" rx="2" ry="2"></rect>
                            <line x1="1" y1="10" x2="23" y2="10"></line>
                          </svg>
                          
                        <span>{{ __('Salaires')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Teachers" data-parent="#accordionExample">
                    @can('ajout_paiements')
                    <li>
                        <a href="{{ route('payments.index') }}"> {{ __('Liste des salaires')  }} </a>
                    </li>    
                    @endcan
                    @can('list_paiements')
                     <li>
                        <a href="{{ route('payments.Details') }}"> {{ __('Voir les datails des paiements')  }} </a>
                    </li>   
                    @endcan
                    
                </ul>
            </li>    
            @endcan
            @can('bultins')
            <li class="menu">
                <a href="#Bultin" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                            <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8l-6-6z"></path>
                            <line x1="14" y1="2" x2="14" y2="8"></line>
                            <line x1="6" y1="10" x2="18" y2="10"></line>
                            <line x1="6" y1="14" x2="18" y2="14"></line>
                            <line x1="6" y1="18" x2="18" y2="18"></line>
                          </svg>
                          
                        <span>{{ __('Impression du Bultin')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Bultin" data-parent="#accordionExample">
                    @can('list_emploiyee_bultin')
                    <li>
                        <a href="{{ route('bulletin.generate') }}"> {{ __('Bultin Employe')  }} </a>
                    </li>    
                    @endcan
                </ul>
            </li>
            @endcan
            @can('factures')
            <li class="menu">
                <a href="#Invoice" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                            <path d="M14 2H6a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8l-6-6z"/>
                            <line x1="14" y1="2" x2="14" y2="8"/>
                            <line x1="6" y1="10" x2="18" y2="10"/>
                            <line x1="6" y1="14" x2="18" y2="14"/>
                            <line x1="6" y1="18" x2="18" y2="18"/>
                          </svg>
                          
                        <span>{{ __('Les Factures')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Invoice" data-parent="#accordionExample">
                  @can('list_factures')
                    <li>
                        <a href="{{ route('invoices.index') }}"> {{ __('voir les factures')  }} </a>
                    </li>  
                  @endcan
                    
                </ul>
            </li>    
            @endcan
            @can('lettre')
            <li class="menu">
                <a href="#Lettre" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail">
                            <path d="M22 12l-10 7-10-7V4a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2z"/>
                            <line x1="22" y1="12" x2="12" y2="19"/>
                            <line x1="2" y1="12" x2="12" y2="19"/>
                          </svg>
                          
                          
                        <span>{{ __('Ecrire un Lettre')  }}</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled" id="Lettre" data-parent="#accordionExample">
                    @can('create_lettre')
                     <li>
                        <a href="{{ route('lettre.create') }}"> {{ __('Lettre')  }} </a>
                    </li>   
                    @endcan
                    
                </ul>
            </li>
            @endcan
        </ul>

    </nav>

</div>
