@extends('layouts.app')

@section('content')
<div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh;">
    <div class="card shadow-sm p-4" style="width: 100%; max-width: 500px; border-radius: 10px;">
        <h4 class="text-center mb-4">Réinitialiser le mot de passe</h4>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="email" value="{{ $email }}">
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="mb-3">
                <label for="code" class="form-label">Code de réinitialisation</label>
                <input 
                    type="text" 
                    name="code" 
                    id="code" 
                    class="form-control @error('code') is-invalid @enderror" 
                    placeholder="Entrez le code de réinitialisation"
                    required
                >
                @error('code')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="password" class="form-label">Nouveau mot de passe</label>
                <input 
                    type="password" 
                    name="password" 
                    id="password" 
                    class="form-control @error('password') is-invalid @enderror" 
                    placeholder="Entrez un nouveau mot de passe"
                    required
                >
                @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="password_confirmation" class="form-label">Confirmer le mot de passe</label>
                <input 
                    type="password" 
                    name="password_confirmation" 
                    id="password_confirmation" 
                    class="form-control" 
                    placeholder="Confirmez votre mot de passe"
                    required
                >
            </div>

            <button type="submit" class="btn btn-primary w-100">Réinitialiser le mot de passe</button>
        </form>
        <div class="text-center mt-3">
            <a href="{{ route('login') }}" class="text-decoration-none">Retour à la connexion</a>
        </div>
    </div>
</div>
@endsection
