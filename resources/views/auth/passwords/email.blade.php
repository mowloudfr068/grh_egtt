@extends('layouts.app')

@section('content')
<div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh;">
    <div class="card shadow-sm p-4" style="width: 100%; max-width: 400px; border-radius: 10px;">
        <h4 class="text-center mb-4">Réinitialisation du mot de passe</h4>
        
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        
        <form method="POST" action="{{ route('password.email') }}" id="resetPasswordForm">
            @csrf
            <div class="mb-3">
                <label for="email" class="form-label">Adresse email</label>
                <input 
                    type="email" 
                    name="email" 
                    id="email" 
                    class="form-control @error('email') is-invalid @enderror" 
                    value="{{ old('email') }}" 
                    placeholder="Entrez votre email" 
                    required
                    autofocus
                >
                @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <!-- Bouton de soumission -->
            <button type="submit" class="btn btn-primary w-100" id="submitButton">
                Envoyer le lien de réinitialisation
            </button>
        </form>

        <!-- Message "Veuillez patienter..." caché initialement -->
        <div id="loadingMessage" class="text-center mt-3" style="display: none;">
            <div class="spinner-border text-primary" role="status">
                <span class="visually-hidden">Chargement...</span>
            </div>
            <p class="mt-2 text-muted">Veuillez patienter...</p>
        </div>

        <!-- Retour à la connexion -->
        <div class="text-center mt-3">
            <a href="{{ route('login') }}" class="text-decoration-none">Retour à la connexion</a>
        </div>
    </div>
</div>

<!-- Script JavaScript -->
<script>
    document.getElementById('resetPasswordForm').addEventListener('submit', function() {
        // Masquer le bouton de soumission
        document.getElementById('submitButton').style.display = 'none';
        
        // Afficher le message de chargement
        document.getElementById('loadingMessage').style.display = 'block';
    });
</script>
@endsection
