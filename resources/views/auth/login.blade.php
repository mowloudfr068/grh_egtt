@extends('layouts.app')

@section('content')
<style>
    body {
        background-color: #f8f9fa;
        color: #212529;
        background: url('{{ asset('images/EGTT.jpg') }}')
    }

    .bg-login-image {
        background: url('{{ asset('images/EGTT.jpg') }}');
        background-position: center;
        background-size: cover;
    }

    .form-control-user {
        border-radius: 10rem;
        padding: 1.5rem 1rem;
        font-size: 0.8rem;
        margin-bottom: 1rem; 
    }

    .btn-user {
        border-radius: 10rem;
        padding: 0.75rem 1rem;
        font-size: 0.8rem;
        margin-top: 1rem; 
    }

    .custom-control {
        margin-bottom: 1rem; 
    }

    .text-center a.small {
        margin-top: 1rem;
    }
</style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                   
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Bienvenue sur la plateforme EGTT</h1>

                                    @error('false_info') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <form action="{{ route('login') }}" method="post" class="user">
                                    @csrf
                                    <div class="form-group">
                                        <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-user" placeholder="Entrez votre  email...">
                                        @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control form-control-user" placeholder="Mot de passe">
                                        @error('password') <span class="text-danger">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="remember">Se souvenir de moi</label>
                                        </div>
                                    </div>
                                    <button type="submit" name="login" class="btn btn-primary btn-user btn-block">
                                        Connexion
                                    </button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="{{ route('password.request') }}">Mot de passe oublié ?</a>
                                </div>
                                
                                <div class="text-center">
                                    @if (Route::has('register'))
                                        <a class="small" href="{{ route('register') }}">Vous n'avez pas de compte ? Inscrivez-vous !</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
