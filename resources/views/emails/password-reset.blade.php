<!DOCTYPE html>
<html>
<head>
    <title>Réinitialisation de mot de passe</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .email-container {
            max-width: 600px;
            margin: 20px auto;
            background: #ffffff;
            border-radius: 8px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }
        .email-header {
            background: #4CAF50;
            color: #ffffff;
            padding: 20px;
            text-align: center;
        }
        .email-body {
            padding: 20px;
            line-height: 1.6;
            color: #333333;
        }
        .email-body p {
            margin: 0 0 15px;
        }
        .code-container {
            margin: 20px 0;
            text-align: center;
        }
        .code {
            font-size: 24px;
            color: #4CAF50;
            font-weight: bold;
            border: 1px solid #4CAF50;
            padding: 10px 20px;
            display: inline-block;
            border-radius: 4px;
            background-color: #f9f9f9;
        }
        .email-footer {
            background: #f4f4f4;
            text-align: center;
            padding: 10px;
            font-size: 14px;
            color: #888888;
        }
        .email-footer a {
            color: #4CAF50;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="email-container">
        <div class="email-header">
            <h1>Réinitialisation de mot de passe</h1>
        </div>
        <div class="email-body">
            <p>Bonjour,</p>
            <p>Vous avez demandé à réinitialiser votre mot de passe. Veuillez utiliser le code ci-dessous pour continuer le processus.</p>
            <div class="code-container">
                <span class="code">{{ $code }}</span>
            </div>
            <p>Si vous n'avez pas fait cette demande, veuillez ignorer cet email ou contacter le support pour toute question.</p>
            <p>Merci,</p>
            <p><strong>L'équipe Support</strong></p>
        </div>
        <div class="email-footer">
            <p>Si vous avez besoin d'aide, contactez-nous à <a href="mailto:support@example.com">support@example.com</a></p>
        </div>
    </div>
</body>
</html>
