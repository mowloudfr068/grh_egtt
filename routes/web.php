<?php

use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UnifiedLoginController;
use App\Http\Controllers\BultinController;
use App\Http\Controllers\Departments\DepartmentController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\LettreController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\Positions\PositionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StatistiqueController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('auth.login');
});


Route::middleware(['web.auth'])->prefix('espace-admin')->group(function () {
    // Departments 
    Route::resource('departments', DepartmentController::class);

    // Positions
    Route::resource('positions', PositionController::class);
    Route::get('/positions-by-department/{department}', [PositionController::class, 'getByDepartment'])->name('positions.by.department');
    Route::get('/positions-department/{department}', [PositionController::class, 'getPositionByDepartment'])->name('positionsBydepartment');
    // Employees
    Route::resource('employees', EmployeeController::class);
    Route::get('/contrats/cdi/{id}', [EmployeeController::class, 'downloadCDIContract'])->name('contracts.cdi');
    Route::get('/contrats/cdd/{id}', [EmployeeController::class, 'downloadCDDContract'])->name('contracts.cdd');
    

    // Congés
    Route::resource('leaves', LeaveController::class);
    Route::get('notifications/{id}/read', [LeaveController::class, 'redirectToLeave'])->name('notification.read');
    Route::get('MarkAsRead_all',[LeaveController::class,'MarkAsRead_all'])->name('MarkAsRead_all');
    // Présences
    Route::resource('attendances', AttendanceController::class);
    Route::get('list_attendances', [AttendanceController::class, 'list'])->name('attendances.list');
    Route::get('edit_attendance', [AttendanceController::class,'editAttendance'])->name('attendances.editAttendance');
    Route::get('/pointages/download-pdf', [AttendanceController::class, 'downloadPDF'])->name('attendances.downloadPDF');



    // Paiements
    Route::resource('payments', PaymentController::class);
    Route::get('Details', [PaymentController::class, 'details'])->name('payments.Details');
    Route::get('/paiements/download-pdf', [PaymentController::class, 'downloadPDF'])->name('payments.downloadPDF');
    Route::post('paiements/mass-store', [PaymentController::class, 'massStore'])->name('payments.mass_store');



    // Bulletins
    Route::get('/GenererBulltin', [BultinController::class, 'index'])->name('bulletin.generate');
    Route::post('/showBultinEmploye', [BultinController::class, 'showBulletin'])->name('bulltin.show');
    Route::post('/showBultinPatron', [BultinController::class, 'showBultinPatron'])->name('bulltin.showPatron');

    // Factures
    Route::resource('invoices', InvoiceController::class);
    Route::get('/invoice/{id}/print', [InvoiceController::class, 'print'])->name('invoices.print');
    Route::get('/factures/{invoice}/pdf', [InvoiceController::class, 'downloadInvoice'])->name('invoices.download');


    // Lettres
    Route::get('/lettre/create', [LettreController::class, 'createLettre'])->name('lettre.create');
    Route::get('/lettre/imprimer', [LettreController::class, 'imprimer'])->name('lettre.imprimer');

    Route::get('/Statistique',[StatistiqueController::class,'index'])->name('dashboard.statistique');

    Route::get('/test',function(){
        return view('pages.TestLivewire');
    });
});




// Auth::routes();

Route::get('login', [UnifiedLoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [UnifiedLoginController::class, 'login'])->name('login');
Route::post('logout', [UnifiedLoginController::class, 'logout'])->name('logout');

// Routes pour les utilisateurs généraux (users)
Route::prefix('user')->group(function () {
    Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
});

Route::group(['middleware' => ['auth']],function(){

    Route::resource('roles',RoleController::class);
    Route::resource('users',UserController::class);

});


 // Groupe de routes pour l'espace employé
 Route::middleware(['auth:employee'])->prefix('espace-employee')->name('employee.')->group(function () {
    Route::get('/info', [EmployeeController::class, 'getInfoEmploiyeeById'])->name('info');
    Route::get('/espaceEmpShow/{employee}', [EmployeeController::class, 'showEmp'])->name('show');
    Route::post('/espaceEmp/{employee}/validate', [EmployeeController::class, 'validateCode'])->name('validate');
    Route::put('EspaceEmpUpdate/{employee}', [EmployeeController::class, 'EspaceEmpUpdate'])->name('EspaceEmpUpdate');
    Route::get('/contratsEmp/cdi/{id}', [EmployeeController::class, 'downloadCDIContractEmployeeAuth'])->name('contracts.cdi');

    Route::get('/contratsEmp/cdd/{id}', [EmployeeController::class, 'downloadCDDContractEmployeeAuth'])->name('contracts.cdd');

    Route::get('/paiements-details', [PaymentController::class, 'getInfoEmploiyeedetailsById'])->name('paiements.details');
    Route::get('/conges', [LeaveController::class, 'getInfoCongesEmploiyeeById'])->name('conges');
    Route::post('/conges/store', [LeaveController::class, 'storeCongesEmploiyeeById'])->name('conges.store');
    Route::post('/conges/annuler', [LeaveController::class, 'AnnulerCongesEmploiyeeById'])->name('conges.annuler');
    Route::get('/bultin', [BultinController::class, 'getInfoEmploiyeeBulletinById'])->name('bultin');
    Route::post('/imprimer-bultin', [BultinController::class, 'ImprimerBultinEmployeeAuth'])->name('bultin.imprimer');
    Route::get('/pointages', [AttendanceController::class, 'getInfoPointagesEmploiyeeById'])->name('pointages');

    

    Route::get('/Dashboard',function(){
        return view('pages.site.dashboard');
    })->name('dashboard');
});

    



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
