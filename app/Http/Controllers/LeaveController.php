<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeaveRequest;
use App\Models\Leave;
use App\Models\Employee;
use App\Models\User;
use App\Notifications\DemandeConge;
use App\Traits\LogsActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class LeaveController extends Controller
{

    use LogsActivity;

    function __construct()
{
$this->middleware('permission:list_conges|ajout_conges|modifier_conges|supprimer_conges', ['only' => ['index','store']]);
$this->middleware('permission:modifier_conges', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_conges', ['only' => ['destroy']]);
}

    public function index()
    {
        $leaves = Leave::with('employee')->get();
        $employees = Employee::all();
        return view('pages.leaves.index', compact('leaves', 'employees'));
    }

    public function store(LeaveRequest $request)
    {
        try {
            
            Leave::create([
                'employee_id' => $request->employee_id,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'type' => $request->type,
                'status' => 'En Attente',
            ]);
            $dernierLeaveAjouter = Leave::latest()->first();
            $this->logActivity('Enregistrement', "Action sur conge id : ".$dernierLeaveAjouter->id);
            session()->flash('success', 'le conge a été ajouté avec succès!');
            return redirect()->route('leaves.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function update(LeaveRequest $request)
    {
        try {

            $validated = $request->validated();
            $leave = Leave::findOrFail($request->id);
            $leave->update([
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'type' => $request->type,
                'status' => $request->status,
            ]);
            $this->logActivity('modification', "Action sur conge id : ".$request->id);
            session()->flash('success', 'le conge a été modifier avec succès!');
            return redirect()->route('leaves.index');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show($id)
    {
        
        
        $leave = Leave::find($id);
         if(!$leave){
            session()->flash('info','le demande a ete annuler ou expire !');
            return redirect()->back();
         }

         return view('pages.leaves.details',compact('leave'));
    }

    public function redirectToLeave($notificationId)
{
    $notification = auth()->user()->unreadNotifications->find($notificationId);

    if ($notification) {
        $leaveId = $notification->data['id']; 
        $notification->markAsRead();
        return redirect()->route('leaves.show', $leaveId);
    }

    session()->flash('info', 'Cette notification a déjà été traitée ou n\'existe pas.');
    return redirect()->back();
}


    public function destroy(Request $request)
    {
        Leave::findOrFail($request->id)->delete();
        $this->logActivity('suppression', "Action sur conge id : ".$request->id);
        session()->flash('success', 'le conge a été supprimer avec succès!');
        return redirect()->route('leaves.index');
    }

    public function MarkAsRead_all(Request $request)
    {
        $userUnreadNotification = auth()->user()->unreadNotifications;
        if($userUnreadNotification){
            $userUnreadNotification->markAsRead();
            return back();

        }

    }

    // ------------- information employee authentifier -------------//
    public function getInfoCongesEmploiyeeById()
{
    $employee = auth()->guard('employee')->user();
    $leaves = Leave::with('employee')->where('employee_id', $employee->id)->get();

    return view('pages.site.leaves.index', compact('leaves','employee'));
}

public function storeCongesEmploiyeeById(Request $request)
{
    try {    
        Leave::create([
            'employee_id' => $request->employee_id,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'type' => $request->type,
            'status' => 'En Attente',
        ]);

        $user = User::get();
        $conge = Leave::latest()->first();
         Notification::send($user,new DemandeConge($conge));
         session()->flash('success', 'votre demande de conge a ete envoiyer avec succès!');
        return redirect()->route('employee.conges');
    } catch (\Exception $e) {
        return redirect()->back()->withErrors(['error' => $e->getMessage()]);
    } 
}
  public function AnnulerCongesEmploiyeeById(Request $request)
  {
    Leave::findOrFail($request->id)->delete();
    session()->flash('success', 'votre demande conge a ete annuler avec succès!');
    return redirect()->route('employee.conges');

  }
}
