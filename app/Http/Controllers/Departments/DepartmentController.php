<?php

namespace App\Http\Controllers\Departments;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use App\Traits\LogsActivity;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    use LogsActivity;

    function __construct()
{
$this->middleware('permission:list_departement|ajout_departement|modifier_departement|supprimer_departement', ['only' => ['index','store']]);
$this->middleware('permission:ajout_departement', ['only' => ['create','store']]);
$this->middleware('permission:modifier_departement', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_departement', ['only' => ['destroy']]);
}

    public function index()
    {
        $departements = Department::all();
        return view('pages.departements.index', compact('departements'));
    }


    public function create()
    {
       // 
    }

  
    public function store(DepartmentRequest $request)
    {

        try {
            // return $request;
            $validated = $request->validated();
            $departement = new Department();
            $departement->name = $request->name;
            $departement->description = $request->description;
            $departement->save();

            $dernierDepAjouter  = Department::latest()->first();
        $this->logActivity('Enregistrement', "Action sur departement id : ".$dernierDepAjouter->id);
            session()->flash('success', 'Departement a été ajouté avec succès!');
            return redirect()->route('departments.index');
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
        
    }

   
    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        //
    }


    public function update(DepartmentRequest $request)
    {
        try {
            $validated = $request->validated();
            $departement = Department::findOrFail($request->id);
            $departement->name = $request->name;
            $departement->description = $request->description;
            if (isset($request->status)) {
                $departement->status = 1;
            } else {
                $departement->status = 0;
            }

            $departement->save();
            $this->logActivity('modification', "Action sur departement id : ".$request->id);
            session()->flash('success', 'Departement a été modifier avec succès!');
            return redirect()->route('departments.index');
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    public function destroy(Request $request)
    {
        
        Department::findOrFail($request->id)->delete();
        session()->flash('success', 'Departement a été supprimer avec succès!');
        $this->logActivity('suppression', "Action sur departement id : ".$request->id);
        return redirect()->route('departments.index');
    }
}
