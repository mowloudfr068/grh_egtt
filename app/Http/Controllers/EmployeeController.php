<?php

namespace App\Http\Controllers;

use App\Mail\EmployeeValidationCode;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Position;
use App\Traits\LogsActivity;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{

    use LogsActivity;

    function __construct()
{
$this->middleware('permission:list_emploiye|ajout_emploiye|modifier_emploiye|supprimer_emploiye', ['only' => ['index','store']]);
$this->middleware('permission:ajout_emploiye', ['only' => ['create','store']]);
$this->middleware('permission:modifier_emploiye', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_emploiye', ['only' => ['destroy']]);
}
    
    public function index()
    {
        $employees = Employee::all();
        $departments = Department::all();
        $positions = Position::all();
        return view('pages.employes.index',compact('employees','departments','positions'));
    }

    public function downloadCDIContract($id)
{
    $employee = Employee::findOrFail($id);

    $pdf = Pdf::loadView('pages.employes.contratPdfCDI', [
        'company_name' => 'Entreprise Generale de Tout Traveaux',
        'employer_name' => 'Mohamed Lemin Moustapha',
        'employee_name' => $employee->first_name . ' ' . $employee->last_name,
        'position' => $employee->position->title,
        'department' => $employee->position->department->name,
        'salary' => $employee->position->base_salary,
        'start_date' => $employee->date_of_hire,
    ]);

    return $pdf->download('Contrat_CDI.pdf');
}

public function downloadCDDContract($id)
{
    $employee = Employee::findOrFail($id);

    $pdf = Pdf::loadView('pages.employes.contratPdfCDD', [
        'company_name' => 'Entreprise Generale de Tout Traveaux(EGTT)',
        'employer_name' => 'Mohamed Lemin Moustapha',
        'employee_name' => $employee->first_name . ' ' . $employee->last_name,
        'position' => $employee->position->title,
        'department' => $employee->position->department->name,
        'salary' => $employee->position->base_salary,
        'start_date' => $employee->date_of_hire,
        'duration' => $employee->contract_duration,
    ]);

    return $pdf->download('Contrat_CDD.pdf');
}

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'nni' => 'required|digits:10|unique:employees,nni',
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|email|unique:employees,email',
                'phone' => ['required', 'regex:/^[2-4]\d{7}$/'], 
                'address' => 'required|string|max:255',
                'date_of_birth' => [
                'required',
                'date',
                function ($attribute, $value, $fail) {
                    $dateOfBirth = \Carbon\Carbon::parse($value);
                    if ($dateOfBirth->age < 18) {
                        $fail('L\'employé doit avoir au moins 18 ans.');
                    }
                },
            ],
                'date_of_hire' => 'required|date',
                'position_id' => 'required|exists:positions,id',
                'contract_type' => 'required|in:CDI,CDD',
                'contract_duration' => 'nullable|integer|min:1|required_if:contract_type,CDD',
            ]);
    
            $lastEmployee = Employee::orderBy('matricule', 'desc')->first();
            $newMatricule = $lastEmployee ? $lastEmployee->matricule + 1 : 100; 
    
            $defaultPassword = bcrypt($validated['nni']);
    
            $employee = Employee::create([
                'nni' => $validated['nni'],
                'matricule' => $newMatricule,
                'first_name' => $validated['first_name'],
                'last_name' => $validated['last_name'],
                'email' => $validated['email'],
                'phone' => $validated['phone'],
                'address' => $validated['address'],
                'date_of_birth' => $validated['date_of_birth'],
                'date_of_hire' => $validated['date_of_hire'],
                'status' => 1,
                'position_id' => $validated['position_id'],
                'password' => $defaultPassword,
                'contract_type' => $validated['contract_type'],
                'contract_duration' => $validated['contract_type'] === 'CDD' ? $validated['contract_duration']: null,
            ]);
    
            $roleEmployee = Role::findByName('employee', 'employee');
            $employee->assignRole($roleEmployee);
    
            $permissions = Permission::where('guard_name', 'employee')->pluck('id');
            $roleEmployee->syncPermissions($permissions);
            $dernierEmpAjouter = Employee::latest()->first();
            $this->logActivity('Enregistrement', "Action sur Employee id : ".$dernierEmpAjouter->id);
            session()->flash('success','Employe a été ajouté avec succès!');
            return redirect()->route('employees.index')->with('success', __('Employé ajouté avec succès.'));
        } catch (\Exception $e) {
            return redirect()->route('employees.index')->with('error', __('Une erreur est survenue lors de l\'ajout de l\'employé.'));
        }
    }
    





  
    public function show(Employee $employee)
    {
        //
    }

    
    public function edit(Employee $employee)
    {
        //
    }

  
    public function update(Request $request, $id)
{
    try {
        $employee = Employee::findOrFail($id);

        $validated = $request->validate([
            'nni' => 'required|digits:10|unique:employees,nni,' . $employee->id,
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:employees,email,' . $employee->id,
            'phone' => ['required', 'regex:/^[2-4]\d{7}$/'], 
            'address' => 'required|string|max:255',
            'date_of_birth' => [
                'required',
                'date',
                function ($attribute, $value, $fail) {
                    $dateOfBirth = \Carbon\Carbon::parse($value);
                    if ($dateOfBirth->age < 18) {
                        $fail('L\'employé doit avoir au moins 18 ans.');
                    }
                },
            ],
            'date_of_hire' => 'required|date',
            'position_id' => 'required|exists:positions,id',
        ]);

        $employee->update([
            'nni' => $validated['nni'],
            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'email' => $validated['email'],
            'phone' => $validated['phone'],
            'address' => $validated['address'],
            'date_of_birth' => $validated['date_of_birth'],
            'date_of_hire' => $validated['date_of_hire'],
            'position_id' => $validated['position_id'],
            'status' => $request->has('status') ? $request->status : $employee->status,
            'contract_type' => $request->contract_type,
        'contract_duration' => $request->contract_type === 'CDD' ? $request->contract_duration : null,
        ]);
        $this->logActivity('modification', "Action sur Employe id : ".$request->id);
        session()->flash('success', 'Employe a été modifier avec succès!');
        return redirect()->route('employees.index')->with('success', __('Employé mis à jour avec succès.'));
    } catch (\Exception $e) {
        return redirect()->route('employees.index')->with('error', __('Une erreur est survenue lors de la mise à jour de l\'employé.'));
    }
}




public function destroy($id)
{
    try {
        $employee = Employee::findOrFail($id);

        

        $employee->delete();
        $this->logActivity('suppression', "Action sur Employe id : ".$id);
        session()->flash('success', 'Employe a été supprimer avec succès!');
        return redirect()->route('employees.index')->with('success', 'Employee deleted successfully.');
    } catch (\Exception $e) {
        return redirect()->back()->withErrors(['error' => $e->getMessage()]);
    }
}

// ----------- mehtode emploiyee authentifier ------------------//
public function getInfoEmploiyeeById()
{
    $employee = auth()->guard('employee')->user();
    $positions = Position::where('id', $employee->position_id)->get(); 

    return view('pages.site.employes.index',compact('employee','positions'));
}

public function EspaceEmpUpdate(Request $request, Employee $employee)
{
    
   
 Session::put('phone', $request->phone);
 Session::put('address', $request->address);

 if ($request->filled('password')) {
     Session::put('password', $request->password);
 }

 if ($request->hasFile('photo')) {
     $photoPath = $request->file('photo')->store('uploads/employees', 'public');
     Session::put('photo', $photoPath);
 }
    $validationCode = Str::random(6);
    Session::put('validation_code', $validationCode); 

    Mail::to($employee->email)->send(new EmployeeValidationCode($validationCode));

    session()->flash('success','Un code de validation a été envoyé par email. Veuillez entrer le code pour valider vos modifications.');
    return redirect()->route('employee.show', $employee->id)->with('message', 'Un code de validation a été envoyé par email. Veuillez entrer le code pour valider vos modifications.');
}

public function validateCode(Request $request, Employee $employee)
{
    $request->validate([
        'validation_code' => 'required|string',
    ]);

    $sessionCode = Session::get('validation_code');
    if ($request->validation_code !== $sessionCode) {
        return redirect()->route('employees.show', $employee->id)->with('error', 'Code de validation incorrect.');
    }

    $employee->phone = $request->session()->get('phone');
    $employee->address = $request->session()->get('address');

    if ($request->session()->has('password')) {
        $employee->password = bcrypt($request->session()->get('password'));
    }

    if ($request->session()->has('photo')) {
        if ($employee->photo && Storage::exists('public/' . $employee->photo)) {
            Storage::delete('public/' . $employee->photo);
        }

        $employee->photo = $request->session()->get('photo');
    }

    $employee->save();

    Session::forget('validation_code');
    $request->session()->forget(['phone', 'address', 'password', 'photo']);

    session()->flash('success', 'Vos modifications ont été validées et enregistrées.');
    return redirect()->route('employee.info', $employee->id);
}


public function showEmp($id){
    $employee = Employee::find($id);
    return view('pages.site.employes.show',compact('employee'));
}

public function downloadCDIContractEmployeeAuth($id)
{
    $employee = Employee::findOrFail($id);

    $pdf = Pdf::loadView('pages.site.employes.contratPdfCDI', [
        'company_name' => 'Entreprise Generale de Tout Traveaux',
        'employer_name' => 'Mohamed Lemin Moustapha',
        'employee_name' => $employee->first_name . ' ' . $employee->last_name,
        'position' => $employee->position->title,
        'department' => $employee->position->department->name,
        'salary' => $employee->position->base_salary,
        'start_date' => $employee->date_of_hire,
    ]);

    return $pdf->download('Contrat_CDI.pdf');
}

public function downloadCDDContractEmployeeAuth($id)
{
    $employee = Employee::findOrFail($id);

    $pdf = Pdf::loadView('pages.site.employes.contratPdfCDD', [
        'company_name' => 'Entreprise Generale de Tout Traveaux(EGTT)',
        'employer_name' => 'Mohamed Lemin Moustapha',
        'employee_name' => $employee->first_name . ' ' . $employee->last_name,
        'position' => $employee->position->title,
        'department' => $employee->position->department->name,
        'salary' => $employee->position->base_salary,
        'start_date' => $employee->date_of_hire,
        'duration' => $employee->contract_duration,
    ]);
    

    return $pdf->download('Contrat_CDD.pdf');
}




}
