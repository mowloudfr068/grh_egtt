<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Traits\LogsActivity;
use Barryvdh\DomPDF\Facade\Pdf as FacadePdf;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{

    use LogsActivity;
   
    function __construct()
{
$this->middleware('permission:list_factures|ajout_factures|modifier_factures|supprimer_factures', ['only' => ['index','store']]);
$this->middleware('permission:ajout_factures', ['only' => ['create','store']]);
$this->middleware('permission:modifier_factures', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_factures', ['only' => ['destroy']]);
}

    public function index()
    {
        $invoices = Invoice::all();
        return view('pages.invoices.index', compact('invoices'));
    }

    public function print($id)
{
    $invoice = Invoice::findOrFail($id);

    return view('pages.invoices.invoice_print', compact('invoice'));
}


    public function store(Request $request)
    {
        Invoice::create($request->all());
        $dernierFactAjouterId = Invoice::latest()->first()->id;
        $this->logActivity('Enregistrement', "Action sur facture id : ".$dernierFactAjouterId);
        session()->flash('success', 'le facture a été ajouté avec succès!');     
       return redirect()->route('invoices.index')->with('success', 'Facture ajoutée avec succès.');
    }

    public function update(Request $request, Invoice $invoice)
    {
        $invoice->update($request->all());
        $this->logActivity('modification', "Action sur facture id : ".$request->id);
        session()->flash('success', 'le facture a été modifier avec succès!');
        return redirect()->route('invoices.index')->with('success', 'Facture modifiée avec succès.');
    }

    public function destroy(Invoice $invoice)
    {
        $invoice->delete();
        
        $this->logActivity('suppression', "Action sur facture id : ".$invoice->id);

        session()->flash('success', 'le facture a été suprimer avec succès!');
        return redirect()->route('invoices.index')->with('success', 'Facture supprimée avec succès.');
    }

    public function downloadInvoice($id)
{
    $invoice = Invoice::find($id);

    $pdf = FacadePdf::loadView('pages.invoices.pdf', compact('invoice'));

    return $pdf->download('facture_INV-' . $invoice->id . '.pdf');
}
}
