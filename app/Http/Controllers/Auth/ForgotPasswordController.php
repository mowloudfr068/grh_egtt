<?php

namespace App\Http\Controllers\Auth;

use App\Models\User; 
use App\Models\Employee; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordResetMail;

class ForgotPasswordController extends Controller
{
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
{
    $request->validate([
        'email' => 'required|email',
    ]);

    $email = $request->input('email');

    
    $isUser = User::where('email', $email)->exists();
    $isEmployee = Employee::where('email', $email)->exists();

    if (!$isUser && !$isEmployee) {
        return back()->withErrors(['email' => "Cet email n'est pas enregistré dans notre système."]);
    }

    $table = $isUser ? 'password_resets' : 'password_resets_employees';
    $token = Str::random(60);
    $code = strtoupper(Str::random(6));

    DB::table($table)->updateOrInsert(
        ['email' => $email],
        [
            'token' => $token,
            'code' => $code,
            'created_at' => now(),
        ]
    );

    Mail::to($email)->send(new PasswordResetMail($code));

    return redirect()->route('password.reset', ['token' => $token, 'email' => $email])
                     ->with('status', 'Un code de réinitialisation a été envoyé à votre email.');
}




}
