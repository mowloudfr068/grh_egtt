<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{

    public function showResetForm(Request $request)
{
    return view('auth.passwords.reset-code', ['email' => $request->email, 'token' => $request->token]);
}

public function reset(Request $request)
{
    $request->validate([
        'email' => 'required|email',
        'password' => 'required|confirmed|min:6',
        'token' => 'required',
        'code' => 'required|size:6',
    ]);

    $email = $request->input('email');

    $userResetRecord = DB::table('password_resets')
                         ->where('email', $email)
                         ->where('code', $request->code)
                         ->where('token', $request->token)
                         ->first();

    $employeeResetRecord = DB::table('password_resets_employees')
                             ->where('email', $email)
                             ->where('code', $request->code)
                             ->where('token', $request->token)
                             ->first();

    $resetRecord = $userResetRecord ?? $employeeResetRecord;

    if (!$resetRecord) {
        return back()->withErrors(['code' => 'Le code ou le token est invalide ou expiré.']);
    }

    if (now()->diffInMinutes($resetRecord->created_at) > 60) {
        return back()->withErrors(['code' => 'Le code a expiré.']);
    }

    if ($userResetRecord) {
        $user = User::where('email', $email)->first();
    } elseif ($employeeResetRecord) {
        $user = Employee::where('email', $email)->first();
    }

    if (!$user) {
        return back()->withErrors(['email' => "Cet email n'est plus valide dans notre système."]);
    }

    $user->password = Hash::make($request->password);
    $user->save();

    $table = $userResetRecord ? 'password_resets' : 'password_resets_employees';
    DB::table($table)->where('email', $email)->delete();

    return redirect()->route('login')->with('status', 'Mot de passe réinitialisé avec succès!');
}




}
