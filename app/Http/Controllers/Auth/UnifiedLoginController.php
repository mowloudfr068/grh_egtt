<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnifiedLoginController extends Controller
{
    const ROLE_ADMIN = 'admin';
    const ROLE_EMPLOYEE = 'employee';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('auth')->only('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login'); 
    }

    public function login(Request $request)
    {
       
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        Auth::guard('web')->logout();
        Auth::guard('employee')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        if (Auth::guard('web')->attempt($request->only('email', 'password'), $request->remember)) {
            $user = Auth::user();
            if ($user->status === 'activer') {
                session(['role' => self::ROLE_ADMIN]);
                return $this->redirectBasedOnRole();
            } else {
                Auth::logout();
                return back()->withErrors(['false_info' => 'Votre compte est désactivé, veuillez contacter l\'administrateur du système.']);
            }
        }

        if (Auth::guard('employee')->attempt($request->only('email', 'password'), $request->remember)) {
            session(['role' => self::ROLE_EMPLOYEE]);
            return $this->redirectBasedOnRole();
        }

        return back()->withErrors(['false_info' => 'Email ou Mot de passe incorrect.']);
    }

    private function redirectBasedOnRole()
    {
        if (session('role') === self::ROLE_ADMIN) {
            return redirect()->intended(route('dashboard.statistique'));
        } elseif (session('role') === self::ROLE_EMPLOYEE) {
            return redirect()->intended(route('employee.dashboard'));
        }

        return redirect()->route('login');
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        Auth::guard('employee')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('login');
    }
}
