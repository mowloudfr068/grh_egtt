<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Invoice;
use App\Models\Leave;
use App\Models\Payment;
use App\Models\Position;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatistiqueController extends Controller
{
    
    public function index()
{
    $employeesCdi = Employee::where('contract_type', 'CDI')->count();
    $employeesCdd = Employee::where('contract_type', 'CDD')->count();
    $totalEmployees = Employee::count();

    $totalSalaries = Employee::join('positions', 'employees.position_id', '=', 'positions.id')
                             ->sum('positions.base_salary');

    $totalSalariesBrut = Payment::sum('salaire_brut_mensuel');
    $totalCnamEmployee = Payment::sum('cnam_employee');
    $totalCnamPatron = Payment::sum('cnam_patron');
    $totalCnssEmployee = Payment::sum('cnss_employee');
    $totalCnssPatron = Payment::sum('cnss_patron');
    $totalIts = Payment::sum('its');
    $totalConges = Payment::sum('conges');
    $totalCout = Payment::sum('total_cout');
    $averageNetSalary = Payment::avg('salaire_net');
    
    $totalDepartments = Department::count();
    $totalPositions = Position::count();

    $totalInvoicesAmount = Invoice::sum('total');
    $totalInvoicesCount = Invoice::count();
    
    $totalUsers = User::count();
    
    $today = Carbon::today();

    $employeesOnLeave = Leave::where('status', 'Approuvé')
        ->whereDate('start_date', '<=', $today)
        ->whereDate('end_date', '>=', $today)
        ->count();
    
    // Retourner la vue avec toutes les statistiques
    return view('dashboard', compact(
        'totalSalaries',
        'totalSalariesBrut',
        'totalCnamEmployee',
        'totalCnamPatron',
        'totalCnssEmployee',
        'totalCnssPatron',
        'totalIts',
        'totalConges',
        'totalCout',
        'averageNetSalary',
        'employeesCdi',
        'employeesCdd',
        'totalEmployees',
        'totalDepartments',
        'totalPositions',
        'totalInvoicesAmount',
        'totalInvoicesCount',
        'totalUsers',
        'employeesOnLeave'
    ));
}

}
