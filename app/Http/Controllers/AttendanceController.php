<?php 

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Employee;
use App\Models\Month;
use App\Traits\LogsActivity;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{

    use LogsActivity;


    function __construct()
{
$this->middleware('permission:list_presences|ajout_presences|modifier_presences|supprimer_presences', ['only' => ['list','store']]);
$this->middleware('permission:ajout_presences', ['only' => ['index','store']]);
$this->middleware('permission:modifier_presences', ['only' => ['editAttendance','update']]);
$this->middleware('permission:supprimer_presences', ['only' => ['destroy']]);
}

    public function index(Request $request)
{
    
    $months = Month::all();

    
    $selectedMonthId = $request->get('month_id',now()->month);
    $selectedMonth = Month::find($selectedMonthId);

    
    $year = $request->get('year', now()->year);

    $daysInMonth = $selectedMonth ? $selectedMonth->days : 0; 
    $days = range(1, $daysInMonth);  

    $employees = Employee::all();

    $attendances = Attendance::where('month_id', $selectedMonthId)
                              ->where('anne', $year)
                              ->get();

    $attendanceData = [];
    foreach ($attendances as $attendance) {
        $attendanceData[$attendance->employee_id] = json_decode($attendance->days, true);
    }

    return view('pages.attendances.index', [
        'months' => $months,
        'selectedMonth' => $selectedMonth,
        'days' => $days,  
        'employees' => $employees,
        'attendanceData' => $attendanceData, 
    ]);
}




public function store(Request $request)
{
    try {
        $validated = $request->validate([
            'month_id' => 'required|exists:months,id',
            'year' => 'required|integer|min:1900|max:2100',
            'attendance' => 'required|array',
        ]);

        foreach ($validated['attendance'] as $employeeId => $daysData) {
            $days = [];
            foreach ($daysData as $day => $status) {
                if ($status === 'present') {
                    $days[] = (int) $day; 
                }
            }

            Attendance::updateOrCreate(
                [
                    'employee_id' => $employeeId,
                    'month_id' => $validated['month_id'],
                    'anne' => $validated['year'],
                ],
                [
                    'statut' => count($days) > 0, 
                    'days' => json_encode($days), 
                ]
            );
        }
        $dernierAttendanceAjouter = Attendance::latest()->first();
        $this->logActivity('Enregistrement', "Action sur pointage id : ".$dernierAttendanceAjouter->id);
        session()->flash('success', 'pointage a été ajouté avec succès!');
        return redirect()->route('attendances.index')->with('success', __('Présences enregistrées avec succès.'));
    } catch (\Exception $e) {
        session()->flash('error', $e->getMessage());
        return redirect()->route('attendances.index')->with('error', __('Erreur lors de l\'enregistrement des présences.'));
    }
}


 

    public function list(Request $request)
    {
        $selectedMonthId = $request->input('month_id', now()->month);
        $selectedYear = $request->input('year', now()->year);
    
        $selectedMonth = Month::find($selectedMonthId);
    
        if (!$selectedMonth) {
            session()->flash('error', 'Mois sélectionné introuvable.');
            return redirect()->back()->with('error', 'Mois sélectionné introuvable.');
        }
    
        $selectedMonthDays = cal_days_in_month(CAL_GREGORIAN, $selectedMonthId, $selectedYear);
    
        $employees = Employee::all();
        $attendances = Attendance::where('month_id', $selectedMonthId)
            ->where('anne', $selectedYear)
            ->get();
    
        $attendanceData = [];
        foreach ($attendances as $attendance) {
            $attendanceData[$attendance->employee_id] = json_decode($attendance->days, true);
        }
    
        return view('pages.attendances.list_attendances', [
            'employees' => $employees,
            'attendanceData' => $attendanceData,
            'months' => Month::all(),
            'selectedMonthId' => $selectedMonthId,
            'selectedYear' => $selectedYear,
            'selectedMonthDays' => $selectedMonthDays,
        ]);
    }
    


 public function show(){


 }

 public function edit()
 {
    dd("Hello je suis le fonction edit attendance");
 }

 public function editAttendance(Request $request)
{
    $employeeId = $request->input('employee_id');
    $monthId = $request->input('month_id');
    $year = $request->input('anne');

    $employee = Employee::findOrFail($employeeId);
    $attendance = Attendance::where('employee_id', $employeeId)
        ->where('month_id', $monthId)
        ->where('anne', $year)
        ->first();

    if (!$attendance) {
        session()->flash('erro','Présence non trouvée.');
        return redirect()->back()->with('error', __('Présence non trouvée.'));
    }

    $days = json_decode($attendance->days, true);
    $selectedMonthDays = cal_days_in_month(CAL_GREGORIAN, $monthId, $year);
    $month = Month::findOrFail($monthId);

    return view('pages.attendances.edit_attendance', [
        'employee' => $employee,
        'attendance' => $attendance,
        'days' => $days,
        'selectedMonthDays' => $selectedMonthDays,
        'month' => $month,
        'year' => $year,
    ]);
}


public function update(Request $request)
{
    $request->validate([
        'employee_id' => 'required|exists:employees,id',
        'month_id' => 'required|exists:months,id',
        'anne' => 'required|integer',
        'days' => 'array', 
    ]);

    $employeeId = $request->input('employee_id');
    $monthId = $request->input('month_id');
    $year = $request->input('anne');
    $days = $request->input('days', []);

    $attendance = Attendance::where('employee_id', $employeeId)
        ->where('month_id', $monthId)
        ->where('anne', $year)
        ->first();

    if (!$attendance) {
        session()->flash('erro','Présence non trouvée.');
        return redirect()->back()->with('error', __('Présence non trouvée.'));
    }

    $attendance->days = json_encode($days);
    $attendance->save();
    $this->logActivity('modification', "Action sur pointage mois : ".$request->month_id." anne ".$year." employee_id ".$employeeId);
    session()->flash('success','pointage a  été modifier avec succès!');
    return redirect()->route('attendances.list')->with('success', __('Présence mise à jour avec succès.'));
}


    public function destroy($id)
    {
        Attendance::findOrFail($id)->delete();
        return redirect()->route('attendances.index')->with('success', 'Présence supprimée.');
    }

    public function downloadPDF(Request $request)
{
    $month_actual = now()->month;
    $selectedMonthId = $request->get('month_id',$month_actual);
    $year = $request->get('year', now()->year);
     $attendances = Attendance::with('employee', 'month')
     ->where('month_id', $selectedMonthId)
     ->where('anne', $year)
     ->get();

 $selected_month = Month::find($selectedMonthId);
 $selected_month_name = $selected_month->libelle;
 $selected_year = $year;

 $pdf = PDF::loadView('pages.attendances.pdf', compact('attendances', 'selected_month_name', 'selected_year'));

 return $pdf->download('détails_presences_'.$selected_month_name.'_'.$selected_year.'.pdf');
}

    // ------------ information employee authentifier---------//
    public function getInfoPointagesEmploiyeeById(Request $request)
{
    $currentEmployee = auth()->guard('employee')->user();

    if (!$currentEmployee) {
        return redirect()->route('login')->with('error', 'Vous devez être connecté pour accéder à cette page.');
    }

    $selectedMonthId = $request->input('month_id', now()->month);
    $selectedYear = $request->input('year', now()->year);

    $selectedMonth = Month::find($selectedMonthId);

    if (!$selectedMonth) {
        session()->flash('success','Mois sélectionné introuvable.');

        return redirect()->back()->with('error', 'Mois sélectionné introuvable.');
    }

    $selectedMonthDays = cal_days_in_month(CAL_GREGORIAN, $selectedMonthId, $selectedYear);

    // return "id emp ".$currentEmployee->id." id month ".$selectedMonth->id." anne ".$selectedYear;
         $attendances = Attendance::where('employee_id',$currentEmployee->id)
        ->where('month_id',$selectedMonth->id)
        ->where('anne',$selectedYear)
        ->get();
        if (!$attendances) {
            session()->flash('error','Mois sélectionné introuvable.');
            return redirect()->route('employee.pointages')->with('error', __('Présence non trouvée.'));
        }


    $attendanceData = [];
    foreach ($attendances as $attendance) {
        $attendanceData[$attendance->employee_id] = json_decode($attendance->days, true);
    }

    return view('pages.site.attendances.list_attendances', [
        'employees' => [$currentEmployee], 
        'attendanceData' => $attendanceData,
        'months' => Month::all(),
        'selectedMonthId' => $selectedMonthId,
        'selectedYear' => $selectedYear,
        'selectedMonthDays' => $selectedMonthDays,
    ]);
}

}
