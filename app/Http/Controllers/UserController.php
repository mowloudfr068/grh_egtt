<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\LogsActivity;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash ;

class UserController extends Controller
{

    use LogsActivity;

function __construct()
{
$this->middleware('permission:list_utilisateur|ajout_utilisateur|modifier_utilisateur|supprimer_utilisateur', ['only' => ['index','store']]);
$this->middleware('permission:ajout_utilisateur', ['only' => ['create','store']]);
$this->middleware('permission:modifier_utilisateur', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_utilisateur', ['only' => ['destroy']]);
}


public function index(Request $request)
{
$users = User::orderBy('id','ASC')->paginate(5);
$roles = Role::where('name', '!=', 'employee')->pluck('name','name');
return view('pages.users.index',compact('users','roles'))
->with('i', ($request->input('page', 1) - 1) * 5);
}

public function create()
{
$roles = Role::pluck('name','name')->all();
return view('pages.users.create',compact('roles'));
}

public function store(Request $request)
{

$this->validate($request, [
'name' => 'required',
'email' => 'required|email|unique:users,email',
'password' => 'required|same:confirm-password',
'roles_name' => 'required',
'status' => 'required'
]);
$input = $request->all();
$input['password'] = Hash::make($input['password']);
$user = User::create($input);
$user->assignRole($request->input('roles_name'));
$dernierUserAjouter = User::latest()->first();
$this->logActivity('Enregistrement', "Action sur utilisateur id : ".$dernierUserAjouter->id);
session()->flash('success','utilisateur a été ajouté avec succès!');
return redirect()->route('users.index')
->with('success','User created successfully');
}

public function show($id)
{
$user = User::find($id);
return view('pages.users.show',compact('user'));
}

public function edit($id)
{
$user = User::find($id);
$roles = Role::pluck('name','name')->all();
$userRole = $user->roles->pluck('name','name')->all();
return view('pages.users.edit',compact('user','roles','userRole'));
}

public function update(Request $request, $id)
{
$this->validate($request, [
'name' => 'required',
'email' => 'required|email|unique:users,email,'.$id,
'password' => 'same:confirm-password',
'roles' => 'required'
]);
$input = $request->all();
if(!empty($input['password'])){
$input['password'] = Hash::make($input['password']);
}else{
$input = array_except($input,array('password'));
}
$user = User::find($id);
$user->update($input);
DB::table('model_has_roles')->where('model_id',$id)->delete();
$user->assignRole($request->input('roles'));
$this->logActivity('modification', "Action sur utilisateur id : ".$id);
session()->flash('success','utilisateur a été modifier avec succès!');
return redirect()->route('users.index')
->with('success','User updated successfully');
}

public function destroy($id)
{
User::find($id)->delete();
$this->logActivity('suppression', "Action sur utilisateur id : ".$id);
session()->flash('success','utilisateur a été supprimer avec succès!');
return redirect()->route('users.index')
->with('success','User deleted successfully');
}
}