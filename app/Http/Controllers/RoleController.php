<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleController extends Controller
{

    use LogsActivity;

function __construct()
{
$this->middleware('permission:list_roles_utilisateur|ajout_roles_utilisateur|modifier_roles_utilisateur|supprimer_roles_utilisateur', ['only' => ['index','store']]);
$this->middleware('permission:ajout_roles_utilisateur', ['only' => ['create','store']]);
$this->middleware('permission:modifier_roles_utilisateur', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_roles_utilisateur', ['only' => ['destroy']]);
}

public function index(Request $request)
{
    $roles = Role::with('permissions')->where('name', '!=', 'employee')->paginate(10); 
    $permissions = Permission::whereDoesntHave('roles', function($query) {
        $query->where('name', 'employee'); 
    })->get();
return view('pages.roles.index',compact('roles','permissions'))
->with('i', ($request->input('page', 1) - 1) * 5);
}

public function create()
{
$permission = Permission::where('guard_name','web')->get();
return view('pages.roles.create',compact('permission'));
}

public function store(Request $request)
{
    $this->validate($request, [
        'name' => 'required|unique:roles,name',
        'permission' => 'required|array',
    ]);

    try {
        $role = Role::create([
            'name' => $request->input('name'),
            'guard_name' => 'web',
        ]);

        $permissions = Permission::whereIn('id', $request->input('permission'))
                                  ->pluck('name')
                                  ->toArray();

        $role->syncPermissions($permissions);
        $dernierRoleAjouter = Role::latest()->first();
        $this->logActivity('Enregistrement', "Action sur role id : ".$dernierRoleAjouter->id);
        session()->flash('success','le role a été ajouté avec succès!');
        return redirect()->route('roles.index')
                         ->with('success', 'Role created successfully.');
    } catch (\Exception $e) {
        return redirect()->back()->withErrors(['error' => $e->getMessage()]);
    }
}


public function show($id)
{
$role = Role::find($id);
$rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
->where("role_has_permissions.role_id",$id)
->get();
return view('pages.roles.show',compact('role','rolePermissions'));
}

public function edit($id)
{
$role = Role::find($id);
$permission = Permission::get();
$rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
->all();
return view('pages.roles.edit',compact('role','permission','rolePermissions'));
}

public function update(Request $request, $id)
{
    $this->validate($request, [
        'name' => 'required|unique:roles,name,' . $id, 
        'permission' => 'required|array', 
    ]);

    try {
        $role = Role::findOrFail($id);

        $role->update([
            'name' => $request->input('name'),
        ]);

        $permissions = Permission::whereIn('id', $request->input('permission'))
                                  ->pluck('name')
                                  ->toArray();

        $role->syncPermissions($permissions);
        $this->logActivity('modification', "Action sur role id : ".$id);
        session()->flash('success','le role a été modifier avec succès!');
        return redirect()->route('roles.index')
                         ->with('success', 'Role updated successfully.');
    } catch (\Exception $e) {
        return redirect()->back()->withErrors(['error' => $e->getMessage()]);
    }
}


public function destroy($id)
{
DB::table("roles")->where('id',$id)->delete();
$this->logActivity('suppression', "Action sur role id : ".$id);
session()->flash('success','le role a été supprimer avec succès!');
return redirect()->route('roles.index')
->with('success','Role deleted successfully');
}
}