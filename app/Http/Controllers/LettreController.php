<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LettreController extends Controller
{

    function __construct()
{
$this->middleware('permission:create_lettre', ['only' => ['createLettre','imprimer']]);

}


    public function createLettre()
    {
        return view('pages.lettres.createLettre');
    }
    public function imprimer(Request $request)
    {
        $destinataire = $request->input('destinataire');
        $corps = $request->input('corps');
        $salutation = $request->input('salutation');
        $signature = $request->input('signature');

        return view('pages.lettres.imprimerLettre', compact('destinataire', 'corps', 'salutation','signature'));
    }
}

