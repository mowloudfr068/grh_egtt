<?php

namespace App\Http\Controllers\Positions;

use App\Http\Controllers\Controller;
use App\Http\Requests\PositionRequest;
use App\Models\Department;
use App\Models\Position;
use App\Traits\LogsActivity;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    use LogsActivity;

    function __construct()
{
$this->middleware('permission:list_position|ajout_position|modifier_position|supprimer_position', ['only' => ['index','store']]);
$this->middleware('permission:ajout_position', ['only' => ['create','store']]);
$this->middleware('permission:modifier_position', ['only' => ['edit','update']]);
$this->middleware('permission:supprimer_position', ['only' => ['destroy']]);
}
    
    public function index()
    {
        $positions = Position::all();
        $departments = Department::all();
        return view('pages.positions.index', compact('positions','departments'));
    }

    public function getByDepartment($departmentId)

{
    
    $positions = Position::where('department_id', $departmentId)->get(['id', 'title']);
    return response()->json($positions);
}

public function getPositionByDepartment($departmentId)

{
    $positions = Position::where('department_id', $departmentId)->pluck('title', 'id');
    return response()->json($positions);
}





    
    public function create()
    {
        //
    }

    public function store(PositionRequest $request)
    { 
        try {
            Position::create($request->all());
            $dernierPosiAjouter = Position::latest()->first();
            $this->logActivity('Enregistrement', "Action sur Position id : ".$dernierPosiAjouter->id);
            session()->flash('success', 'position a été ajouté avec succès!');
            return redirect()->route('positions.index')->with('success', 'Position created successfully.');
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

   
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

 
    public function update(PositionRequest $request)
    {
        
        try {
            
            $validated = $request->validated();
            $position = Position::findOrFail($request->id);
            $position->update($request->all());
            $this->logActivity('modification', "Action sur Position id : ".$request->id);
            session()->flash('success', 'position a été modifier avec succès!');
            return redirect()->route('positions.index');
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

  
    public function destroy(Request $request)
    {


        Position::findOrFail($request->id)->delete();
        $this->logActivity('suppression', "Action sur Position id : ".$request->id);
        session()->flash('success', 'position a été supprimer avec succès!');
        return redirect()->route('positions.index');
    }
}
