<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Employee;
use App\Models\Month;
use App\Models\Payment;
use Illuminate\Http\Request;

class BultinController extends Controller
{

    function __construct()
{
$this->middleware('permission:list_emploiyee_bultin|imprimer_bultin_emploiyee|imprimer_bultin_patron', ['only' => ['index','showBulletin']]);
$this->middleware('permission:list_emploiyee_bultin', ['only' => ['index']]);
$this->middleware('permission:imprimer_bultin_emploiyee', ['only' => ['showBulletin']]);
$this->middleware('permission:imprimer_bultin_patron', ['only' => ['showBultinPatron']]);
}

    public function index(Request $request)
    {
        $selectedMonthId = $request->input('month_id', now()->month);
        $selectedYear = $request->input('year', now()->year);
    
        $selectedMonth = Month::find($selectedMonthId);
    
        if (!$selectedMonth) {
            return redirect()->back()->with('error', 'Mois sélectionné introuvable.');
        }
    
        $selectedMonthDays = cal_days_in_month(CAL_GREGORIAN, $selectedMonthId, $selectedYear);
    
        $employees = Employee::all();
        $attendances = Attendance::where('month_id', $selectedMonthId)
            ->where('anne', $selectedYear)
            ->get();
    
        $attendanceData = [];
        foreach ($attendances as $attendance) {
            $attendanceData[$attendance->employee_id] = json_decode($attendance->days, true);
        }
    
        return view('pages.bultin.bultin_employee', [
            'employees' => $employees,
            'attendanceData' => $attendanceData,
            'months' => Month::all(),
            'selectedMonthId' => $selectedMonthId,
            'selectedYear' => $selectedYear,
            'selectedMonthDays' => $selectedMonthDays,
        ]);
    }

    public function showBulletin(Request $request)
{
    $request->validate([
        'employee_id' => 'required|exists:employees,id',
        'month_id' => 'required|exists:months,id',
        'anne' => 'required|integer|min:2000|max:' . date('Y'),
    ]);
    $employeeId = $request->input('employee_id');
    $monthId = $request->input('month_id');
    $year = $request->input('anne');

    $employee = Employee::findOrfail($employeeId);
    $attendance = Attendance::where('employee_id',$employee->id)
    ->where('month_id',$monthId)->where('anne',$year)->first();
    if (!$attendance) {
        session()->flash('error','le pointage introuvable');
        return redirect()->back();
    }
    $payment  = Payment::where('attendance_id',$attendance->id)->first();
    $month = Month::findOrFail($monthId);
    if (!$payment) {
        session()->flash('error','le paiement introuvable');
        return redirect()->back();
    }

    $companyName = "EGTT"; 
    $startDate = "01 {$month->libelle} $year";
    $endDate = $month->days . " {$month->libelle} $year";
    $leaveDays = 2; 
    $remainingLeave = 5; 
    $usedLeave = 3; 
    $workedHours = 160; 

    return view('pages.bultin.impression_bultin_employee', compact(
     'payment', 'month', 'companyName', 'startDate', 'endDate', 'leaveDays', 'remainingLeave', 'usedLeave', 'workedHours'
    ));
}


public function showBultinPatron(Request $request)
{
    $request->validate([
        'employee_id' => 'required|exists:employees,id',
        'month_id' => 'required|exists:months,id',
        'anne' => 'required|integer|min:2000|max:' . date('Y'),
    ]);
    $employeeId = $request->input('employee_id');
    $monthId = $request->input('month_id');
    $year = $request->input('anne');

    $employee = Employee::findOrfail($employeeId);
    $attendance = Attendance::where('employee_id',$employee->id)
    ->where('month_id',$monthId)->where('anne',$year)->first();
    if (!$attendance) {
        session()->flash('error','le pointage introuvable');
        return redirect()->back();
    }
    $payment  = Payment::where('attendance_id',$attendance->id)->first();
    $month = Month::findOrFail($monthId);

    if (!$attendance) {
        return back()->withErrors(['attendance' => 'Attendance data not found.']);
    }
    if (!$payment) {
        return back()->withErrors(['payment' => 'Payment data not found.']);
    }

    $companyName = "EGTT"; 
    $startDate = "01 {$month->libelle} $year";
    $endDate = $month->days . " {$month->libelle} $year";
    $leaveDays = 2; 
    $remainingLeave = 5; 
    $usedLeave = 3; 
    $workedHours = 160; 

    return view('pages.bultin.impression_bultin_patron', compact(
         'payment','leaveDays', 'startDate', 'endDate', 
        'remainingLeave', 'usedLeave', 'workedHours', 'companyName'
    ));
}


//  ----------- information emploiyee authentifier-----------//
public function getInfoEmploiyeeBulletinById(Request $request)
{
    
    
    $selectedMonthId = $request->input('month_id', now()->month);
    $selectedYear = $request->input('year', now()->year);

    $selectedMonth = Month::find($selectedMonthId);

    if (!$selectedMonth) {
        return redirect()->back()->with('error', 'Mois sélectionné introuvable.');
    }

    $selectedMonthDays = cal_days_in_month(CAL_GREGORIAN, $selectedMonthId, $selectedYear);

    $employees = auth()->guard('employee')->user();
    $attendances = Attendance::where('month_id', $selectedMonthId)
        ->where('anne', $selectedYear)
        ->get();
        

    $attendanceData = [];
    foreach ($attendances as $attendance) {
        $attendanceData[$attendance->employee_id] = json_decode($attendance->days, true);
    }

    return view('pages.site.bultin.bultin_employee', [
        'employees' => [$employees],
        'attendanceData' => $attendanceData,
        'months' => Month::all(),
        'selectedMonthId' => $selectedMonthId,
        'selectedYear' => $selectedYear,
        'selectedMonthDays' => $selectedMonthDays,
    ]);
}
public function ImprimerBultinEmployeeAuth(Request $request)
{
    $request->validate([
        'employee_id' => 'required|exists:employees,id',
        'month_id' => 'required|exists:months,id',
        'anne' => 'required|integer|min:2000|max:' . date('Y'),
    ]);
    $employeeId = $request->input('employee_id');
    $monthId = $request->input('month_id');
    $year = $request->input('anne');

    $employee = auth()->guard('employee')->user();
    $attendance = Attendance::where('employee_id',$employee->id)
    ->where('month_id',$monthId)->where('anne',$year)->first();
    if (!$attendance) {
        session()->flash('error','le pointage introuvable');
        return redirect()->back();
    }
    $payment  = Payment::where('attendance_id',$attendance->id)->first();
    $month = Month::findOrFail($monthId);
    
    if (!$payment) {
        session()->flash('error','le paiement introuvable');
        return redirect()->back();
    }

    $companyName = "EGTT"; 
    $startDate = "01 {$month->libelle} $year";
    $endDate = $month->days . " {$month->libelle} $year";
    $leaveDays = 2; 
    $remainingLeave = 5; 
    $usedLeave = 3; 
    $workedHours = 160; 

    return view('pages.site.bultin.impression_bultin_employee', compact(
     'payment', 'month', 'companyName', 'startDate', 'endDate', 'leaveDays', 'remainingLeave', 'usedLeave', 'workedHours'
    ));
}

}
