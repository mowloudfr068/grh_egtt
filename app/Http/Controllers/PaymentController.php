<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Employee;
use App\Models\Leave;
use App\Models\Month;
use App\Models\Payment;
use App\Traits\LogsActivity;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    use LogsActivity;

    function __construct()
{
$this->middleware('permission:list_paiements|ajout_paiements|modifier_paiements|annuler_paiements', ['only' => ['details','store']]);
$this->middleware('permission:ajout_paiements', ['only' => ['index','store','calculerIts']]);
$this->middleware('permission:modifier_paiements', ['only' => ['edit','update']]);
$this->middleware('permission:annuler_paiements', ['only' => ['destroy']]);
}
    
    public function index(Request $request)
{
    
    $selectedMonthId = $request->get('month_id', now()->month);
    $selectedYear = $request->get('year', now()->year);

    $employees = Employee::all();
    $months = Month::all();
    $attendances = Attendance::where('month_id', $selectedMonthId)
        ->whereYear('anne', $selectedYear)
        ->get();

    $attendanceData = [];
        foreach ($attendances as $attendance) {
            $attendanceData[$attendance->employee_id] = json_decode($attendance->days, true);
        }

    return view('pages.payments.index', compact('employees', 'months', 'attendances', 'selectedMonthId', 'selectedYear', 'attendanceData'));
}



public function details(Request $request)
{
    // return $request;
    $selectedMonthId = $request->get('month_id', now()->month);
    $selectedYear = $request->get('year', now()->year);


    $selectedMonth = Month::find($selectedMonthId);
    
    if (!$selectedMonth) {
        session()->flash('error', 'Mois sélectionné introuvable.');
        return redirect()->back();
    }

    $months = Month::all();

    $payments = Payment::with('attendance.employee', 'attendance.month')
        ->when($selectedMonthId, function ($query) use ($selectedMonthId) {
            return $query->whereHas('attendance', function ($query) use ($selectedMonthId) {
                $query->where('month_id', $selectedMonthId);
            });
        })
        ->when($selectedYear, function ($query) use ($selectedYear) {
            return $query->whereHas('attendance', function ($query) use ($selectedYear) {
                $query->where('anne', $selectedYear);
            });
        })
        ->get();

    $sousTotal = $payments->sum(function ($payment) {
        return $payment->salaire_brut_mensuel 
            + $payment->cnam_patron 
            + $payment->medecin_travail 
            + $payment->cnss_patron 
            + $payment->conges;
    });

    $majoration13 = $sousTotal * 0.13; 
    $totalGeneral = $sousTotal + $majoration13;

    return view('pages.payments.details_paiements', compact(
        'payments',
        'months',
        'sousTotal',
        'majoration13',
        'totalGeneral',
        'selectedMonthId',
        'selectedYear'
    ));
}






   
    public function create()
    {
        //
    }

    public function store(Request $request)
{
    try {
        $employee = Employee::where('id', $request->employee_id)->first();

        $attendance = Attendance::where('employee_id', $employee->id)
            ->where('month_id', $request->month_id)
            ->where('anne', $request->year)
            ->first();

        if (!$attendance) {
            session()->flash('error', 'Aucune présence trouvée pour cet employé, mois et année.');
            return redirect()->back()->withErrors(['error' => 'Aucune présence trouvée pour cet employé, mois et année.']);
        }

        $existingPayment = Payment::where('attendance_id', $attendance->id)->first();
        if ($existingPayment) {
            session()->flash('error', 'Le paiement a déjà été effectué pour cet employé, mois et année.');
            return redirect()->back()->withErrors(['error' => 'Le paiement a déjà été effectué pour cet employé, mois et année.']);
        }

        $authorizedLeaves = Leave::where('employee_id', $employee->id)
            ->where('status', 'Approuvé') 
            ->where(function ($query) use ($request) {
                $query->whereBetween('start_date', [Carbon::create($request->year, $request->month_id, 1), Carbon::create($request->year, $request->month_id, 1)->endOfMonth()])
                      ->orWhereBetween('end_date', [Carbon::create($request->year, $request->month_id, 1), Carbon::create($request->year, $request->month_id, 1)->endOfMonth()]);
            })
            ->get();

        $totalLeaveDays = 0;
        foreach ($authorizedLeaves as $leave) {
            $start = max(Carbon::parse($leave->start_date), Carbon::create($request->year, $request->month_id, 1));
            $end = min(Carbon::parse($leave->end_date), Carbon::create($request->year, $request->month_id, 1)->endOfMonth());
            $totalLeaveDays += $start->diffInDays($end) + 1; // +1 pour inclure le dernier jour
        }

        

        $jour = intval($request->jour) + $totalLeaveDays;

        $salaireBrutMensuel = floatval($employee->position->base_salary);
        $slbb = ($salaireBrutMensuel / 21) * $jour;
        $cnamEmployee = round($slbb * 0.04, 2);
        $cnssEmployee = round(min($slbb * 0.13, 70), 2);
        $its = round($this->calculerIts($slbb), 2);
        $salaireNet = $slbb - ($cnamEmployee + $cnssEmployee + $its);
        $cnamPatron = round($slbb * 0.05, 2);
        $medecinTravail = round($slbb * 0.02, 2);
        $cnssPatron = round(min($slbb * 0.13, 1050), 2);
        $conges = round($salaireBrutMensuel / 12, 2);
        $totalCout = round($slbb + $cnamPatron + $medecinTravail + $cnssPatron + $conges, 2);

        $lastPayment = Payment::latest()->first();
        $lastOrderNumber = $lastPayment ? intval(substr($lastPayment->ordre, 4)) : 0;
        $newOrder = 'PAY-' . str_pad($lastOrderNumber + 1, 3, '0', STR_PAD_LEFT);

        $payment = Payment::create([
            'attendance_id' => $attendance->id,
            'ordre' => $newOrder,
            'salaire_brut_mensuel' => $slbb,
            'cnam_employee' => $cnamEmployee,
            'cnss_employee' => $cnssEmployee,
            'its' => $its,
            'salaire_net' => $salaireNet,
            'cnam_patron' => $cnamPatron,
            'medecin_travail' => $medecinTravail,
            'cnss_patron' => $cnssPatron,
            'conges' => $conges,
            'total_cout' => $totalCout,
        ]);
        $dernierPayAjouter = Payment::latest()->first();
        $this->logActivity('Enregistrement', "Action sur paiement id : ".$dernierPayAjouter->id." ORDRE ".$dernierPayAjouter->ordre);
        session()->flash('succes', 'Le paiement a été ajouté avec succès!');
        return redirect()->route('payments.index')->with('success', 'Paiement ajouté avec succès.');
    } catch (\Exception $e) {
        return redirect()->back()->withErrors(['error' => $e->getMessage()]);
    }
}


public function massStore(Request $request)
{
    try {

        $monthId = $request->month_id;
        $year = $request->year;
        $employees = Employee::with(['position'])->get();
        $attendances = Attendance::where('month_id', $monthId)
            ->where('anne', $year)
            ->get()
            ->keyBy('employee_id'); 

        $payments = [];
        $lastOrderNumber = Payment::latest()->value('ordre') 
            ? intval(substr(Payment::latest()->value('ordre'), 4)) 
            : 0;

         $dernierPayAvantPayerToutId = Payment::latest()->value('id')+1;

        foreach ($employees as $employee) {
            $attendance = $attendances->get($employee->id);

            if (!$attendance) {
                continue; 
            }
            $existingPayment = Payment::where('attendance_id', $attendance->id)->exists();
            if ($existingPayment) {
                continue; 
            }
            $daysArray = json_decode($attendance->days, true);
            $joursTravailles = is_array($daysArray) ? count($daysArray) : 0;
            $authorizedLeaves = Leave::where('employee_id', $employee->id)
                ->where('status', 'Autorisé')
                ->where(function ($query) use ($monthId, $year) {
                    $query->whereMonth('start_date', $monthId)
                          ->whereYear('start_date', $year)
                          ->orWhereMonth('end_date', $monthId)
                          ->whereYear('end_date', $year);
                })
                ->get();

            $totalLeaveDays = 0;
            foreach ($authorizedLeaves as $leave) {
                $startDate = Carbon::parse($leave->start_date);
                $endDate = Carbon::parse($leave->end_date);

                $daysInMonth = $startDate->month === $monthId || $endDate->month === $monthId;
                if ($daysInMonth) {
                    $totalLeaveDays += $startDate->diffInDaysFiltered(function ($date) {
                        return $date->isWeekday();
                    }, $endDate) + 1;
                }
            }

            $joursTravailles += $totalLeaveDays;

            if ($joursTravailles <= 0) {
                continue; 
            }
            $salaireBrutMensuel = floatval($employee->position->base_salary);
            $salaireBrut = ($salaireBrutMensuel / 21) * $joursTravailles;
            $cnamEmployee = round($salaireBrut * 0.04, 2);
            $cnssEmployee = round(min($salaireBrut * 0.13, 70), 2);
            $its = round($this->calculerIts($salaireBrut), 2);
            $salaireNet = $salaireBrut - ($cnamEmployee + $cnssEmployee + $its);
            $cnamPatron = round($salaireBrut * 0.05, 2);
            $medecinTravail = round($salaireBrut * 0.02, 2);
            $cnssPatron = round(min($salaireBrut * 0.13, 1050), 2);
            $conges = round($salaireBrutMensuel / 12, 2);
            $totalCout = round($salaireBrut + $cnamPatron + $medecinTravail + $cnssPatron + $conges, 2);
            $lastOrderNumber++; 
            $newOrder = 'PAY-' . str_pad($lastOrderNumber, 3, '0', STR_PAD_LEFT);

            $payments[] = [
                'attendance_id' => $attendance->id,
                'ordre' => $newOrder,
                'salaire_brut_mensuel' => $salaireBrut,
                'cnam_employee' => $cnamEmployee,
                'cnss_employee' => $cnssEmployee,
                'its' => $its,
                'salaire_net' => $salaireNet,
                'cnam_patron' => $cnamPatron,
                'medecin_travail' => $medecinTravail,
                'cnss_patron' => $cnssPatron,
                'conges' => $conges,
                'total_cout' => $totalCout,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        if (!empty($payments)) {
            Payment::insert($payments);
        
            $dernierPayApretPayerToutId = Payment::latest()->value('id');
            $this->logActivity('Enregistrement', "Action sur Paiements id : ".$dernierPayAvantPayerToutId." jusqua ".$dernierPayApretPayerToutId);
            session()->flash('success', 'Paiements effectués avec succès!');
        } else {
            session()->flash('info', 'Aucun employé à payer pour ce mois et cette année.');
        }

        return redirect()->route('payments.index');
    } catch (\Exception $e) {
        return redirect()->back()->withErrors(['error' => $e->getMessage()]);
    }
}










private function calculerIts($slbb)
{
    if ($slbb <= 6322.9) {
        return 0; // Si le salaire brut est inférieur à ce seuil, l'ITS est 0
    }

    $ri = round(($slbb * 100 - $slbb * 4 - 607000) / 100, 2); // Calcul du revenu imposable

    if ($ri <= 9000) {
        return $ri * 0.15; // Taux de 15% pour le revenu jusqu'à 9000 UM
    } elseif ($ri > 9000 && $ri <= 21000) {
        return $ri * 0.25 - 900; // Taux de 25% pour le revenu entre 9000 et 21000 UM
    } else {
        return $ri * 0.40 - 4050; // Taux de 40% pour le revenu au-delà de 21000 UM
    }
}




    



    
    public function show(Payment $payment)
    {
        //
    }


    public function edit(Payment $payment)
    {
        //
    }

   
    public function update(Request $request, Payment $payment)
    {
        //
    }

    public function destroy($id)
{
    $payment = Payment::find($id);

    if (!$payment) {
        session()->flash('error', 'Le paiement n\'existe pas.');
        return redirect()->route('payments.Details');
    }

    // Supprimer le paiement
    $payment->delete();
    $this->logActivity('suppression', "Action sur paiement id : ".$id);
    session()->flash('success', 'Le paiement a été annulé avec succès.');
    return redirect()->route('payments.Details');
}


    public function downloadPDF(Request $request)
{
    $payments = Payment::with('attendance.employee', 'attendance.month')
        ->whereHas('attendance', function($query) use ($request) {
            $query->where('month_id', $request->month_id)
                ->where('anne', $request->year);
        })
        ->get();

    $selected_month = Month::find($request->month_id);
    $selected_month_name = $selected_month->libelle;
    $selected_year = $request->year;
    
    $months = Month::all();

    // Calcul des totaux
    $sousTotal = $payments->sum(function ($payment) {
        return $payment->salaire_brut_mensuel 
            + $payment->cnam_patron 
            + $payment->medecin_travail 
            + $payment->cnss_patron 
            + $payment->conges;
    });
    $majoration13 = $sousTotal * 0.13; // 13% du sous-total
    $totalGeneral = $sousTotal + $majoration13; // Total 

    // Charger la vue
    $pdf = Pdf::loadView('pages.payments.pdf', compact('payments', 'months','sousTotal','majoration13','totalGeneral'));

    return $pdf->download('détails_paiements_'.$selected_month_name.'_'.$selected_year.'pdf');
}

    // ----------------------- information emploiyee authentifier------//
    public function getInfoEmploiyeedetailsById(Request $request)
{
     $currentEmployee = auth()->guard('employee')->user();

     if (!$currentEmployee) {
         return redirect()->route('login')->with('error', 'Vous devez être connecté pour accéder à cette page.');
     }
 
     $monthId = $request->input('month_id');
     $year = $request->input('year', date('Y')); 
 
     $months = Month::all();
 
     $payments = Payment::with('attendance.employee', 'attendance.month')
         ->whereHas('attendance', function ($query) use ($currentEmployee, $monthId, $year) {
             $query->where('employee_id', $currentEmployee->id)
                   ->when($monthId, function ($query) use ($monthId) {
                       $query->where('month_id', $monthId);
                   })
                   ->when($year, function ($query) use ($year) {
                       $query->where('anne', $year);
                   });
         })
         ->get();
 
     return view('pages.site.payments.details_paiements', compact('payments', 'months', 'monthId', 'year'));
}

}
