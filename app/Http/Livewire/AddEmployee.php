<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AddEmployee extends Component
{
    public $name;
    public $email;

    // Règles de validation
    protected $rules = [
        'name' => 'required|min:3',
        'email' => 'required|email',
    ];

    // Validation en temps réel
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    // Méthode pour enregistrer
    public function saveContact()
    {
        $this->validate(); // Validation complète

        // Logique pour sauvegarder
        // Exemple : Employee::create(['name' => $this->name, 'email' => $this->email]);

        session()->flash('success', 'Employé ajouté avec succès.');

        // Réinitialiser les champs du formulaire
        $this->reset(['name', 'email']);

        // Émettre un événement pour fermer le modal
        $this->emit('closeModal');
        return redirect()->route('dashboard.statistique');
    }

    public function render()
    {
        return view('livewire.add-employee');
    }
}
