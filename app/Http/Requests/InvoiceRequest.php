<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    
    public function authorize()
    {
        return true; 
    }

    
    public function rules()
    {
        return [
            'description'  => 'required|string|max:255',      
            'date_debut'   => 'required|date',                 
            'date_fin'     => 'required|date|after_or_equal:date_debut', 
            'total'        => 'required|numeric|min:0',       
            'nom_client'   => 'required|string|max:255',       
            'tel_client'   => 'required|string|max:15',       
        ];
    }

   
    public function messages()
    {
        return [
            'description.required'  => 'La description de la facture est obligatoire.',
            'date_debut.required'   => 'La date de début est obligatoire.',
            'date_debut.date'       => 'La date de début doit être une date valide.',
            'date_fin.required'     => 'La date de fin est obligatoire.',
            'date_fin.date'         => 'La date de fin doit être une date valide.',
            'date_fin.after_or_equal' => 'La date de fin doit être égale ou après la date de début.',
            'total.required'        => 'Le total est obligatoire.',
            'total.numeric'         => 'Le total doit être un nombre.',
            'total.min'             => 'Le total doit être supérieur ou égal à 0.',
            'nom_client.required'   => 'Le nom du client est obligatoire.',
            'tel_client.required'   => 'Le numéro de téléphone du client est obligatoire.',
            'tel_client.max'        => 'Le numéro de téléphone du client ne doit pas dépasser 15 caractères.',
        ];
    }
}
