<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceRequest extends FormRequest
{
    /**
     * Détermine si l'utilisateur est autorisé à faire cette requête.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Vous pouvez ajouter des règles d'autorisation si nécessaire
    }

    /**
     * Définir les règles de validation.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'month_id' => 'required|exists:months,id',
            'year' => 'required|integer|digits:4',
            'attendance' => 'required|array',
            'attendance.*' => 'array',
            'attendance.*.*' => 'in:present,absent', // Statut doit être 'present' ou 'absent'
        ];
    }

    /**
     * Messages personnalisés pour les erreurs de validation.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'month_id.required' => 'Le mois est obligatoire.',
            'month_id.exists' => 'Le mois sélectionné est invalide.',
            'year.required' => "L'année est obligatoire.",
            'year.integer' => "L'année doit être un nombre.",
            'year.digits' => "L'année doit être composée de 4 chiffres.",
            'attendance.required' => 'Les données de présence sont obligatoires.',
            'attendance.array' => 'Les données de présence doivent être un tableau.',
            'attendance.*.array' => 'Chaque employé doit avoir un tableau de présences.',
            'attendance.*.*.in' => 'Le statut de présence doit être "present" ou "absent".',
        ];
    }
}
