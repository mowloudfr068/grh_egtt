<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
   
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'nni' => 'required|string',
            'matricule' => 'required|string',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:employees,email,'.$this->id,
            'phone' => 'required|string|max:15',
            'date_of_birth' => 'required|date',
            'date_of_hire' => 'required|date',
            'position_id' => 'required|exists:positions,id',
            'password' => 'string|min:8',
        ];
    }
}
