<?php


namespace App\Traits;

use App\Models\AuditLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

trait LogsActivity
{
    public function logActivity($action, $description)
    {
        if (Auth::check()) {
            AuditLog::create([
                'user_id' => Auth::id(),
                'role' => Auth::user()->roles_name, 
                'action' => $action,
                'description' => $description,
                'ip_address' => Request::ip(),
            ]);
        }
    }
}
