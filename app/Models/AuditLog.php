<?php

namespace App\Models;

use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    use HasFactory;

    protected $fillable = ['user_id',
    'role',
    'action',
    'description',
    'ip_address'];
    protected $listen = [
        Login::class => [
            'App\Listeners\LogSuccessfulLogin',
        ],
        Logout::class => [
            'App\Listeners\LogSuccessfulLogout',
        ],
    ];

    protected $casts = [
        'role' => 'array',
    ];
}
