<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee_id', 
        'month_id', 
        'anne', 
        'statut', 
        'days',
    ];

    protected $casts = [
        'days' => 'array',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function month()
    {
        return $this->belongsTo(Month::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'attendance_id');
    }

    
}
