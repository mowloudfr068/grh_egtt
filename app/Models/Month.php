<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    use HasFactory;
    protected $fillable = ['libelle','days'];

    public function attendances()
{
    return $this->hasMany(Attendance::class);
}
}
