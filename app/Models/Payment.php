<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'ordre',
        'salaire_brut_mensuel',
        'cnam_employee',
        'cnss_employee',
        'its',
        'salaire_net',
        'cnam_patron',
        'medecin_travail',
        'cnss_patron',
        'conges',
        'total_cout',
        'attendance_id',
    ];

    
    public function attendance()
    {
        return $this->belongsTo(Attendance::class);
    }

   

 

}
