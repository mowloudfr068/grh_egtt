<?php

namespace App\Models;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Employee extends Model implements AuthenticatableContract
{
    use Authenticatable,HasFactory,Notifiable,HasRoles;
    protected $fillable = [
        'matricule','nni','first_name', 'last_name', 'email', 'phone', 'address', 'date_of_birth', 
        'date_of_hire', 'position_id', 'password','status','contract_type', 
    'contract_duration',
    ];


    public function position()
    {
        return $this->belongsTo(Position::class);
    }
    

    public function attendances()
{
    return $this->hasMany(Attendance::class);
}
}
