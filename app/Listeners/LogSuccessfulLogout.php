<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use App\Models\AuditLog;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;

class LogSuccessfulLogout
{
    public function handle(Logout $event)
    {

        if (Auth::guard('web')->check()) {
            $user = Auth::guard('web')->user();

            if ($user) {
                AuditLog::create([
                    'user_id' => $user->id,
                    'role' => is_array($user->roles_name) 
                        ? implode(', ', $user->roles_name) 
                        : $user->roles_name,
                    'action' => 'Déconnexion',
                    'description' => 'Utilisateur déconnecté avec succès',
                    'ip_address' => Request::ip(),
                ]);
            }
        }
    }
}
