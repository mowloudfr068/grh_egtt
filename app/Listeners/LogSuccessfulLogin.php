<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use App\Models\AuditLog;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;

class LogSuccessfulLogin
{
    public function handle(Login $event)
{
    $user = $event->user;

    if (!$user instanceof \App\Models\User) {
        return; 
    }

    if ($user) {
        $role = is_array($user->roles_name) 
            ? implode(', ', $user->roles_name) 
            : $user->roles_name;

        AuditLog::create([
            'user_id' => $user->id,
            'role' => $role,
            'action' => 'Connexion',
            'description' => 'Utilisateur connecté avec succès',
            'ip_address' => Request::ip(),
        ]);
    }
}


}
