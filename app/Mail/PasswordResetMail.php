<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class PasswordResetMail extends Mailable
{
    public $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function build()
    {
        return $this->subject('Code de réinitialisation de mot de passe')
                    ->view('emails.password-reset')
                    ->with('code', $this->code);
    }
}
