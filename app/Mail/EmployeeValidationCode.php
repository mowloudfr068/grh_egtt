<?php


namespace App\Mail;

use Illuminate\Mail\Mailable;

class EmployeeValidationCode extends Mailable
{
    public $validationCode;

    public function __construct($validationCode)
    {
        $this->validationCode = $validationCode;
    }

    public function build()
    {
        return $this->subject('Code de validation de modification de profil')
                    ->view('pages.site.employes.email.employee-validation-code');
    }
}
