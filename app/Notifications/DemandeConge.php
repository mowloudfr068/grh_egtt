<?php

namespace App\Notifications;

use App\Models\Leave;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DemandeConge extends Notification
{
    use Queueable;

    private $conge;

  
    public function __construct(Leave $conge)
    {
        $this->conge = $conge;
    }


    public function via($notifiable)
    {
        return ['database'];
    }



    public function toDatabase($notifiable)
    {
        return [

            // 'data' =>$this->details['body']
            'id'  => $this->conge->id,
            'title' =>'Un conge a ete demande par l\'employe : ',
            'user' => auth()->guard('employee')->user()->first_name
        ];

    }
 
}
